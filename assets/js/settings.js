jQuery(document).ready( function($){
  $('#atdw_sync_method').change(function(){
    if($(this).val() == 'manual'){
      $('#row-atdw_cron_method').hide();
      $('.row-atdw_cron_schedule').hide();
      $('.row-atdw_cron_settings').hide();
      $('.button-syncnow').show();
    }else{
      $('#row-atdw_cron_method').show();
      $('.row-atdw_cron_schedule').show();
      $('.row-atdw_cron_settings').show();
      $('.button-syncnow').hide();
    }
  });  
  
  $('#atdw_cron_method').change(function(){
    if($(this).val() == 'server'){
      $('.row-atdw_cron_settings').show();
    }else{
      $('.row-atdw_cron_settings').hide();
    }
  });
});