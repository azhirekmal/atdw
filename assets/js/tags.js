jQuery(document).ready( function($){
  $('#menu-posts, #menu-posts>a').removeClass('wp-has-current-submenu').removeClass('wp-menu-open');
  $('#menu-posts, #menu-posts>a').addClass('wp-not-current-submenu');
  $('#toplevel_page_atdw, #toplevel_page_atdw>a').removeClass('wp-not-current-submenu');
  $('#toplevel_page_atdw, #toplevel_page_atdw>a').addClass('wp-has-current-submenu').removeClass('wp-menu-open');
});