<?php
/**
 * @author Osky Interactive (https://www.oskyinteractive.com.au)
 * @package
 */
 
if ( !defined( 'ABSPATH' ) ) {
	@ini_set( 'include_path', '../../../' );
	require_once '../../../wp-load.php';
}

// need to add auth checking
// ...

// Global paths
if (!defined('ATDW_THEME_DIR'))
  define('ATDW_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
if (!defined('ATDW_PLUGIN_NAME'))
  define('ATDW_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));
if (!defined('ATDW_PLUGIN_DIR'))
  define('ATDW_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . ATDW_PLUGIN_NAME);
if (!defined('ATDW_PLUGIN_URL'))
  define('ATDW_PLUGIN_URL', WP_PLUGIN_URL . '/' . ATDW_PLUGIN_NAME);
  
// to define query ATDW API directly or via scrapper
define( 'ATDW_API_READER', false);

// cron related functions
require_once(ATDW_PLUGIN_DIR."/sync.php");
$ATDW_Sync = new ATDW_Sync();

// sync TripAdvisor entries
$ATDW_Sync->sync_tripadvisor();

// sync ATDW entries
$ATDW_Sync->sync();