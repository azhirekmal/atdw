<?php 

  class ATDW_tool
	{
    
    public function get_post_custom_compress($postId)
    {
    	$array = get_post_custom($postId);
    	foreach($array as $arrayK => $arrayV) { $array[$arrayK] = $arrayV[0]; }
    	$array2 = get_post( $postId, "ARRAY_A" );
    	$array = array_merge($array, $array2);
    	return $array;
    }
    
    public function subval_sort($a,$subkey) 
    {
    	foreach($a as $k=>$v) 
    	{
    		$b[$k] = strtolower($v[$subkey]);
    	}
    	asort($b);
    	foreach($b as $key=>$val) 
    	{
    		$c[] = $a[$key];
    	}
    	return $c;
    }
    
    public function is_assoc_array($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
    
    public function get_buss_social_links($data)
    {
    	$intLoop = 1;
    	$return = array();
      while($intLoop <= 10)
    	{
    		if(!empty($data['external_system_code-' . $intLoop]) && (($data['external_system_code-' . $intLoop] == 'FACEBOOK') || ($data['external_system_code-' . $intLoop] == 'TWITTER') || ($data['external_system_code-' . $intLoop] == 'INSTAGRAM') || ($data['external_system_code-' . $intLoop] == 'TRIPADVISO')))  
    		{
    			if((!(strpos($data['external_system_text-' . $intLoop], "http") === false)) || (!(strpos($data['external_system_text-' . $intLoop], "www.") === false)))
    			{
    				$return[$intLoop - 1]['type'] = $data['external_system_code-' . $intLoop];
    				$return[$intLoop - 1]['link'] = $data['external_system_text-' . $intLoop];
    				$return[$intLoop - 1]['field'] = 'external_system_text-' . $intLoop;
    			}
    		}
    		$intLoop++;
    	}
    	return $return;
    }
    
    public function atdw_data_process($data, $attr, $posttype = '')
    {
    	switch($attr)
    	{
    		case "profile" :
    			//$this->atdw_data_div_list($attr, "Title", @$data['atdw_pr_product_name']);
    			
    			$temp = explode("||", strip_tags(@$data['post_excerpt']));
    			if(isset($temp[1])){
    			     $temp[1] = str_replace(array("<p>", "</p>", "&lt;/p&gt;", "&lt;p&gt;"), "", $temp[1]);
    			}else{
    			     $temp[1] = null;
    			}
    			$this->atdw_data_div_list($attr, "Identification Number", array("ATDW" =>$temp[1], "ABN" => (isset($data['atdw_pr_abn']) ? $data['atdw_pr_abn'] : null)));
    														
					$this->atdw_data_div_list($attr, "Sync", (isset($data['atdw_sync_toggle']) ? $data['atdw_sync_toggle'] : null));

    			// $this->atdw_data_div_list($attr, "Show on Regional Highlight", (isset($data['atdw_show_on_highlight_toggle']) ? $data['atdw_show_on_highlight_toggle'] : null));
    			
    			$this->atdw_data_div_list($attr, "Location", array("City" => (isset($data['atdw_pr_city']) ? $data['atdw_pr_city'] : null),
    														"State" => (isset($data['atdw_pr_state']) ? $data['atdw_pr_state'] : null) ));
    			
    			$this->atdw_data_div_list($attr, "Description", (isset($data['atdw_pr_product_description']) ? $data['atdw_pr_product_description'] : null) );
          
    			$this->atdw_data_div_list($attr, "Short Description", (isset($data['atdw_pr_product_short_description']) ? $data['atdw_pr_product_short_description'] : null) );
    			
    			switch($posttype)
    			{
    			  case "atdw_accomm":
    			    $this->atdw_data_div_list($attr, "Product Rate", 
    			      array("From" => (!empty($data['atdw_pr_all_rate_from']) ? ($data['atdw_pr_all_rate_from']) : ""),
        						"To" => (!empty($data['atdw_pr_all_rate_to']) ? ($data['atdw_pr_all_rate_to']) : ""),
        						"Note" => (!empty($data['atdw_unknown_rate_basis_text']) ? ($data['atdw_unknown_rate_basis_text']) : "")));
              
    			    $this->atdw_data_div_list($attr, "Star Rating", (isset($data['atdw_pr_product_attr_rating-1']) ? $data['atdw_pr_product_attr_rating-1'] : null) );
              
    			    $this->atdw_data_div_list($attr, "Time", array("Reception Hours" => (isset($data['atdw_pr_reception_hours_txt']) ? $data['atdw_pr_reception_hours_txt'] : null),
      														"Check-In" => (isset($data['atdw_pr_check-in_time']) ? $data['atdw_pr_check-in_time'] : null),
      														"Check-Out" => (isset($data['atdw_pr_check-out_time']) ? $data['atdw_pr_check-out_time'] : null) ));
    			    break;
              
    			  case "atdw_restaurant": case "atdw_attraction": case "atdw_tour": 
    			    //look for the key with the value of 'OPENTIME', eg 'external_system_code-6'
    			    $tmp_opentime_key = array_search('OPENTIME',$data);
    			    if($tmp_opentime_key && $tmp_opentime_key != 'post_parent'){
    			      //eg change 'external_system_code-6' to 'external_system_text-6'
    			      $opentime_key = str_replace('external_system_code', 'external_system_text', $tmp_opentime_key);
    			      $this->atdw_data_div_list($attr, "Operating Hours", @$data[$opentime_key]);
    			    }else{
    			      $this->atdw_data_div_list($attr, "Operating Hours", '');
    			    }
    			    break;
    			    case "atdw_event":
                $this->atdw_data_div_list($attr, "Event Details", array("Start Date" => (isset($data['atdw_pr_event_starts']) ? $data['atdw_pr_event_starts'] : null),
                                  "End Date" => (isset($data['atdw_pr_event_ends']) ? $data['atdw_pr_event_ends'] : null),
                                  "Frequency" => (isset($data['atdw_pr_event_frequency']) ? $data['atdw_pr_event_frequency'] : null),
                                  "Status" => (isset($data['atdw_pr_event_status']) ? $data['atdw_pr_event_status'] : null) ));
    			    break;
    			}
    			
    			$faci = array("Children Catered" => (isset($data['atdw_pr_children_catered']) ? $data['atdw_pr_children_catered'] : null),
    								//"Disabled Access" => @$data['atdw_pr_disabled_access'],
    								"Pet Allowed" => (isset($data['atdw_pr_pet_allowed']) ? $data['atdw_pr_pet_allowed'] : null),
    								"Number of Room(s)" => (isset($data['atdw_pr_room_number']) ? $data['atdw_pr_room_number'] : null) );
    			if($posttype != "atdw_accomm") { array_pop($faci); }    			
    			$this->atdw_data_div_list($attr, "Facilities", $faci);
          
          $this->atdw_data_div_list($attr, "Accessibility", (isset($data['atdw_pr_disabled_access']) ? $data['atdw_pr_disabled_access'] : null) );
    			
    			if(!empty($data['atdw_booking_button']))
    			{
    				$this->atdw_data_div_list($attr, "Booking Button Landing Page", 'Example : <a href="' . $data['atdw_booking_button'] . '" target="_blank">' . $data['atdw_booking_button'] . '</a><br/>
    				<input name="input_atdw_booking_button" type="text" value="' . $data['atdw_booking_button'] . '" style="padding: 3px; width: 100%;" />');
    			}
    			else
    			{
    				$this->atdw_data_div_list($attr, "Booking Button Landing Page", '<input name="input_atdw_booking_button" type="text" value="http://" style="padding: 3px; width: 100%;" />');
    			}			
    			
    			break;
    			
    		case "communications" :
    			$this->atdw_data_div_list($attr, "Addresses", str_replace(", ",",<br/>",array("Location" => (isset($data['atdw_pr_pysical_addr']) ? $data['atdw_pr_pysical_addr'] : null), "Postal" => (isset($data['atdw_pr_postal']) ? $data['atdw_pr_postal'] : null) )));	
    			
    			$this->atdw_data_div_list($attr, "Address Latitude &amp; Longitude", array("Latitude" => (isset($data['atdw_pr_pysical_lat']) ? $data['atdw_pr_pysical_lat'] : null),
    																				"Longitude" => (isset($data['atdw_pr_pysical_long']) ? $data['atdw_pr_pysical_long'] : null) ) );
    			
    			$this->atdw_data_div_list($attr, "Contact Number", array("Phone Number" => (isset($data['atdw_pr_comm_phone']) ? $data['atdw_pr_comm_phone'] : null),
    																"Fax Number" => (isset($data['atdw_pr_comm_fax']) ? $data['atdw_pr_comm_fax'] : null),
    																"Mobile Number" => (isset($data['atdw_pr_comm_mobile']) ? $data['atdw_pr_comm_mobile'] : null),
    																"Tollfree Number" => (isset($data['atdw_pr_comm_tollfree']) ? $data['atdw_pr_comm_tollfree'] : null) ) );
    			
    			$this->atdw_data_div_list($attr, "E-Communication", array("Website" => (isset($data['atdw_pr_comm_website']) ? $data['atdw_pr_comm_website'] : null),
    																"Email" => (isset($data['atdw_pr_comm_email']) ? $data['atdw_pr_comm_email'] : null) ) );
    																
    			$socialLinks = $this->get_buss_social_links($data);
    			$temp_social_fb = '';
          $temp_social_ta = '';
          $temp_social_tw = '';
          $temp_social_ig = '';
          if(!empty($socialLinks))
    			{
    				foreach($socialLinks as $socialLink)
    				{
    					switch ($socialLink['type']) {
    					  case 'FACEBOOK':
    					    $temp_social_fb = $socialLink['field'];
    					    break;					  
    					  case 'TWITTER':
    					    $temp_social_tw = $socialLink['field'];
    					    break;                  
    					  case 'INSTAGRAM':
    					    $temp_social_ig = $socialLink['field'];
    					    break;                    
                case 'TRIPADVISO':
                  $temp_social_ta = $socialLink['field'];
      					  break;
                default:
                break;  
    					}
    				}
    			}
          $this->atdw_data_div_list($attr, "Social Links", array("Facebook" => (isset($data[$temp_social_fb]) ? $data[$temp_social_fb] : null),
                                    "Twitter" => (isset($data[$temp_social_tw]) ? $data[$temp_social_tw] : null),
                                    "Instagram" => (isset($data[$temp_social_ig]) ? $data[$temp_social_ig] : null),
    																"TripAdvisor" => (isset($data[$temp_social_ta]) ? $data[$temp_social_ta] : null) ) );
    			
    			break;
    		
    		case "media" :
    
    			$images = $this->draw_atdw_buss_imgs($data);
    			
    			if(count($images) > 0)
    			{
    				foreach($images as $image)
    				{
    					if(!empty($image['img']))
    					{
    						$imageTags[] = '<a class="thickbox_h" style="outline:0px;" href="' . $image['img'] . '" title="' . $image['alt'] . '"><img class="imageView" src="' . $image['img'] . '" height="100" style="padding: 3px; border: 1px solid #BBB; background-color: white;"></a><br><input type="checkbox" name="input_atdw_removimage-'. $image['key'] .'" value="1"> Remove this image?';
    					}
    				}
    				$this->atdw_data_div_list($attr, "Images", $imageTags);
    			}
    			else
    			{
    				$this->atdw_data_div_list($attr, "Images", array("none"));
    			}
    			$this->atdw_data_div_list($attr, "Add New Image", array("none"));
          
    			// videos
    			$videos = $this->draw_atdw_buss_vids($data);
    			
    			if(count($videos) > 0)
    			{
    				foreach($videos as $video)
    				{
    					if(!empty($video['vid']))
    					{
    						$videoTags[] = '<a href="' . $video['vid'] . '" target="_blank">' . $video['vid'] . '</a>'. ($video['alt'] ? '<br><br>' . $video['alt'] : '') . '<br><br><input type="checkbox" name="input_atdw_removvideo-'. $video['key'] .'" value="1"> Remove this video?';
    					}
    				}
    				$this->atdw_data_div_list($attr, "Videos", $videoTags);
    			}
    			else
    			{
    				$this->atdw_data_div_list($attr, "Videos", array("none"));
    			}
    			$this->atdw_data_div_list($attr, "Add New Video", array("none"));
    			
    			break;
    		
    		
    		case "activities" :					
    		case "facilities" :
    		case "things to do" :
    			
    			if($attr == "facilities"){
    				$key_word = "atdw_pr_product_attr_facility-";
    			}else if($attr == "activities"){
    				$key_word = "atdw_pr_product_attr_activity-";
    			}else if($attr == "things to do"){
    				$key_word = "atdw_pr_product_attr_experience-";
    			}
    			$sets = $this->atdw_data_multiple_value($data, $key_word);
    			$sets  = (count($sets) > 0 ? $sets : array(""));
    			$this->atdw_data_div_list($attr, ucwords($attr), $sets);			
    			break;
    	}
    }
    
    public function atdw_data_div_list($id, $label, $value)
    {
    	global $dataGlobal, $wpdb; ?>
    	<div id="<?php echo $id . "_" . strtolower(str_replace(" ", "_", $label)); ?>" class="busDetails hoverEffect" style="border: 1px #EEE solid; display: block; -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px; margin: 10px 5px; padding: 10px;">
    		<label style="font-weight: bold;"><?php echo $label; ?></label><br/>
    		<?php if(!is_array($value)) { ?>
    		
					<p style="line-height: 20px; margin: 10px 20px -10px 20px; font-style: italic;">
					<?php 
    			$atdw_fieldname = 'input_atdw_' . $id . '_' . strtolower(str_replace(" ", "", $label));
    			switch($label){
    				case 'Title':
    					echo '<input type="text" style="width:100%;" name="'.$atdw_fieldname.'" value="'.$value.'">';
    					break;
    				case 'Description':
    					$temp_description = str_replace("<br />","\n",$value);
    					$temp_description = str_replace("<br>","\n",$temp_description);
    					echo '<textarea rows="10" style="width:100%;" name="'.$atdw_fieldname.'">'.$temp_description.'</textarea>';
    					break;
    				case 'Short Description':
    					$temp_description = str_replace("<br />","\n",$value);
    					$temp_description = str_replace("<br>","\n",$temp_description);
    					echo '<textarea rows="3" style="width:100%;" name="'.$atdw_fieldname.'">'.$temp_description.'</textarea>';
    					break;              
            case 'Star Rating':
      				$temp_rating = strtolower($value);
      				echo '<select name="'.$atdw_fieldname.'">';
      				echo '<option value="Not Available"'.(($temp_rating == '' || $temp_rating == 'not available' ) ? ' selected="selected"' : '').'>Not Available</option>';
      				echo '<option value="1 Star"'.(($temp_rating == '1 star') ? ' selected="selected"' : '').'>1 Star</option>';
      				echo '<option value="1 1/2 Stars"'.(($temp_rating == '1 1/2 stars') ? ' selected="selected"' : '').'>1 1/2 Stars</option>';
      				echo '<option value="2 Stars"'.(($temp_rating == '2 stars') ? ' selected="selected"' : '').'>2 Stars</option>';
      				echo '<option value="2 1/2 Stars"'.(($temp_rating == '2 1/2 stars') ? ' selected="selected"' : '').'>2 1/2 Stars</option>';
      				echo '<option value="3 Stars"'.(($temp_rating == '3 stars') ? ' selected="selected"' : '').'>3 Stars</option>';
      				echo '<option value="3 1/2 Stars"'.(($temp_rating == '3 1/2 stars') ? ' selected="selected"' : '').'>3 1/2 Stars</option>';
      				echo '<option value="4 Stars"'.(($temp_rating == '4 stars') ? ' selected="selected"' : '').'>4 Stars</option>';
      				echo '<option value="4 1/2 Stars"'.(($temp_rating == '4 1/2 stars') ? ' selected="selected"' : '').'>4 1/2 Stars</option>';
      				echo '<option value="5 Stars"'.(($temp_rating == '5 stars') ? ' selected="selected"' : '').'>5 Stars</option>';
      				echo '</select>';
      				break;
            case 'Accessibility': case 'Operating Hours':
        			echo '<input type="text" style="width:100%;" name="'.$atdw_fieldname.'" value="'.$value.'">';
        			break;
            case 'Sync':
    					echo '<input type="checkbox" name="'.$atdw_fieldname.'" ';
    					//check whether is a new post
    					$temp_post_status = $wpdb->get_var("SELECT post_status FROM wp_posts WHERE ID = '" . get_the_ID() . "'");
    					if(($temp_post_status != 'publish')&&($temp_post_status != 'private')){
    						echo 'checked="checked" ';
    					}else{
    						echo ($value == '1') ? 'checked="checked" ' : '';
    					}					
    					echo 'value="1"> Exclude this entry from ATDW sync.';
    					break;
            // case 'Show on Regional Highlight':
            //             echo '<input type="checkbox" name="'.$atdw_fieldname.'" ';
            //             //check whether is a new post
            //             $temp_post_status = $wpdb->get_var("SELECT post_status FROM wp_posts WHERE ID = '" . get_the_ID() . "'");
            //             if(($temp_post_status != 'publish')&&($temp_post_status != 'private')){
            //                 echo 'checked="checked" ';
            //             }else{
            //                 echo ($value == '1') ? 'checked="checked" ' : '';
            //             }                   
            //             echo 'value="1"> Override random highlight.';
            //             break;
    				default:
    					echo $value;
    					break;
    			}
    			//echo '['.$label.']';
    			?></p>
    			
    		<?php } elseif($this->is_assoc_array($value)) { $sx = (count($value) > 1 ? (100 / count($value)) : 100); ?>
    		
    			<div style="padding: 10px;">
    			<?php foreach($value as $k => $v) { ?>			
    				<div style="float: left; width: <?php echo $sx; ?>%">
    					<label style="font-weight: bold;"><?php echo $k; ?></label><br/>
    					<p style="line-height: 14px; margin: 10px 0px 5px 20px; font-style: italic;"><?php 
    					$atdw_fieldname = 'input_atdw_' . $id . '_' . strtolower(preg_replace("/[^a-zA-Z0-9]/", "", $k));
    					switch($k){
    						case 'ATDW': case 'ABN': 
    						case 'City': case 'State':
    						case 'Reception Hours': case 'Check-In': case 'Check-Out': 
    						case 'Frequency': case 'Status':
    						case 'Children Catered': case 'Disabled Access': case 'Pet Allowed': case 'Number of Room(s)': 
    						case 'Latitude': case 'Longitude': 
    						case 'Phone Number': case 'Fax Number': case 'Mobile Number': case 'Tollfree Number': 
    						case 'Website': case 'Email': 
                case 'Facebook': case 'Twitter': case 'Instagram': case 'TripAdvisor': case 'Note':
    							echo '<input type="text" value="'.$v.'" name="'.$atdw_fieldname.'" style="width:90%;">'; 
    							break;
                case 'Start Date':
    							echo '<script>
    							jQuery(document).ready(function($) {
    							  $( function() { $( "#datepicker-estart" ).datepicker({
    							    dateFormat: "yy-mm-dd", 
    							    minDate: 0,
    							    onClose: function(selectedDate) {
    							      if(selectedDate){
    							        $("#datepicker-eend").datepicker("option", "minDate", selectedDate);
    							      }else{
    							        $("#datepicker-eend").datepicker("option", "minDate", 0);
    							      }
    							    }
    							  }); } );
    							});
    							</script>';
    							echo '<input type="text" value="'.$v.'" name="'.$atdw_fieldname.'" id="datepicker-estart" style="width:90%;">'; 
    							break;
                case 'End Date':
    							if($v){
    							  $tmp_middate = '$("#datepicker-estart").datepicker().val()';
    							}else{
    							  $tmp_middate = '0';
    							}
                  echo '<script>
    							jQuery(document).ready(function($) {
    							  $( function() { $( "#datepicker-eend" ).datepicker({dateFormat: "yy-mm-dd", minDate: '.$tmp_middate.'}); } );
    							});
    							</script>';
    							echo '<input type="text" value="'.$v.'" name="'.$atdw_fieldname.'" id="datepicker-eend" style="width:90%;">'; 
    							break;  
                case 'From': case 'To':
    							echo '$ <input type="text" value="'.$v.'" name="'.$atdw_fieldname.'">'; 
    							break;
                case 'Location': case 'Postal': 
    							echo '<textarea rows="10" style="width:100%;" name="'.$atdw_fieldname.'">'.str_replace("<br/>","\n",$v).'</textarea>';
    							break;  
    						default:
    							echo $v;
    					}
    					//echo "[[".$k."]]";				
    					?></p>
    				</div>
    			<?php } ?>
    			</div>
    			
    		<?php } else { ?>
    		
    			<div style="padding: 10px;">
    			<?php
    		if(($label == "Facilities") || ($label == "Activities") || ($label == "Things To Do")){ 
    			for($i = 0; $i < 24; $i++){
    			?>			
    				<div style="float: left; width: 33%;">
    				<p style="line-height: 14px; margin: 10px 0px 0px 0px; font-style: italic;">
    					<?php 
              if($label == "Facilities"){
    						echo '<input type="text" value="'.(isset($value[$i]) ? $value[$i] : '').'" name="input_atdw_facility-'.($i+1).'">'; 
    					}else if($label == "Activities"){
    						echo '<input type="text" value="'.(isset($value[$i]) ? $value[$i] : '').'" name="input_atdw_activity-'.($i+1).'">'; 
    					}else if($label == "Things To Do"){
    						echo '<input type="text" value="'.(isset($value[$i]) ? $value[$i] : '').'" name="input_atdw_experience-'.($i+1).'">'; 
    					}
    					?>
    				</p>			
    				</div>
    			<?php 
    			} 
    		}else if($label == "Add New Image"){
    			
    			echo 'Upload: <input type="file" name="input_atdw_newimage"><br>';
    			echo 'Image Description: <input type="text" name="input_atdw_newimage_text">';

    		}else if($label == "Add New Video"){
    			
    			echo 'URL: <input type="text" name="input_atdw_newvideo"><br>';
    			echo 'Video Description: <input type="text" name="input_atdw_newvideo_text">';

    		}else{
    			$i = 0;
    			foreach($value as $v) { 
    				?>			
    					<div style="float: left; width: 33%;">
    					<p style="line-height: 14px; margin: 10px 0px 0px 0px; font-style: italic;">
    						<?php 
    						echo (empty($v) ? "none" : $v); 	
    						$i++;
    						?>
    					</p>			
    					</div>
    				<?php
    			}
    		}
    		?>
    			</div>
    			
    		<?php } ?>
    		<br style="clear:both;" />
    	</div>
    	<?php
    }
    
    public function atdw_sticky_joinlf($join_paged_statement)
    {
    	$join_paged_statement .= " LEFT JOIN wp_postmeta atdwstick ON atdwstick.post_id = wp_posts.ID";
    	return $join_paged_statement;
    }
    
    public function atdw_sticky_where($where_statement)
    {
    	$where_statement .= " AND (atdwstick.meta_key = 'atdw_booking_button_int')" ;
    	return $where_statement;
    }
    
    public function atdw_sticky_orderby($orderby_statement)
    {
    	$orderby_statement = "atdwstick.meta_value DESC, wp_posts.post_title ASC";
    	return $orderby_statement;
    }
    
    public function add_meta_atdw_location($wpQuery)
    {
    	$wpQuery->set( 'meta_key', 'atdw_pr_city' );
    	return $wpQuery;
    }
    
    public function add_meta_atdw_location_get($where = '')
    {
    	if(!empty($_GET['R']))
    	{
    		//added DEC 2012
    		$reg = $_GET['R'];
    		$reg = get_term_by( 'slug', $reg, 'towns_cat', ARRAY_A);
    		global $wpdb;
    		$towns = $wpdb->get_col('SELECT object_id FROM  `wp_term_relationships` WHERE  `term_taxonomy_id` = '.$reg['term_taxonomy_id']);
    		if(!empty($towns))
    		{
    			$sql = array();
    			foreach ($towns as $town) {
    				$sql[] = ' wp_postmeta.meta_value = "' . get_the_title($town) . '" ';
    			}
    			$where .= ' AND ('.implode(' OR ', $sql).' ) ';
    		}
    	}
    	else
    	{
    		$string = explode('-', $_GET['L']);
    		if(is_array($string))
    		{
    			$new = "";
    			foreach($string as $index => $part)
    			{
    				if($index != 0)
    				{
    					$new .= " ";
    				}
    				$new .= $part;
    			}
    			$string = $new;
    		}
    		$where .= ' AND wp_postmeta.meta_value LIKE "%' . $string . '%"';
    	}
    
    	return $where;
    }
    
    public function atdw_data_multiple_value($data, $key_word, $sort = true)
    {
    	$sets = array();
    	$intLoop = 0;	
    	foreach(@$data as $k => $v)
    	{
    		if(!(strpos($k,$key_word) === false))
    		{
    			if($v){
    				$sets[$intLoop] = $v;
    				$intLoop++;
    			}
    		}
    	}
    	if($sort)
    	{
    		sort($sets);
    	}
    	return $sets;
    }
    
    public function atdw_data_random_image($data)
    {
    	$images = draw_atdw_buss_imgs($data);
    	
    	$imagesEcho = array("src" => get_bloginfo('url') . "/wp-content/themes/shoalhaven/images/logoContent.jpg",
    						"alt" => $data['post_title']);
    	
    	$count = count($images);
    	if($count > 0)
    	{
    		$random = rand(0 , ($count - 1));
    		$imagesEcho['src'] = $images[$random]['img'];	
    		$imagesEcho['alt'] = $images[$random]['alt'];	
    	}
    	
    	return $imagesEcho;
    }
    
    public function atdw_data_multiple_tabular($sets, $column)
    {
    	$count = count($sets);
    	$row = round(($count / $column), 0);
    	$tabular = "";
    	$width = (100 / $column);
    	
    	$intReset = 1;
    	$intLoop = 1;
    	$intRow = 1;
    	foreach($sets as $set)
    	{
    		if($intReset == 1)
    		{
    			if($row <= $intRow)
    			{
    				$tabular .= '<tr class="last">';
    			}
    			else
    			{
    				$tabular .= "<tr>";
    			}
    		}
    		
    		$tabular .= '<td style="width: ' . $width .'%">' . $set . "</td>";
    		
    		if($column == $intReset)
    		{
    			$tabular .= "</tr>";
    			$intReset = 1;
    			$intRow++;
    		}
    		else
    		{
    			$intReset++;
    		}
    		
    		if($count == ($intLoop))
    		{
    			while($intReset <= $column)
    			{
    				$tabular .= '<td style="background: none;">&nbsp;</td>';
    				$intReset++;
    			}
    			$tabular .= "</tr>";
    		}
    		$intLoop++;		
    	}	
    	
    	if(isset($_GET['diag'])) { var_dump(htmlentities($tabular, ENT_QUOTES)); }
    	
    	return $tabular;
    }
    
    public function draw_atdw_buss_imgs($data)
    {
    	$images = array();
    	
    	if(!empty($data['atdw_bmm_img_sequence']))
    	{
    		$temp = json_decode($data['atdw_bmm_img_sequence'], true);
    		if(!empty($temp))
    		{
    			foreach ($temp as $key => $value)
    			{
    				if(!empty($data['atdw_bmm_image-'.$key]))
    				{
    					$images[] = array(
    										'img' => $data['atdw_bmm_image-'.$key],
    										'alt' => $data['atdw_bmm_img_alt-'.$key],
    										'key' => $key
    									);
    				}
    			}
    			
    			if(!empty($images))
    			{
    				return $images;
    			}
    		}
    	}
    	
    	$intLoop = 0;
    	foreach($data as $k => $v)
    	{
    		if( (!(strpos($k,"atdw_bmm_image-") === false)) && (!empty($v)) )
    		{
    			$temp = str_replace("atdw_bmm_image-", "", $k);
    			$images[$intLoop]['img'] = $v;							
    			$images[$intLoop]['alt'] = @$data['atdw_bmm_img_alt-' . $temp];
    			$intLoop++;						
    		}
    	}
    	return $images;
    }
    
    public function draw_atdw_buss_vids($data)
    {
    	$videos = array();
    	
    	if(!empty($data['atdw_bmm_vid_sequence']))
    	{
    		$temp = json_decode($data['atdw_bmm_vid_sequence'], true);
    		if(!empty($temp))
    		{
    			foreach ($temp as $key => $value)
    			{
    				if(!empty($data['atdw_bmm_video-'.$key]))
    				{
    					$videos[] = array(
    										'vid' => $data['atdw_bmm_video-'.$key],
    										'alt' => $data['atdw_bmm_vid_alt-'.$key],
    										'key' => $key
    									);
    				}
    			}
    			
    			if(!empty($videos))
    			{
    				return $videos;
    			}
    		}
    	}
    	
    	$intLoop = 0;
    	foreach($data as $k => $v)
    	{
    		if( (!(strpos($k,"atdw_bmm_video-") === false)) && (!empty($v)) )
    		{
    			$temp = str_replace("atdw_bmm_video-", "", $k);
    			$videos[$intLoop]['vid'] = $v;							
    			$videos[$intLoop]['alt'] = @$data['atdw_bmm_vid_alt-' . $temp];
    			$intLoop++;						
    		}
    	}
    	return $videos;
    }
    
    public function hook_atdw_js($atdw_data)
    { ?>
    	<style type="text/css">
    		#TB_window {width:auto!important;height:auto!important;}
    		#TB_window img#TB_Image {margin:15px!important;}
    	</style>
    	<script type="text/javascript">
    		jQuery(document).ready(function() {
    			jQuery("#screen-options-link-wrap").css("display", "none");
    			jQuery(".hoverEffect").hover(function(){
    				jQuery(this).css("background-color", "#DDD");
    			}, function(){
    				jQuery(this).css("background-color", "");
    			});
    			tb_init('.thickbox_h');
    		});
    	</script>
    	<?php
    }
	}