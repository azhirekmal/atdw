<?php 
if(!class_exists('ATDW_Sync'))
{
  class ATDW_Sync
	{
    private $atdw_api_host, $atdw_api_key, $tripadvisor_key;
    private $execution_count, $current_execution, $document_root, $document_root_url;
    private $entries_sync_limit, $entries_sync_counter, $entries_sync_limit_reached, $atdw_sync_next_city, $atdw_sync_next_id;
    private $multi_lang, $lang_en, $lang_cn_t, $lang_cn_s;
    
    public function __construct()
    {
      $this->atdw_api_host = get_option('atdw_setting_host');
      $this->atdw_api_key = get_option('atdw_setting_key');
      $this->tripadvisor_key = get_option('atdw_tripadvisor_key');
      $wp_upload_dir = wp_upload_dir();
      $this->document_root = $wp_upload_dir['basedir'].'/'.ATDW_PLUGIN_NAME.'/';
      $this->document_root_url = $wp_upload_dir['baseurl'].'/'.ATDW_PLUGIN_NAME.'/';
      $this->entries_sync_limit = 20;
      $this->town_sync_perpage = 5;
      
      //lang codes
      $this->lang_en = "en";
      
      //check if multi lang is enabled
      if(function_exists('icl_object_id')){
        $this->multi_lang = true;
        $this->lang_cn_t = "zh-hant";
        $this->lang_cn_s = "zh-hans";
      }else{
        $this->multi_lang = false;
      }
    } // END public function __construct
    
    private function get_flag(){
      $flag_file = $this->document_root.'flag';
      if (!file_exists($this->document_root) && !is_dir($this->document_root)) {
          mkdir($this->document_root);         
      } 
      if(file_exists($this->document_root . 'flag')){
        $hndlr = fopen($flag_file, 'r+');
      }else{
        $hndlr = fopen($flag_file, 'w+');
      }
      $execute = (int)fread($hndlr, 1000);
      if(!$execute){
        $execute = 1;
      } 
      
      fclose($hndlr);
      return $execute;
    }
      
    private function set_flag(){  
      $flag_file = $this->document_root.'flag';
      if (!file_exists($this->document_root) && !is_dir($this->document_root)) {
          mkdir($this->document_root);         
      } 
      if(file_exists($this->document_root . 'flag')){
        $hndlr = fopen($flag_file, 'r+');
      }else{
        $hndlr = fopen($flag_file, 'w+');
      }
      
      // get current flag
      $execute = $this->get_flag();
      
      if($this->multi_lang){
        // before start, set the language to default
        global $sitepress;
        $sitepress->switch_lang('en');
      }
      
      // get total pages of towns
      $args = array(
      			"fields" => "ids",
      			"post_type" => "towns",
      			"post_status" => "publish",
      			"posts_per_page" => $this->town_sync_perpage
      			);
      $the_query = new WP_Query( $args );
      $this->execution_count = $the_query->max_num_pages;
      
      ftruncate($hndlr, 0);
      rewind($hndlr);
      $write = ( (($execute + 1) > $this->execution_count) ? '1' : ($execute + 1) );
      fwrite($hndlr,  $write);
      fclose($hndlr);
      
      update_option( 'atdw_sync_next_counter', $write, 'no' );
      
      return $execute;
    }
    
    public function sync(){
      echo 'Starting ATDW Listings sync.<br>';
      echo 'Logs will be created at: '.$this->document_root.'<br>';
      
      // setup
      $process_starts_at = time();
      $execute = $this->set_flag();
      $this->current_execution = $execute;
      $execution_count = $this->execution_count;
      $output = null;
      ini_set('memory_limit','2048M');
      ini_set('max_execution_time', '3600');
      
      //to store the name of the next city to sync
      if(!get_option('atdw_sync_next_city')){
        add_option( 'atdw_sync_next_city', null, '', 'no');
        $this->atdw_sync_next_city = null;
      }else{
        $this->atdw_sync_next_city = get_option('atdw_sync_next_city'); 
      }
      
      //to store the inedx of the next entry to sync
      if(!get_option('atdw_sync_next_id')){
        add_option( 'atdw_sync_next_id', null, '', 'no');
        $this->atdw_sync_next_id = null;
      }else{
        $this->atdw_sync_next_id = get_option('atdw_sync_next_id'); 
      }
      
      $output.= "\n\n" . '############################################ STARTS (' . date('Y-m-d H:i:s') . ') ############################################' . "\n";
      if($execute > 0)
      {
      	$output.= "Execution Number : " . $execute . " of " . $execution_count . "\n\n";
      }
      else
      {
      	$output.= "Execution ALL!\n\n";
      	$output.= "SYSTEM IMMEDIATE TERMINATED!\n\n";
      	echo nl2br($output);
      	return;
      }
      
      if($this->multi_lang){
        // before start, set the language to default
        global $sitepress;
        $sitepress->switch_lang('en');
      }
      
      // retriving towns list
      $args = array(
      			"post_type" => "towns",
      			"post_status" => "publish",
      			"posts_per_page" => $this->town_sync_perpage,
      			"paged" => max(1, $execute),
      			"order" => "ASC",
      			"orderby" => "title"
      			);
      $the_query = new WP_Query( $args );
      
      if($execute > 0)
      {
      	$exe_index = 1;
      	$temp_post_count = ((int)$the_query->post_count);
      	$exe_aset = (float)(($temp_post_count) / $execution_count);
      	$exe_min = ceil( $exe_aset * ($execute - 1) );
      	$exe_min = ($exe_min >= 1 ? $exe_min : 1);
      	$exe_max = ceil( $exe_aset * $execute );	
      }
      
      $cities = array();
      $c_i = 0;
      foreach ($the_query->posts as $city_post)
      {
      	if(!empty($city_post->post_title))
      	{
      		$cities[$c_i]['name'] = trim($city_post->post_title);
      		$cities[$c_i]['state'] = get_post_meta($city_post->ID, 'town_state', true);
      		//$region_terms = wp_get_post_terms($city_post->ID, 'towns_cat');
      		//$region_names = wp_list_pluck($region_terms, 'name'); //need to add checking on whether region is empty for this city
      		//$cities[$c_i]['region'] = implode(',', $region_names);
      		$cities[$c_i]['current_index'] = (max(1, $execute) - 1) * $this->town_sync_perpage + $c_i + 1 ;
      		$c_i++;
      	}
      }
      $cities_total = $the_query->found_posts;
      $output.= "\nTotal Cities count: " . $cities_total . "\n";
      
      global $wpdb;
      $new_record = array();
      $update_record = array();
      $privated_record = array();
      
      foreach($cities as $city){
      	$city_name = $city['name'];
      	$city_state = $city['state'];
        //$city_region = $city['region'];
        $city_region = null;
      	$businesses_in_system1 = array();
      	$businesses_in_system2 = array();
        
        // before proceed, check whether sync limit is reached.
        if($this->entries_sync_limit_reached == true){
          break;
        }
        
        // If there's stored city, means the previous execution was stopped half way due to sync limit exceeded. 
        // Resume the execution starting from stored city.
        if($this->atdw_sync_next_city){
          if($city_name != $this->atdw_sync_next_city){
            // This is not the stored city yet, proceed to next city.
            continue;
          }else{
            // This is the city, and we'll restart from here.
            // Clear the stored city value
            $this->atdw_sync_next_city = null;
            update_option( 'atdw_sync_next_city', null, 'no' );
          }
        }        
        
          $output.= "\n Processing for: " . $city_name . " (State: " . $city_state .") [current city index: ". $city['current_index'] ."][total: ". $cities_total ."]";
          
          $businesses_in_system1 = $wpdb->get_results('SELECT wp_posts.post_excerpt, wp_posts.ID FROM wp_posts, wp_postmeta WHERE wp_posts.ID = wp_postmeta.post_id AND ( wp_posts.post_type IN ("atdw_accomm","atdw_attraction","atdw_event","atdw_tour","atdw_genservice","atdw_restaurant"	) ) AND wp_postmeta.meta_key = "atdw_pr_city" AND wp_postmeta.meta_value LIKE "' . $city_name . '"', ARRAY_A);
          
          foreach($businesses_in_system1 as $businesses_insys)
          {
            $businesses_in_system2[(str_replace(array("atdw_buss_id||","<p>","</p>", "&lt;/p&gt;", "&lt;p&gt;","\n","\r"), "", $businesses_insys['post_excerpt']))] = $businesses_insys['ID'];
          }
          
          unset($businesses_in_system1);

          $output.= "\n  [ current ]  Active business count: " . count($businesses_in_system2) . "\n";
          
          $process_this_city = $this->process_this_city($city_name, $city_region, $city_state, $businesses_in_system2);
          
          // Logs
          $output.= $process_this_city['output'];
          if(isset($process_this_city['new_record'])){
            $new_record = array_merge($new_record, $process_this_city['new_record']);
          }
          if(isset($process_this_city['update_record'])){
            $update_record = array_merge($update_record, $process_this_city['update_record']);
          }
          if(isset($process_this_city['privated_record'])){
            $privated_record = array_merge($privated_record, $process_this_city['privated_record']);
          }
        
        $exe_index++;
        
      }
      
      //just in case some of them just break before.
      $updated_closed = $wpdb->query("UPDATE wp_posts SET post_status = 'private' WHERE post_title LIKE 'ATDW In Progress%'");
      $output.= "\n Done : PRIVATE ALL INCOMPLETED BUSINESS. - Affected : " . $updated_closed;
      $output.= "\n\n" . '###################################################  SUMMARY  ###################################################';
      $output.= "\n\n" . 'New Record' . (count($new_record) > 1 ? 's' : '') . ': ' . count($new_record) . "\n";
      $output.= 'Updated Record' . (count($update_record) > 1 ? 's' : '') . ': ' . count($update_record) . "\n";
      $output.= 'Privated Record' . (count($privated_record) > 1 ? 's' : '') . ': ' . count($privated_record);
      $output.= "\n\n" . '#### New Records as follows : ' . count($new_record) . ' Record' . (count($new_record) > 1 ? 's' : '') . ".\n";
      $output.= print_r($new_record, true);
      $output.= "\n\n" . '#### Record Updated as follows : ' . count($update_record) . ' Record' . (count($update_record) > 1 ? 's' : '') . ".\n";
      $output.= print_r($update_record, true);
      $output.= "\n\n" . '#### Privated Records as follows : ' . count($privated_record) . ' Record' . (count($privated_record) > 1 ? 's' : '') . ".\n";
      $output.= print_r($privated_record, true);
      
      $output.= "\n\n--STARTING ALIGNMENT - THE GHOST KEY--\n";	
      $all_posts = $wpdb->get_results("SELECT ID FROM wp_posts WHERE (post_type = 'atdw_accomm' OR post_type = 'atdw_attraction' OR post_type = 'atdw_event' OR post_type = 'atdw_tour' OR post_type = 'atdw_genservice' OR post_type = 'atdw_restaurant') AND post_status = 'publish';", ARRAY_A);	
      $all_have = $wpdb->get_results("SELECT wp_posts.ID FROM wp_posts,wp_postmeta WHERE wp_posts.ID = wp_postmeta.post_id AND (wp_posts.post_type = 'atdw_accomm' OR wp_posts.post_type = 'atdw_attraction' OR wp_posts.post_type = 'atdw_event' OR wp_posts.post_type = 'atdw_tour' OR wp_posts.post_type = 'atdw_genservice' OR wp_posts.post_type = 'atdw_restaurant') AND wp_posts.post_status = 'publish' AND wp_postmeta.meta_key = 'atdw_booking_button' AND wp_postmeta.meta_value IS NOT NULL;", ARRAY_A);	
      $output.= 'All published business : ' . count($all_posts) . "\n";
      $output.= 'Assigned Business : ' . count($all_have) . "\n";
      $output.= 'Processing Ghost Key for : ' . (((int)count($all_posts)) - ((int)count($all_have))) . "\n";

      $gkey[0] = 0;
      $gkey[1] = 0;
      foreach($all_posts as $index => $all_post)
      {
      	if( in_array($all_post, $all_have) )
      	{
      		update_post_meta($all_post['ID'], 'atdw_booking_button_int', 1);
      		$gkey[0]++;
      	}
      	else
      	{
      		update_post_meta($all_post['ID'], 'atdw_booking_button_int', 0);
      		$gkey[1]++;
      	}	
      }

      $output.= '--completed-- ' . $gkey[0] . ' GHOST KEY 1 updated.' . "\n";
      $output.= '--completed-- ' . $gkey[1] . ' GHOST KEY 0 updated.' . "\n\n";

      $output.= "\n\n" . '===COMPLETED=== ' . date('Y-m-d H:i:s') . ' =============' . "\n";

      $process_starts_at = ((time()) - $process_starts_at);
      $output.=  $process_starts_at . " Second" . ($process_starts_at > 1 ? 's' : '') . "\n\n";
      $output.= "\nSystem terminated. \n\n ATDW Wordpress Plugin";
      
      // saving into log files
      // create folder if yet exist
      if (!file_exists($this->document_root . 'logs/') && !is_dir($this->document_root . 'logs/')) {
      	mkdir($this->document_root . 'logs/');
      }
      if (!file_exists($this->document_root . 'logs/executed/') && !is_dir($this->document_root . 'logs/executed/')) {
        mkdir($this->document_root . 'logs/executed/');
      }
      //if (!file_exists($this->document_root . 'logs/atdw/') && !is_dir($this->document_root . 'logs/atdw/')) {
      //  mkdir($this->document_root . 'logs/atdw/');
      //}

      $cronFile = $this->document_root . 'logs/executed/' . date("Y_m_d") . '.log';
      $fh = fopen($cronFile, 'a') or die("can't open file");
      fwrite($fh, $output);
      fclose($fh);

      if($execute == $execution_count)
      {
      	//$new_name = $this->document_root . 'logs/atdw/' . date("Y_m_d") . '.zip';
      	//if(file_exists($new_name))
      	//{
      	//	$new_name = $this->document_root . 'logs/atdw/' . date("Y_m_d") . '_' . time() . '.zip';
      	//}

      	//$this->Zip($this->document_root . 'logs/atdw/' . date("Y_m_d") . '/', $new_name);
      	//$output.= "\n\n\nATDW LOG FILE: " . $new_name;
      	$output.= "\n\n\nRESULT LOG FILE: " . $this->document_root . 'logs/executed/' . date("Y_m_d") . '.log';

      	if(function_exists('w3tc_pgcache_flush')) 
      	{
      		w3tc_pgcache_flush();
      		w3tc_dbcache_flush();
      		w3tc_minify_flush();
      		w3tc_objectcache_flush();
      	} 
      	elseif(function_exists('wp_cache_clear_cache')) 
      	{
      		wp_cache_clear_cache();
      	}
      }
      
      // output to page
      echo nl2br($output);
    }
    
    private function process_this_city($city, $region, $state, $businesses_in_system2){
      global $wpdb;
      $return = array();
      $return['new_record'] = array();
      $return['update_record'] = array();
      $output = "\n";
      
      // create new folder for logs
      if (!file_exists($this->document_root . 'logs/') && !is_dir($this->document_root . 'logs/')) {
        mkdir($this->document_root . 'logs/');
        //mkdir($this->document_root . 'logs/atdw/');
      }
      if (!file_exists($this->document_root . 'logs/atdw/' . date("Y_m_d") . '/') && !is_dir($this->document_root . 'logs/atdw/' . date("Y_m_d") . '/')) {
        //mkdir($this->document_root . 'logs/atdw/' . date("Y_m_d") . '/');
        //mkdir($this->document_root . 'logs/atdw/' . date("Y_m_d") . '/entries/');
      }
      
      $strURL	= 'http://' . $this->atdw_api_host . '/api/atlas/products?key=' . $this->atdw_api_key . '&st='. $state .'&ct=' . str_replace(' ', '%20', $city) . '&size=300';
      $output.= "Querying entries from: ".$strURL."\n";
      $strXML	= @file_get_contents(str_replace(' ', '%20', $strURL));
      if(!$strXML){
        $strXML = $this->get_web_page(str_replace(' ', '%20', $strURL));
      }
      //$cronFile = $this->document_root . "logs/atdw/" . date("Y_m_d") . "/" . date("Y_m_d") . "_" . $city . ".atdw";
      //$fh = fopen($cronFile, 'w') or die("can't open log file");
      //fwrite($fh, $strXML);
      //fclose($fh);
      
      $checkXML = $this->isXML($strXML);
      if($checkXML === true){
        $businesses_list = new SimpleXMLElement($strXML);
        $businesses_list = $this->object2array($businesses_list);
      }else{
        $businesses_list = null;
        $output.= " [Error reading XML (". $strURL ."): ". $checkXML ."]";
      }
      
      // API call to get Booking URLs. 
      $strURL_burl	= 'http://' . $this->atdw_api_host . '/api/atlas/products?key=' . $this->atdw_api_key . '&st='. $state .'&ct=' . str_replace(' ', '%20', $city) . '&size=300&fl=comms_burl';
      $output.= "Querying Booking URLs from: ".$strURL_burl."\n";
      $strXML_burl	= @file_get_contents(str_replace(' ', '%20', $strURL_burl));
      if(!$strXML_burl){
        $strXML_burl = $this->get_web_page(str_replace(' ', '%20', $strURL_burl));
      }
      $checkXML = $this->isXML($strXML_burl);
      if($checkXML === true){
        $burl_list = new SimpleXMLElement($strXML_burl);
        $burl_list = $this->object2array($burl_list);
      }else{
        $burl_list = null;
        $output.= " [Error reading XML (". $strXML_burl ."): ". $checkXML ."]";
      }
      $output.= "\n";
      
      // ATDW categories that to be excluded from this sync
      $remove_cats = array('HIRE', 'TRANSPORT', 'JOURNEY', 'INFO', 'DESTINFO');
      
      if(((int)$businesses_list['number_of_results']) > 0)
      {
      	$output.= "  [   ATDW  ]  Active business count: " . $businesses_list['number_of_results'] . " \n\n";
      	
      	if(((int)$businesses_list['number_of_results']) == 1)
      	{
      		if(
      			(!in_array($businesses_list['products']['product_record']['product_category_id'], $remove_cats, false))
      		)
      		{
      			$process_this_business = $this->process_this_business(
      								$businesses_list['products']['product_record']['product_id'], 
      								$businesses_list['products']['product_record']['product_name'], 
      								$businesses_list['products']['product_record']['product_category_id'], 
      								((!isset($businesses_in_system2[ $businesses_list['products']['product_record']['product_id'] ])) ? 'new' : 'update'),
      								(!empty($businesses_in_system2[ $businesses_list['products']['product_record']['product_id'] ]) ? $businesses_in_system2[ $businesses_list['products']['product_record']['product_id'] ] : 0),
      								$burl_list['products']['product_record'],
      								$region,
      								$city
      							);
            // Logs
            $output.= $process_this_business['output'];
            if(isset($process_this_business['new_record'])){
              $return['new_record'] = array_merge($return['new_record'], $process_this_business['new_record']);
            }
            if(isset($process_this_business['update_record'])){
              $return['update_record'] = array_merge($return['update_record'], $process_this_business['update_record']);
            }
      		}
      		else
      		{
      			$output.= "\t" . "IGNORED = " . $businesses_list['products']['product_record']['product_id'] . " = " . 
      				$businesses_list['products']['product_record']['product_category_id'] . " | " .
      				$businesses_list['products']['product_record']['product_name'] . "\n";			
      		}
      		// for default language
      		$businesses_in_system2[ $businesses_list['products']['product_record']['product_id'] ] = 'done';
          if($this->multi_lang){
            // for traditional chinese
            $businesses_in_system2[ 'atdw_buss_id_cn_t||'.$businesses_list['products']['product_record']['product_id'] ] = 'done';
            // for simplified chinese
            $businesses_in_system2[ 'atdw_buss_id_cn_s||'.$businesses_list['products']['product_record']['product_id'] ] = 'done';
          }
      	}
      	elseif(((int)$businesses_list['number_of_results']) > 1)
      	{
      		foreach($businesses_list['products']['product_record'] as $index => $business_atdw)
      		{
      			if(
      				(!in_array($business_atdw['product_category_id'], $remove_cats, false))
      			)
      			{
      				$process_this_business = $this->process_this_business(
      										$business_atdw['product_id'], 
      										$business_atdw['product_name'], 
      										$business_atdw['product_category_id'], 
      										((!isset($businesses_in_system2[ $business_atdw['product_id'] ])) ? 'new' : 'update'),
      										(!empty($businesses_in_system2[ $business_atdw['product_id'] ]) ? $businesses_in_system2[ $business_atdw['product_id'] ] : 0),
      										$burl_list['products']['product_record'],
      										$region,
      										$city
      									);
              // Logs
              $output.= $process_this_business['output'];
              if(isset($process_this_business['new_record'])){
                $return['new_record'] = array_merge($return['new_record'], $process_this_business['new_record']);
              }
              if(isset($process_this_business['update_record'])){
                $return['update_record'] = array_merge($return['update_record'], $process_this_business['update_record']);
              }
      			}
      			else
      			{
      				$output.= "\t" . "IGNORED = " . $business_atdw['product_id'] . " = " . 
      					$business_atdw['product_category_id'] . " | " .
      					$business_atdw['product_name'] . "\n";
      				
      			}
      			// for default language
            $businesses_in_system2[ $business_atdw['product_id'] ] = 'done';
            if($this->multi_lang){
              // for traditional chinese
              $businesses_in_system2[ 'atdw_buss_id_cn_t||'.$business_atdw['product_id'] ] = 'done';
              // for simplified chinese
              $businesses_in_system2[ 'atdw_buss_id_cn_s||'.$business_atdw['product_id'] ] = 'done';
            }
      		}		
      	}

      	$have_privated = false;
      	foreach($businesses_in_system2 as $product_id => $post_id)
      	{
      		//for records that already exist in current db, but not available in ATDW on this sync
      		if($post_id != 'done')
      		{
      			//get entry details, to write into log
      			$temp = $wpdb->get_row('SELECT post_title, post_type FROM wp_posts WHERE ID = ' . $post_id . ' LIMIT 1;', ARRAY_A);
      			
      			//check this entry's settings, whether "Exclude this entry from ATDW sync" is turned on
      			//if "Exclude this entry from ATDW sync" is turned on, do not update this entry as "Private"
      			$temp_sync_check_query = "SELECT meta_value FROM wp_postmeta WHERE post_id = ".$post_id." AND meta_key = 'atdw_sync_toggle'";
      			$temp_sync_check = $wpdb->get_var($temp_sync_check_query);
      			if($temp_sync_check == '1'){
      				//exclude this entry for the ATDW sync process
      				$output.= "\t" . 'NOT FOUND IN ADTW BUT KEEP IN SYSTEM (EXCLUDED FROM SYNC) = ' . $product_id . " = "  . $temp['post_type'] . " | " . $temp['post_title'] . "\n";
      			}else{
      				$return['privated_record'][$product_id] = $temp['post_title'];
      				$output.= "\t" . 'PRIVATE = ' . $product_id . " = "  . $temp['post_type'] . " | " . $temp['post_title'] . "\n";
      				
      				$wpdb->update('wp_posts', 
      								array(
      										'post_status' => 'private',
      										'post_modified'	=> date('Y-m-d H:i:s', (time() - 86400)),
      										'post_modified_gmt'	=> date('Y-m-d H:i:s', (time() - 86400))
      									),
      								array(
      										'ID' => $post_id
      									)
      							);
      				$have_privated = true;
      			}			
      		}
      	}

      	if($have_privated)
      	{
      		foreach($businesses_in_system2 as $product_id => $post_id)
      		{
      			if( ($post_id != 'done') && ( !(strpos($strXML, '<product_id>' . $product_id . '</product_id>') === false) ) )
      			{
      				$return['privated_record'][$product_id] = $return['privated_record'][$product_id] . " | FAILED READING XML!";
      				$output.= 'XXXXXXXXXXXXXXXXXXXXXXXXXXXX DIAG! XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
      				$output.= '## XML ##';
      				//print_r($strXML);
      			}
      		}		
      	}
      	
      }
      else
      {
      	$output.= "  [   ATDW  ]  Active business count: 0 \n\n";
      }
      
      // for logging
      $return['output'] = $output;
      
      return $return;
    }
    
    private function process_this_business($buss_id, $buss_name, $cat_id, $action = 'update', $post_id, $booking_urls = null, $region = null, $city = null)
    {
    	global $wpdb;
    	$return = array();
      $output = null;
      
      // before proceed, check whether sync limit is reached.
      if($this->entries_sync_limit_reached == true){
        return $return;
      }
      
      // If there's stored id, means the previous execution was stopped half way due to sync limit exceeded. 
      // Resume the execution starting from stored id.
      if($this->atdw_sync_next_id){  
        if($buss_id != $this->atdw_sync_next_id){
          // This is not the stored entry yet, proceed to next entry.
          return $return;
        }else{
          // This is the item, and we'll restart from here.
          // Clear the stored index value
          $this->atdw_sync_next_id = null;
          update_option( 'atdw_sync_next_id', null, 'no' );
        }
      }
      
      // add to overall sync counter
      if($this->atdw_sync_next_id == null){
        $this->entries_sync_counter++;
      }

    	$time_start = $this->microtime_float();
    	$string_name = ($action == 'new' ? 'new_record' : 'update_record');

    	$return[$string_name][$buss_id] = $cat_id . " | " .	$buss_name;
    	$output.= "\t" . strtoupper($action) . " = " . $buss_id . " = " . $cat_id . " | " . $buss_name;
      
      //check the customsync exclude list
      //if the entry is in the exclude list, do no sync it, and update the entry's status to "private"
      $atdw_customsync_list_exclude = (get_option('atdw_customsync_list_exclude')) ? get_option('atdw_customsync_list_exclude') : array();
      if( in_array($buss_id, $atdw_customsync_list_exclude, false) ){
        $output.= "\n\n[ The entry of ".$buss_name." (".$buss_id.") is found in custom sync exclude list. This entry will not be synced. ";
        if($post_id){
          //get the IDs for each language
          $get_multilang_posts = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_excerpt LIKE '%||" . $buss_id . "';");
          foreach ($get_multilang_posts as $get_multilang_post) {
            //update the entry's status to "private"
            $setprivate = $wpdb->update('wp_posts', 
          								array(
          										'post_status' => 'private',
          										'post_modified'	=> date('Y-m-d H:i:s', (time() - 86400)),
          										'post_modified_gmt'	=> date('Y-m-d H:i:s', (time() - 86400))
          									),
          								array(
          										'ID' => $get_multilang_post->ID
          									)
          							);
            if($setprivate){
              $output.= "The status for this entry (post id: ".$get_multilang_post->ID.") has been set to \"private\". ";
            }
          }
        }
        $output.= "]\n\n";
        
        $return['output'] = $output;        
        return $return;
      }

    	//check this entry's settings, whether "Exclude this entry from ATDW sync" is turned on
    	$temp_sync_check_query = "SELECT meta_value FROM wp_postmeta WHERE post_id = (select ID from wp_posts where post_excerpt like '%atdw_buss_id||" . $buss_id . "%' LIMIT 1) AND meta_key = 'atdw_sync_toggle'";
    	$temp_sync_check = $wpdb->get_var($temp_sync_check_query);
    	if($temp_sync_check == '1'){
    		//exclude this entry for the ATDW sync process
    	}else{
    		$output.= "\n\n[--Processing English content--]\n";
    		$process_this_business_ext = $this->process_this_business_ext($buss_id, $cat_id, $booking_urls, $region, $city);
    		$output.= $process_this_business_ext['output'];
        
    		if($this->multi_lang){
    		  // process for other languages: traditional chinese
    		  $output.= "\n[--Processing Traditional Chinese content--]\n";
    		  $process_this_business_ext_cn_t = $this->process_this_business_ext_lang($buss_id, $cat_id, $booking_urls, $region, $city, $this->lang_cn_t);
    		  $output.= $process_this_business_ext_cn_t['output'];
          
    		  // process for other languages: Simplified chinese
    		  $output.= "\n[--Processing Simplified Chinese content--]\n";
    		  $process_this_business_ext_cn_s = $this->process_this_business_ext_lang($buss_id, $cat_id, $booking_urls, $region, $city, $this->lang_cn_s);
    		  $output.= $process_this_business_ext_cn_s['output'];
    		}
        
    		$time_end = $this->microtime_float();
    		$time = $time_end - $time_start;
    		$output.= "\t\t- data ". ($action == 'new' ? 'stored' : 'updated') . " in " . $time . " second" . ($time > 1 ? 's' : '') . "\n\n";
    	}
      
      // for logging
      $return['output'] = $output;
      
      return $return;
    }
    
    private function process_this_business_ext($buss_id, $cat_id, $booking_urls, $region = null, $city = null)
    {
      global $wpdb;
      $the_business_post = array();
      $business = array();
      $return = array();
      $output = null;
      
      $output.= ' [Syncing entry #'.$this->entries_sync_counter." of current execution]";
      
      // before sync this individual entry, first check whether the sync limit have been reached
      if( $this->entries_sync_counter > $this->entries_sync_limit ){
        // limit reached, save into system
        $this->entries_sync_limit_reached = true;
        //update_option( 'entries_sync_limit_reached', true, 'no' ); 
        update_option( 'atdw_sync_next_city', $city, 'no' );
        update_option( 'atdw_sync_next_id', $buss_id, 'no' );
        
        // output message
        $output.= "\n\nSync limit (".$this->entries_sync_limit.") exceeded. Stopping execution now. Progress will be saved for next execution. \n\n";
        // update the execution flag
        $temp_flag_file = $this->document_root.'flag';
        $temp_hndlr = fopen($temp_flag_file, 'w+');
        $temp_write = $this->current_execution;
        update_option( 'atdw_sync_next_counter', $temp_write, 'no' );
        fwrite($temp_hndlr,  $temp_write);
        fclose($temp_hndlr);
        
        $return['output'] = $output; return $return;
      }
      
      if(empty($buss_id))
      {
      	$output.= '---XXXX--error obtaining product id-----XXXXX-----';
        // for logging
        $return['output'] = $output;
        return $return;
      }
      
      $the_business_post['atdw_buss_id'] = $buss_id;
      switch($cat_id){
      	case 'ACCOMM': 
      		$the_business_post['post_type'] = 'atdw_accomm';
      		$the_business_post['taxonomy'] = 'atdw_accomm_cat';
      		$the_business_post['slug'] = 'stay';
      		break;
      	case 'ATTRACTION': 
      		$the_business_post['post_type'] = 'atdw_attraction';
      		$the_business_post['taxonomy'] = 'atdw_attraction_cat';
      		$the_business_post['slug'] = 'see-and-do';
      		break;
      	case 'EVENT': 
      		$the_business_post['post_type'] = 'atdw_event';
      		$the_business_post['taxonomy'] = 'atdw_event_cat';
      		$the_business_post['slug'] = 'events';
      		break;
      	case 'TOUR': 
      		$the_business_post['post_type'] = 'atdw_tour';
      		$the_business_post['taxonomy'] = 'atdw_tour_cat';
      		$the_business_post['slug'] = 'tours-and-trails';
      		break;
      	case 'GENSERVICE': 
      		$the_business_post['post_type'] = 'atdw_genservice';
      		$the_business_post['taxonomy'] = 'atdw_genservice_cat';
      		$the_business_post['slug'] = 'professional-services';
      		break;  
      	case 'RESTAURANT': 
      		$the_business_post['post_type'] = 'atdw_restaurant';
      		$the_business_post['taxonomy'] = 'atdw_restaurant_cat';
      		$the_business_post['slug'] = 'food-and-wine';
      		break;
      	default:
      		$the_business_post['post_type'] = 'atdw_others';
      		$the_business_post['taxonomy'] = 'atdw_others_cat';
      		break;
      }
      
      $the_business_post['post_content'] = "";
      $the_business_post['post_title'] = "";
      $the_business_post['post_name'] = "";
      //$the_business_post['guid'] = "";
      
      // Check whether the entry is already imported into the site
      if($this->multi_lang){
        // Get default language. Requires WPML plugin to be installed and activated first.
        $default_lang = $this->lang_en;
        $theid = $wpdb->get_var("SELECT wp_icl_translations.element_id FROM wp_posts LEFT JOIN wp_icl_translations ON wp_icl_translations.trid = wp_posts.ID WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' AND language_code = '" . $default_lang . "' LIMIT 1;");	
        // If default language translation is not created yet, eg encountered interuption during last execution (post_title will be saved as "ATDW In Progress")
        if(empty($theid)){
          $theid = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' LIMIT 1;");	
        }
      }else{
        $theid = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' LIMIT 1;");	
      }

      if(empty($theid))
      {
      	$post = array(
      				  'comment_status' 	=> 'closed',
      				  'ping_status' 	=> 'closed',
      				  'post_author' 	=> 1,
      				  'post_content' 	=> '',
      				  'post_date' 		=> date('Y-m-d H:i:s', (time() - 86400)),
      				  'post_date_gmt' 	=> date('Y-m-d H:i:s', (time() - 86400)),
      				  'post_excerpt' 	=> 'atdw_buss_id||' . $the_business_post['atdw_buss_id'],
      				  'post_name' 		=> $the_business_post['atdw_buss_id'],
      				  'post_parent' 	=> 0,
      				  'post_status' 	=> 'publish',
      				  'post_title' 		=> 'ATDW In Progress - ' . $the_business_post['atdw_buss_id'],
      				  'post_type' 		=> $the_business_post['post_type']
      				);  
      	$theid = wp_insert_post( $post );
      	update_post_meta($theid, 'atdw_booking_button_int', '0');
      }
      else
      {
      	wp_set_object_terms( $theid, NULL, $the_business_post['taxonomy'] );	
      }

      $the_business_post['ID'] = $theid;
      
      //find the records in the wp_postmeta for this ID, and set the meta_value for all to blank
      $temp_query = $wpdb->query( $wpdb->prepare(
      	"UPDATE wp_postmeta SET meta_value = %s WHERE post_id = %d AND meta_key != %s AND meta_key != %s AND meta_key != %s AND meta_key != %s AND meta_key != %s AND (meta_key like %s or meta_key like %s)",
      	"",
      	(int)$the_business_post['ID'],
      	"atdw_booking_button",
      	"atdw_booking_button_int",
        "atdw_pr_city",
        "atdw_pr_region",
        "atdw_pr_state",
        "atdw_%",
        "external_system_%"
      ) );

      // english content
      $strURL	= 'http://' . $this->atdw_api_host . '/api/atlas/product?key=' . $this->atdw_api_key . '&productId=' . $the_business_post['atdw_buss_id'] ;
      $strXML	= @file_get_contents(str_replace(' ', '%20', $strURL));
      if(!$strXML){
        $strXML = $this->get_web_page(str_replace(' ', '%20', $strURL));
      }
      
      if(!$strXML)
      {
      	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err Code: 404] \n\n";
        // for logging
        $return['output'] = $output;
        return $return;
      }elseif( (strlen($strXML) == 216) || ($strXML == "{\"error\":{\"name\":\"TypeError\",\"status\":500,\"message\":\"Cannot assign to read only property 'status' of illegal access\"}}") )
      {
      	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err Code: 500] \n\n";
        // for logging
        $return['output'] = $output;
        return $return;
      }

      //create log, as reference for future debug
      //$atdw_entry_file = $this->document_root . "logs/atdw/" . date("Y_m_d") . "/entries/" . date("Y_m_d") . "_" . $the_business_post['atdw_buss_id'] . ".atdw";
      //$fae = fopen($atdw_entry_file, 'w') or die("can't open log file");
      //fwrite($fae, $strXML);
      //fclose($fae);

      // english content
      $checkXML = $this->isXML($strXML);
      if($checkXML === true){
        $buss_info = new SimpleXMLElement($strXML);
        $buss_info = $this->object2array($buss_info);
      }else{
        $buss_info = null;
        $output.= " [Error reading XML (". $strURL ."): ". $checkXML ."]";
      }

      if(empty($buss_info['product_distribution'])) 
      {
      	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err: product_distribution is empty ] \n\n";
        // for logging
        $return['output'] = $output;
        return $return; 
      } 
      $business['infos'] 		= $buss_info['product_distribution']['product_record']; 				//details
      $business['addresses'] 	= $buss_info['product_distribution']['product_address']; 				//location + postal ++
      $business['contacts'] 	= $buss_info['product_distribution']['product_communication']; 			//phone + email + faxs + etc
      $business['externals'] 	= $buss_info['product_distribution']['product_external_system']; 		//external links & socials
      $business['vertical']	= $buss_info['product_distribution']['product_vertical_classification']; //verticals
      $business['attributes'] = $buss_info['product_distribution']['product_attribute']; 				//facilities + expirience ++
      $business['images'] 	= $buss_info['product_distribution']['product_multimedia']; 			//imges jpg + png + gif
      $business['videos'] 	= $buss_info['product_distribution']['product_multimedia']; 			//video
      $business['entry_cost'] 	= $buss_info['product_distribution']['product_entry_cost']; 			//entry cost
      $business['event_frequency'] 	= $buss_info['product_distribution']['event_frequency']; 			//event dates
      unset($strXML);
      unset($buss_info);
      
      /*
      *
      *
      	=============================================== INFO ======================================
      *
      *
      */

      $section = 'infos';
      $standard = array(
      				"frequency_start_date" 					=> "atdw_pr_event_starts",
      				"frequency_end_date" 					=> "atdw_pr_event_ends",
      				"attribute_id_frequency_description" 	=> "atdw_pr_event_frequency",
      				"attribute_id_event_status_description" => "atdw_pr_event_status",
      				"number_of_rooms" 						=> "atdw_pr_room_number",
      				"city_name" 							=> "atdw_pr_city",
      				"state_name" 							=> "atdw_pr_state",
      				"product_description" 					=> "atdw_pr_product_description",
      				"product_short_description" 					=> "atdw_pr_product_short_description",
      				"reception_hours" 						=> "atdw_pr_reception_hours",
      				"reception_hours_text" 					=> "atdw_pr_reception_hours_txt",
      				"australian_business_number" 			=> "atdw_pr_abn"
      			);
      $theArray = $this->test_array_index($business[$section], 'product_name');
      
      // the next event date
      $event_next_occurrence = $business['infos']['next_occurrence'];
      // the overall event start date
      $event_overall_start = $business['infos']['frequency_start_date'];
      // frequency type
      $attribute_id_frequency = $business['infos']['attribute_id_frequency'];
      
      unset($business[$section]);
      
      foreach($theArray as $key => $info)
      {
      	if(isset($standard[$key]))
      	{
      		if(!empty($info)){
      			update_post_meta($the_business_post['ID'], @$standard[$key], $info);
      		}
      	}
      	else
      	{
      		switch($key)
      		{
      			case "product_name":
      				$the_business_post['post_title'] = $info;
      				$the_business_post['post_name'] = sanitize_title($info);
      				
      				//need the "ATDW Service Type Name", eg "BAR", which the slug will become "clubs-pubs-bars-and-bistros"
      				//$the_business_post['guid'] = $settings['site_url'] . '/' . $the_business_post['slug'] . '/' . $the_business_post['service_type'] . '/' . $the_business_post['post_name'] . '/';
      				
      				update_post_meta($the_business_post['ID'], 'atdw_pr_product_name', $info);
      				break;
      			
      			case "product_description":
      				$the_business_post['post_content'] = sanitize_title($info);
      				update_post_meta($the_business_post['ID'], 'atdw_pr_product_description', $info);
      				break;				
      			
      			case "rate_from":
      				update_post_meta($the_business_post['ID'], 'atdw_pr_all_rate_from', $this->cron_validate_amount($info));
      				break;
      			
      			case "rate_to":
      				update_post_meta($the_business_post['ID'], 'atdw_pr_all_rate_to', $this->cron_validate_amount($info));
      				break;
      			
      			case "children_catered_for_flag":					
      				update_post_meta($the_business_post['ID'], 'atdw_pr_children_catered', $this->cron_validate_yes($info));
      				break;
      				
      			case "pet_allowed":
      			case "pets_allowed_flag":
      				update_post_meta($the_business_post['ID'], 'atdw_pr_pet_allowed', $this->cron_validate_yes($info));
      				break;
      				
      			case "check_in_time";
      				update_post_meta($the_business_post['ID'], 'atdw_pr_check-in_time', $this->cron_validate_time($info));
      				break;
      							
      			case "check_out_time";
      				update_post_meta($the_business_post['ID'], 'atdw_pr_check-out_time', $this->cron_validate_time($info));
      				break;
      			
      			default :
      				update_post_meta($the_business_post['ID'], 'atdw_unknown_' . $key, $info);
      		}
      	}	
      }
      
      //update booking URLs
      foreach ($booking_urls as $booking_url) {
      	if( isset($booking_url['product_id']) && ($booking_url['product_id'] == $the_business_post['atdw_buss_id']) ){
      		if(isset($booking_url['comms_burl']) && !empty($booking_url['comms_burl'])){
      			update_post_meta($the_business_post['ID'], 'atdw_booking_button', $booking_url['comms_burl']);
      			update_post_meta($the_business_post['ID'], 'atdw_booking_button_int', '1');
      			break;
      		}
      	}
      }
      
      //update Region
      if($region){
        update_post_meta($the_business_post['ID'], 'atdw_pr_region', $region);
      }

      unset($theArray);
      unset($standard);

      /*
      *
      *
      	=============================================== ADDRESSES ======================================
      *
      *
      */
      $section = 'addresses';	
      $theArray = $this->test_array_index($business[$section], 'address_line_1');
      unset($business[$section]);

      foreach($theArray as $index => $address)
      {
      	$address = (array)$address;

      	$addressX = '';
      	$addressX .= ( isset($address['address_line_1']) ? $this->cron_validate_string_w_delimiter($address['address_line_1']) : null );
      	$addressX .= ( isset($address['address_line_2']) ? $this->cron_validate_string_w_delimiter($address['address_line_2']) : null );
      	$addressX .= ( isset($address['address_line_3']) ? $this->cron_validate_string_w_delimiter($address['address_line_3']) : null );
      	$addressX .= ( isset($address['city_name']) ? $this->cron_validate_string_w_delimiter($address['city_name']) : null );
      	$addressX .= ( isset($address['state_name']) ? $this->cron_validate_string_w_delimiter($address['state_name']) : null );
      	$addressX .= ( (isset($address['address_postal_code']) && (!is_array($address['address_postal_code'] ))) ? $address['address_postal_code'] : null );

      	if($address['attribute_id_address'] == 'PHYSICAL')
      	{
      		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_addr', $addressX);
      		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_lat', $address['geocode_gda_latitude']);
      		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_long', $address['geocode_gda_longitude']);				
      	}
      	elseif($address['attribute_id_address'] == 'POSTAL')
      	{
      		update_post_meta($the_business_post['ID'], 'atdw_pr_postal', $addressX);
      	}
      }
      unset($theArray);	


      /*
      *
      *
      	=============================================== CONTACTS ======================================
      *
      *
      */
      $section = 'contacts';
      $theArray = $this->test_array_index($business[$section], 'attribute_id_communication');
      unset($business[$section]);

      $standard = array(
      				"CAEMENQUIR" => "atdw_pr_comm_email",
      				"CAURENQUIR" => "atdw_pr_comm_website",
      				"CAPHENQUIR" => "atdw_pr_comm_phone",
      				"CAFXENQUIR" => "atdw_pr_comm_fax",
      				"CAMBENQUIR" => "atdw_pr_comm_mobile",
      				"CATLENQUIR" => "atdw_pr_comm_tollfree",
      				"CAUBENQUIR" => "atdw_pr_comm_booking"
      			);

      foreach($theArray as $index => $comm)
      {
      	$comm = (array)$comm;
      	if(isset($standard[$comm['attribute_id_communication']]))
      	{
      		if($comm['attribute_id_communication'] == 'CAPHENQUIR'){
      			//formating the spaces of the phone number
      			$temp_phone = preg_replace('/\s+/', '',$comm['communication_detail']);
      			if(strlen($temp_phone) == 8){
      				if($comm['communication_area_code'] == "04"){
      					$comm['communication_detail'] = substr($temp_phone,0,2)." ".substr($temp_phone,2,3)." ".substr($temp_phone,5,3);
      				}else{
      					$temp_phone_arr = str_split($temp_phone,4);
      					$comm['communication_detail'] = ' ';
      					$comm['communication_detail'] .= implode(" ", $temp_phone_arr);
      				}
      			}
      		}
      		//area code if any
      		$value = (isset($comm['communication_area_code']) ? $comm['communication_area_code'] : '') . $comm['communication_detail'];
      		update_post_meta($the_business_post['ID'], $standard[$comm['attribute_id_communication']], $value);
      	}
      	else
      	{
      		update_post_meta($the_business_post['ID'], $standard[$comm['attribute_id_communication']], '');
      	}
      }
      unset($theArray);
      unset($standard);

      /*
      *
      *
      	=============================================== EXTERNAL LINKS ======================================
      *
      *
      */
      $section = 'externals';		
      $theArray = $this->test_array_index($business[$section], 'external_system_code');
      unset($business[$section]);

      $indexOrder1 = 1;
      foreach($theArray as $index => $comm)
      {
      	$comm = (array)$comm;
      	if(isset($comm['external_system_code'])){
          update_post_meta($the_business_post['ID'], 'external_system_code-' . $indexOrder1, $comm['external_system_code']);
          update_post_meta($the_business_post['ID'], 'external_system_text-' . $indexOrder1, $comm['external_system_text']);
          $indexOrder1++;
        }
      }
      unset($theArray);
      unset($standard);

      /*
      *
      *
      	=============================================== VERTICAL CONNECTIONS ======================================
      *
      *
      */
      $section = 'vertical';
      $theArray = $this->test_array_index($business[$section], 'product_type_id');
      unset($business[$section]);
      $collected_terms = array();
      foreach($theArray as $index => $vert)
      {
      	$vert = (array)$vert;
      	$tempY = "";
      	$taxonomy = "";
      	switch($vert['product_type_id'])
      	{
      		// Custom "Product Type" allocations. Currently need to be done via hard-code.
      		// eg In ATDW is "Exhibition and Shows", but on the website need to be displayed as "Exhibitions & Shows".
      		case "EXHIBIT": //Exhibition and Shows
      			$temp_term = get_term_by( 'slug', 'exhibitions-and-shows', 'atdw_event_cat', ARRAY_A);
      			$tempY = $temp_term['term_taxonomy_id']; //$tempY = 33;
      			break;
      		case "EVTCOMNTY": //Community Event
      			$temp_term = get_term_by( 'slug', 'community-events', 'atdw_event_cat', ARRAY_A);
      			$tempY = $temp_term['term_taxonomy_id']; //$tempY = 25;  
      			break;
      		case "MINDUSTRY": //Agri, Mining and Industry
      			$temp_term = get_term_by( 'slug', 'agriculture-mining-and-industry', 'atdw_attraction_cat', ARRAY_A);
      			$tempY = $temp_term['term_taxonomy_id']; //$tempY = 55;  
      			break;
      		default :
      			$tempY = "";
      	}
      	
      	if(!empty($tempY))
      	{
      		$collected_terms[] = $tempY;
      	}
      	else
      	{
      		//to get the <product_type_description> tag from ATDW query, and format it to be similar with other existing categories
      		$temp_product_type_description = str_replace(' and ', ' &amp; ', $vert['product_type_description']);
      		//check again whether the category exists
      		$temp_term = term_exists($temp_product_type_description, $the_business_post['taxonomy']);
      		if ($temp_term !== 0 && $temp_term !== null) {
        			//if it is an existing category, use this
        			$tempY = (int)$temp_term['term_id'];
      		}else{
        			//create new category, if category not exist
        			$temp_inserted = wp_insert_term(
        				$temp_product_type_description,
        				$the_business_post['taxonomy'],
        				array ( 'slug' => sanitize_title($vert['product_type_description']) )
        			);
        			//assign the new term id
        			$tempY = (int)$temp_inserted['term_id'];
      		}
      		$collected_terms[] = $tempY;
      	}

      	if(!empty($collected_terms))
      	{
      		wp_set_object_terms( $the_business_post['ID'], $collected_terms, $the_business_post['taxonomy'], false );
      	}
      }
      unset($collected_terms);
      unset($theArray);
      unset($tempY);


      /*
      *
      *
      	=============================================== ATTRIBUTES ======================================
      *
      *
      */
      $section = 'attributes';
      $theArray = $this->test_array_index($business[$section], 'attribute_id_description');
      unset($business[$section]);

      $indexOrder1 = 1;
      $indexOrder2 = 1;
      $indexOrder3 = 1;
      $indexOrder4 = 1;
      $indexOrder5 = 1;
      foreach($theArray as $index => $attr)
      {
      	$attr = (array)$attr;
      	if(isset($attr['attribute_type_id'])){
          switch($attr['attribute_type_id'])
        	{
        		case 'ENTITY FAC':
        			if(!empty($attr['attribute_id_description'])){
        				update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_facility-' . $indexOrder1, $attr['attribute_id_description']);
        			}
        			$indexOrder1++;
        			break;
        		
        		case 'DISASSIST':
        			update_post_meta($the_business_post['ID'], 'atdw_pr_disabled_access', $attr['attribute_id_description']);
        			break;
        		
        		case 'ACTIVITY':
        			update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_activity-' . $indexOrder3, $attr['attribute_id_description']);
        			$indexOrder3++;
        			break;
        			
        		case 'TAG':
        			if(!empty($attr['attribute_id_description'])){
        				switch($attr['attribute_id_description']){
        					case 'Family': $temp_tag = 'Family Friendly'; break;
        					default: $temp_tag = $attr['attribute_id_description']; break;
        				}
        				wp_set_object_terms( $the_business_post['ID'], $temp_tag, 'atdw_tags', true );
        			}
        			break;
        		
        		case 'CUISINE':
        			if(!empty($attr['attribute_id_description'])){
        				update_post_meta($the_business_post['ID'], 'atdw_pr_product_cuisine-' . $indexOrder5, $attr['attribute_id_description']);
        			}
        			$indexOrder5++;  
        			break;	
        		
        		case 'BYOLICENCE': // Liquor Licensing
        			update_post_meta($the_business_post['ID'], 'atdw_pr_byolicense_id', $attr['attribute_id']);
        			update_post_meta($the_business_post['ID'], 'atdw_pr_byolicense_description', $attr['attribute_id_description']);
        			break;
              
        		case 'REST PRICE': // Price range
        			update_post_meta($the_business_post['ID'], 'atdw_pr_price_range', $attr['attribute_id_description']);
        			break;  
        		
        		case 'EXPERIENCE':
        			
        			switch(strtolower($attr['attribute_id_description']))
        			{
        				/* Sub category structure only can be set via hard-code. Temporary skipping this part. */
        			}
        			
        			if(!empty($tempY))
        			{			
        				//check to avoid replication
        				$result = $wpdb->get_results('SELECT * FROM wp_term_relationships WHERE object_id = "' .$the_business_post['ID'] . '" AND term_taxonomy_id = "' . $tempY . '" LIMIT 1', ARRAY_A);
        				if(count($result) < 1 || $result == false)
        				{
        					$wpdb->insert( "wp_term_relationships", array("object_id" => $the_business_post['ID'], "term_taxonomy_id" => $tempY, "term_order" => 0));
        				}
        			}
        			if(!empty($attr['attribute_id_description'])){
        				update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_experience-' . $indexOrder2, $attr['attribute_id_description']);
        				$indexOrder2++;
        			}
        			break;
        	}
        }
      	
      	if(isset($attr['attribute_type_id_description']) && (in_array($attr['attribute_type_id_description'], array("Rating AAA", "Star Ratings"))))
      	{
      		update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_rating-' . $indexOrder4, $attr['attribute_id_description']);
      		$indexOrder4++;
      	}
      }
      unset($theArray);

      /*
      *
      *
      	=============================================== IMAGES ======================================
      *
      *
      */
      $section = 'images';
      $theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_content');
      if(empty($theArray))
      {
      	$theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_contnt');
      }
      unset($business[$section]);

      $removed = $wpdb->query('
      		UPDATE 
      			`wp_postmeta`
      		SET
      			`meta_value` = ""
      		WHERE 
      			`post_id` = ' . $the_business_post['ID'] . '
      		AND 
      		(
      			`meta_key` LIKE "atdw_bmm_image-%" 
      		 		OR
      		 	`meta_key` LIKE "atdw_bmm_img_alt-%" 
      		)
      		');

      $output.= "\r\n\n\t\t" . "--IMAGE UPDATES = UNSET: " . $removed . " image(s)";

      $get_the_sequence = array();

      $indexOrder = 1;
      foreach($theArray as $index => $img)
      {
      	$img = (array)$img;

      	if(
      		(
      			(isset($img['attribute_id_multimedia_content']) && $img['attribute_id_multimedia_content'] == 'IMAGE')
      			||
      			(isset($img['attribute_id_multimedia_contnt']) && $img['attribute_id_multimedia_contnt'] == 'IMAGE')
      		)
      		&&
      		(
      			($img['attribute_id_size_orientation'] == '4X3') && ($img['width'] == '800')
      		)
      	)
      	{
      		$var1 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_image-' . $indexOrder . '" LIMIT 1;');
      		if($var1)
      		{
      			$test1 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $img['server_path']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_image-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test1 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_image-' . $indexOrder,
      														'meta_value'=> $img['server_path']
      												)
      											);
      		}


      		$var2 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_img_alt-' . $indexOrder . '" LIMIT 1;');
      		if($var2)
      		{
      			$test2 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $img['alt_text']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_img_alt-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test2 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_img_alt-' . $indexOrder,
      														'meta_value'=> $img['alt_text']
      												)
      											);
      		}

      		$get_the_sequence[$indexOrder] = (int)$img['sequence_number'];
      		
      		$output.= "\r\n----------" . $img['server_path'] . ' [' .  print_r($test1, true) . '|' . print_r($test2, true) . '] ';		
      		$indexOrder++;
      	}			
      }

      asort($get_the_sequence);
      update_post_meta($the_business_post['ID'], 'atdw_bmm_img_sequence', json_encode($get_the_sequence));

      $output.= "\r\n\t\t" . " --IMAGE UPDATES = SET: " . ($indexOrder-1) . " image(s) \r\n";

      unset($theArray);
      
      /*
      *
      *
      	=============================================== VIDEOS ======================================
      *
      *
      */
      $section = 'videos';
      $theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_content');
      if(empty($theArray))
      {
      	$theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_contnt');
      }
      unset($business[$section]);

      $removed = $wpdb->query('
      		UPDATE 
      			`wp_postmeta`
      		SET
      			`meta_value` = ""
      		WHERE 
      			`post_id` = ' . $the_business_post['ID'] . '
      		AND 
      			`meta_key` LIKE "atdw_bmm_video-%"
      		');

      $output.= "\r\n\n\t\t" . "--VIDEO UPDATES = UNSET: " . $removed . " video(s)";

      $get_the_sequence = array();

      $indexOrder = 1;
      foreach($theArray as $index => $vid)
      {
      	$vid = (array)$vid;

      	if(isset($vid['attribute_id_multimedia_content']) && $vid['attribute_id_multimedia_content'] == 'VIDEO')
      	{
      		$var1 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_video-' . $indexOrder . '" LIMIT 1;');
      		if($var1)
      		{
      			$test1 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $vid['server_path']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_video-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test1 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_video-' . $indexOrder,
      														'meta_value'=> $vid['server_path']
      												)
      											);
      		}


      		$var2 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_vid_alt-' . $indexOrder . '" LIMIT 1;');
      		if($var2)
      		{
      			$test2 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $vid['multimedia_description']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_vid_alt-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test2 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_vid_alt-' . $indexOrder,
      														'meta_value'=> $vid['multimedia_description']
      												)
      											);
      		}

      		$get_the_sequence[$indexOrder] = (int)$vid['sequence_number'];
      		
      		$output.= "\r\n----------" . $vid['server_path'] . ' [' .  print_r($test1, true) . '|' . print_r($test2, true) . '] ';		
      		$indexOrder++;
      	}			
      }

      asort($get_the_sequence);
      update_post_meta($the_business_post['ID'], 'atdw_bmm_vid_sequence', json_encode($get_the_sequence));

      $output.= "\r\n\t\t" . " --VIDEO UPDATES = SET: " . ($indexOrder-1) . " video(s) \r\n";

      unset($theArray);
      
      /*
      *
      *
      	=============================================== ENTRY COST ======================================
      *
      *
      */
      $section = 'entry_cost';	
      $theArray = $this->test_array_index($business[$section], 'entry_cost_from');
      unset($business[$section]);

      $indexOrderEc = 1;
      foreach($theArray as $index => $entryco)
      {
      	$entryco = (array)$entryco;
      	if(isset($entryco['attribute_id_entry_cost'])){
          $temp_entry_id = strtolower($entryco['attribute_id_entry_cost']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_id-'.$indexOrderEc, $entryco['attribute_id_entry_cost']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_description-'.$indexOrderEc, $entryco['attribute_id_entry_cost_description']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_from-'.$indexOrderEc, $entryco['entry_cost_from']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_to-'.$indexOrderEc, $entryco['entry_cost_to']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_valid_from_date-'.$indexOrderEc, $entryco['valid_from_date']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_valid_to_date-'.$indexOrderEc, $entryco['valid_to_date']);
          update_post_meta($the_business_post['ID'], 'atdw_entry_cost_comment_text-'.$indexOrderEc, $entryco['comment_text']);
          $indexOrderEc++;
        }
      }
      unset($theArray);	
      
      /*
      *
      *
      	=============================================== Event Dates ======================================
      *
      *
      */      
      $section = 'event_frequency';
      $theArray = $this->test_array_index($business[$section], 'frequency_start_date');
      unset($business[$section]);
      if($theArray){
        if( $attribute_id_frequency == 'ONCE ONLY' ){
          // need to find the event date that match with <next_occurrence>,
          // and assign the dates (start & end) as atdw_pr_event_starts and atdw_pr_event_ends
          foreach($theArray as $index => $ev_item)
          {
            $ev_item = (array)$ev_item;            
            if(isset($ev_item['frequency_start_date']) && $ev_item['frequency_start_date'] == $event_next_occurrence){
              update_post_meta($the_business_post['ID'], 'atdw_pr_event_starts', $ev_item['frequency_start_date']);
              update_post_meta($the_business_post['ID'], 'atdw_pr_event_ends', $ev_item['frequency_end_date']);
            }            
          }
        }else{
          // check whether $event_overall_start is a past date
          $event_overall_start_timestamp = strtotime($event_overall_start);
          if(time() > $event_overall_start_timestamp){
            // current time is greater than the given date, thus this is a past event
            // use the <next_occurrence> as new start date
            update_post_meta($the_business_post['ID'], 'atdw_pr_event_starts', $event_next_occurrence);
          }
        }
        
        // temporarily save all event dates, for debug
        update_post_meta($the_business_post['ID'], 'atdw_pr_event_items', $theArray);
      }

      /*
      *
      *
      	=============================================== GENERAL POST ======================================
      *
      *
      */
      $wpdb->update('wp_posts', array(
      								'post_title' 	=> $the_business_post['post_title'],
      								'post_name' 	=> $the_business_post['post_name'],
      								'post_content' 	=> $the_business_post['post_content'],
      								//'guid' 			=> $the_business_post['guid'],
      								'post_modified'	=> date('Y-m-d H:i:s', (time() - 86400)),
      								'post_modified_gmt'	=> date('Y-m-d H:i:s', (time() - 86400)),
      								'post_status'	=> 'publish'
      							), array('ID' 		=> $the_business_post['ID']));

      $output.= "\r\n";
      
      // for logging
      $return['output'] = $output;
      
      return $return;
    }
    
    /**
     * Saving other language content.
     * Requires WPML to be installed and activated first.
     */
     private function process_this_business_ext_lang($buss_id, $cat_id, $booking_urls, $region = null, $city, $lang)
     {
       global $wpdb;
       $the_business_post = array();
       $business = array();
       $return = array();
       $output = null;  
       
       // multi-language
       switch($lang){
         case "zh-hant": $xml_mv = "CHINESE-T"; $excerpt_prefix = "atdw_buss_id_cn_t"; break;
         case "zh-hans": $xml_mv = "CHINESE-S"; $excerpt_prefix = "atdw_buss_id_cn_s"; break;
         default: $xml_mv = "invalid"; $excerpt_prefix = "others"; break;
       }
       
       $output.= ' [Syncing entry #'.$this->entries_sync_counter." of current execution]";
       
       // before sync this individual entry, first check whether the sync limit have been reached
       if( $this->entries_sync_counter > $this->entries_sync_limit ){
         // limit reached, save into system
         $this->entries_sync_limit_reached = true;
         //update_option( 'entries_sync_limit_reached', true, 'no' ); 
         update_option( 'atdw_sync_next_city', $city, 'no' );
         update_option( 'atdw_sync_next_id', $buss_id, 'no' );
         
         // output message
         $output.= "\n\nSync limit (".$this->entries_sync_limit.") exceeded. Stopping execution now. Progress will be saved for next execution. \n\n";
         // update the execution flag
         $temp_flag_file = $this->document_root.'flag';
         $temp_hndlr = fopen($temp_flag_file, 'w+');
         $temp_write = $this->current_execution;
         fwrite($temp_hndlr,  $temp_write);
         fclose($temp_hndlr);
         
         $return['output'] = $output; return $return;
       }
       
       if(empty($buss_id))
       {
       	$output.= '---XXXX--error ontaning product id-----XXXXX-----';
         // for logging
         $return['output'] = $output;
         return $return;
       }
       
       $the_business_post['atdw_buss_id'] = $buss_id;
       switch($cat_id){
       	case 'ACCOMM': 
       		$the_business_post['post_type'] = 'atdw_accomm';
       		$the_business_post['taxonomy'] = 'atdw_accomm_cat';
       		$the_business_post['slug'] = 'stay';
       		break;
       	case 'ATTRACTION': 
       		$the_business_post['post_type'] = 'atdw_attraction';
       		$the_business_post['taxonomy'] = 'atdw_attraction_cat';
       		$the_business_post['slug'] = 'see-and-do';
       		break;
       	case 'EVENT': 
       		$the_business_post['post_type'] = 'atdw_event';
       		$the_business_post['taxonomy'] = 'atdw_event_cat';
       		$the_business_post['slug'] = 'events';
       		break;
       	case 'TOUR': 
       		$the_business_post['post_type'] = 'atdw_tour';
       		$the_business_post['taxonomy'] = 'atdw_tour_cat';
       		$the_business_post['slug'] = 'tours-and-trails';
       		break;
       	case 'GENSERVICE': 
       		$the_business_post['post_type'] = 'atdw_genservice';
       		$the_business_post['taxonomy'] = 'atdw_genservice_cat';
       		$the_business_post['slug'] = 'professional-services';
       		break;  
       	case 'RESTAURANT': 
       		$the_business_post['post_type'] = 'atdw_restaurant';
       		$the_business_post['taxonomy'] = 'atdw_restaurant_cat';
       		$the_business_post['slug'] = 'food-and-wine';
       		break;
       	default:
       		$the_business_post['post_type'] = 'atdw_others';
       		$the_business_post['taxonomy'] = 'atdw_others_cat';
       		break;
       }
       
       $the_business_post['post_content'] = "";
       $the_business_post['post_title'] = "";
       $the_business_post['post_name'] = "";
       //$the_business_post['guid'] = "";
       
       // check whether the entry is already imported into the site
       $theid = $wpdb->get_var("SELECT wp_icl_translations.element_id FROM wp_posts LEFT JOIN wp_icl_translations ON wp_icl_translations.trid = wp_posts.ID WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' AND language_code = '" . $lang . "' LIMIT 1;");
       // If default language translation is not created yet, eg encountered interuption during last execution (post_title will be saved as "ATDW In Progress")
       if(empty($theid)){
         $theid = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_excerpt LIKE '%".$excerpt_prefix."||" . $the_business_post['atdw_buss_id'] . "%' LIMIT 1;");
       }
 
       if(empty($theid))
       {
         //to get the default language post id
         $default_lang = $this->lang_en;
         $default_lang_postid = $wpdb->get_var("SELECT wp_icl_translations.element_id FROM wp_posts LEFT JOIN wp_icl_translations ON wp_icl_translations.trid = wp_posts.ID WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' AND language_code = '" . $default_lang . "' LIMIT 1;");
         // If default language translation is not created yet, eg encountered interuption during last execution (post_title will be saved as "ATDW In Progress")
         if(empty($default_lang_postid)){
           $default_lang_postid = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_excerpt LIKE '%atdw_buss_id||" . $the_business_post['atdw_buss_id'] . "%' LIMIT 1;");
         } 
        
         //to get the default language post title, which will be used as the post title for this entry if there's no post title value for this language
         $default_lang_posttitle = $wpdb->get_var("SELECT post_title FROM wp_posts WHERE ID = ".$default_lang_postid.";");
        
         $param = array(
       				  'comment_status' 	=> 'closed',
       				  'ping_status' 	=> 'closed',
       				  'post_author' 	=> 1,
       				  'post_content' 	=> '',
       				  'post_date' 		=> date('Y-m-d H:i:s', (time() - 86400)),
       				  'post_date_gmt' 	=> date('Y-m-d H:i:s', (time() - 86400)),
       				  'post_excerpt' 	=> $excerpt_prefix . '||' . $the_business_post['atdw_buss_id'],
       				  'post_name' 		=> $the_business_post['atdw_buss_id'],
       				  'post_parent' 	=> 0,
       				  'post_status' 	=> 'publish',
       				  'post_title' 		=> $default_lang_posttitle,
       				  'post_type' 		=> $the_business_post['post_type']
         );
        
        //insert into db        
       	$theid = $this->api_wpml_translate_post( $default_lang_postid, $the_business_post['post_type'], $lang, $param );
       	update_post_meta($theid, 'atdw_booking_button_int', '0');
       }
       else
       {
       	wp_set_object_terms( $theid, NULL, $the_business_post['taxonomy'] );	
       }
 
       $the_business_post['ID'] = $theid;
 
       //find the records in the wp_postmeta for this ID, and set the meta_value for all to blank
       $temp_query = $wpdb->query( $wpdb->prepare(
       	"UPDATE wp_postmeta SET meta_value = %s WHERE post_id = %d AND meta_key != %s AND meta_key != %s AND meta_key != %s AND meta_key != %s AND meta_key != %s AND (meta_key like %s or meta_key like %s)",
       	"",
       	(int)$the_business_post['ID'],
       	"atdw_booking_button",
       	"atdw_booking_button_int",
       	"atdw_pr_city",
       	"atdw_pr_region",
       	"atdw_pr_state",
       	"atdw_%",
       	"external_system_%"
       ) );
 
       // query for other language content
       $strURL	= 'http://' . $this->atdw_api_host . '/api/atlas/product?key=' . $this->atdw_api_key . '&productId=' . $the_business_post['atdw_buss_id'] . '&mv=' . $xml_mv ;
       $strXML	= @file_get_contents(str_replace(' ', '%20', $strURL));
       if(!$strXML){
         $strXML = $this->get_web_page(str_replace(' ', '%20', $strURL));
       }
       
       if(!$strXML)
       {
       	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err Code: 404] \n\n";
         // for logging
         $return['output'] = $output;
         return $return;
       }elseif( (strlen($strXML) == 216) || ($strXML == "{\"error\":{\"name\":\"TypeError\",\"status\":500,\"message\":\"Cannot assign to read only property 'status' of illegal access\"}}") )
       {
       	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err Code: 500] \n\n";
         // for logging
         $return['output'] = $output;
         return $return;
       }
 
       //create log, as reference for future debug
       //$atdw_entry_file = $this->document_root . "logs/atdw/" . date("Y_m_d") . "/entries/" . date("Y_m_d") . "_" . $the_business_post['atdw_buss_id'] . ".atdw";
       //$fae = fopen($atdw_entry_file, 'w') or die("can't open log file");
       //fwrite($fae, $strXML);
       //fclose($fae);
 
       $checkXML = $this->isXML($strXML);
       if($checkXML === true){
         $buss_info = new SimpleXMLElement($strXML);
         $buss_info = $this->object2array($buss_info);
       }else{
         $buss_info = null;
         $output.= " [Error reading XML (". $strURL ."): ". $checkXML ."]";
       }
 
       if(empty($buss_info['product_distribution'])) 
       {
       	$output.= " [ERROR ATDW CALL. ATDW Product ID: " . $the_business_post['atdw_buss_id'] . " ] [ATDW Err: product_distribution is empty ] \n\n";
         // for logging
         $return['output'] = $output;
         return $return; 
       } 
       $business['infos'] 		= $buss_info['product_distribution']['product_record']; 				//details
       $business['addresses'] 	= $buss_info['product_distribution']['product_address']; 				//location + postal ++
       $business['contacts'] 	= $buss_info['product_distribution']['product_communication']; 			//phone + email + faxs + etc
       $business['externals'] 	= $buss_info['product_distribution']['product_external_system']; 		//external links & socials
       $business['vertical']	= $buss_info['product_distribution']['product_vertical_classification']; //verticals
       $business['attributes'] = $buss_info['product_distribution']['product_attribute']; 				//facilities + expirience ++
       $business['images'] 	= $buss_info['product_distribution']['product_multimedia']; 			//imges jpg + png + gif
       $business['videos'] 	= $buss_info['product_distribution']['product_multimedia']; 			//video
       $business['entry_cost'] 	= $buss_info['product_distribution']['product_entry_cost']; 			//entry cost
       $business['event_frequency'] 	= $buss_info['product_distribution']['event_frequency']; 			//event dates
       unset($strXML);
       unset($buss_info);
       
       /*
       *
       *
       	=============================================== INFO ======================================
       *
       *
       */
 
       $section = 'infos';
       $standard = array(
       				"frequency_start_date" 					=> "atdw_pr_event_starts",
       				"frequency_end_date" 					=> "atdw_pr_event_ends",
       				"attribute_id_frequency_description" 	=> "atdw_pr_event_frequency",
       				"attribute_id_event_status_description" => "atdw_pr_event_status",
       				"number_of_rooms" 						=> "atdw_pr_room_number",
       				"city_name" 							=> "atdw_pr_city",
       				"state_name" 							=> "atdw_pr_state",
       				"product_description" 					=> "atdw_pr_product_description",
       				//"product_short_description" 					=> "atdw_pr_product_short_description",
       				"reception_hours" 						=> "atdw_pr_reception_hours",
       				"reception_hours_text" 					=> "atdw_pr_reception_hours_txt",
       				"australian_business_number" 			=> "atdw_pr_abn"
       			);
       $theArray = $this->test_array_index($business[$section], 'product_name_mv');
       
       // default language values for empty multi-language content
       $dl_product_name = $business['infos']['product_name'];
       $dl_product_description = $business['infos']['product_description'];
       
       // the next event date
       $event_next_occurrence = $business['infos']['next_occurrence'];
       // the overall event start date
       $event_overall_start = $business['infos']['frequency_start_date'];
       // frequency type
       $attribute_id_frequency = $business['infos']['attribute_id_frequency'];
       
       unset($business[$section]);
       
       foreach($theArray as $key => $info)
       {
       	if(isset($standard[$key]))
       	{
       		if(!empty($info)){
       			update_post_meta($the_business_post['ID'], @$standard[$key], $info);
       		}
       	}
       	else
       	{
       		switch($key)
       		{
       			case "product_name_mv":
       				if(empty($info)){
       				  // check if there's multi language content inserted before this
       				  $info_prev = get_the_title($the_business_post['ID']);
       				  if($info_prev){
       				    // use back the exiting content
       				    $info = $info_prev;
       				  }else{
       				    // multi language content is not available
       				    // use default language content instead
       				    $info = $dl_product_name;
       				  }
       				}
       				$the_business_post['post_title'] = $info;
       				$the_business_post['post_name'] = sanitize_title($info);
       				update_post_meta($the_business_post['ID'], 'atdw_pr_product_name', $info);
       				break;
       			
       			case "product_description_mv":
       				if(empty($info)){
       				  // check if there's multi language content inserted before this
       				  $info_prev = get_post_field('post_content', $the_business_post['ID']);
       				  if($info_prev){
       				    // use back the exiting content
       				    $info = $info_prev;
       				  }
       				}
       				if(!empty($info)){
       				  $the_business_post['post_content'] = sanitize_title($info);
       				  update_post_meta($the_business_post['ID'], 'atdw_pr_product_description', $info);
       				}
       				break;				
       			
       			case "rate_from":
       				update_post_meta($the_business_post['ID'], 'atdw_pr_all_rate_from', $this->cron_validate_amount($info));
       				break;
       			
       			case "rate_to":
       				update_post_meta($the_business_post['ID'], 'atdw_pr_all_rate_to', $this->cron_validate_amount($info));
       				break;
       			
       			case "children_catered_for_flag":					
       				update_post_meta($the_business_post['ID'], 'atdw_pr_children_catered', $this->cron_validate_yes($info));
       				break;
       				
       			case "pet_allowed":
       			case "pets_allowed_flag":
       				update_post_meta($the_business_post['ID'], 'atdw_pr_pet_allowed', $this->cron_validate_yes($info));
       				break;
       				
       			case "check_in_time";
       				update_post_meta($the_business_post['ID'], 'atdw_pr_check-in_time', $this->cron_validate_time($info));
       				break;
       							
       			case "check_out_time";
       				update_post_meta($the_business_post['ID'], 'atdw_pr_check-out_time', $this->cron_validate_time($info));
       				break;
       			
       			default :
       				update_post_meta($the_business_post['ID'], 'atdw_unknown_' . $key, $info);
       		}
       	}	
       }
       
       //update booking URLs
       foreach ($booking_urls as $booking_url) {
       	if( isset($booking_url['product_id']) && ($booking_url['product_id'] == $the_business_post['atdw_buss_id']) ){
       		if(isset($booking_url['comms_burl']) && !empty($booking_url['comms_burl'])){
       			update_post_meta($the_business_post['ID'], 'atdw_booking_button', $booking_url['comms_burl']);
       			update_post_meta($the_business_post['ID'], 'atdw_booking_button_int', '1');
       			break;
       		}
       	}
       }
       
       //update Region
       if($region){
         update_post_meta($the_business_post['ID'], 'atdw_pr_region', $region);
       }
 
       unset($theArray);
       unset($standard);
 
       /*
       *
       *
       	=============================================== ADDRESSES ======================================
       *
       *
       */
       $section = 'addresses';	
       $theArray = $this->test_array_index($business[$section], 'address_line_1');
       unset($business[$section]);
 
       foreach($theArray as $index => $address)
       {
       	$address = (array)$address;
 
       	$addressX = '';
       	$addressX .= ( isset($address['address_line_1']) ? $this->cron_validate_string_w_delimiter($address['address_line_1']) : null );
       	$addressX .= ( isset($address['address_line_2']) ? $this->cron_validate_string_w_delimiter($address['address_line_2']) : null );
       	$addressX .= ( isset($address['address_line_3']) ? $this->cron_validate_string_w_delimiter($address['address_line_3']) : null );
       	$addressX .= ( isset($address['city_name']) ? $this->cron_validate_string_w_delimiter($address['city_name']) : null );
       	$addressX .= ( isset($address['state_name']) ? $this->cron_validate_string_w_delimiter($address['state_name']) : null );
       	$addressX .= ( (isset($address['address_postal_code']) && (!is_array($address['address_postal_code'] ))) ? $address['address_postal_code'] : null );
 
       	if($address['attribute_id_address'] == 'PHYSICAL')
       	{
       		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_addr', $addressX);
       		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_lat', $address['geocode_gda_latitude']);
       		update_post_meta($the_business_post['ID'], 'atdw_pr_pysical_long', $address['geocode_gda_longitude']);				
       	}
       	elseif($address['attribute_id_address'] == 'POSTAL')
       	{
       		update_post_meta($the_business_post['ID'], 'atdw_pr_postal', $addressX);
       	}
       }
       unset($theArray);	
 
 
       /*
       *
       *
       	=============================================== CONTACTS ======================================
       *
       *
       */
       $section = 'contacts';
       $theArray = $this->test_array_index($business[$section], 'attribute_id_communication');
       unset($business[$section]);
 
       $standard = array(
       				"CAEMENQUIR" => "atdw_pr_comm_email",
       				"CAURENQUIR" => "atdw_pr_comm_website",
       				"CAPHENQUIR" => "atdw_pr_comm_phone",
       				"CAFXENQUIR" => "atdw_pr_comm_fax",
       				"CAMBENQUIR" => "atdw_pr_comm_mobile",
       				"CATLENQUIR" => "atdw_pr_comm_tollfree",
       				"CAUBENQUIR" => "atdw_pr_comm_booking"
       			);
 
       foreach($theArray as $index => $comm)
       {
       	$comm = (array)$comm;
       	if(isset($standard[$comm['attribute_id_communication']]))
       	{
       		if($comm['attribute_id_communication'] == 'CAPHENQUIR'){
       			//formating the spaces of the phone number
       			$temp_phone = preg_replace('/\s+/', '',$comm['communication_detail']);
       			if(strlen($temp_phone) == 8){
       				if($comm['communication_area_code'] == "04"){
       					$comm['communication_detail'] = substr($temp_phone,0,2)." ".substr($temp_phone,2,3)." ".substr($temp_phone,5,3);
       				}else{
       					$temp_phone_arr = str_split($temp_phone,4);
       					$comm['communication_detail'] = ' ';
       					$comm['communication_detail'] .= implode(" ", $temp_phone_arr);
       				}
       			}
       		}
       		//area code if any
       		$value = (isset($comm['communication_area_code']) ? $comm['communication_area_code'] : '') . $comm['communication_detail'];
       		update_post_meta($the_business_post['ID'], $standard[$comm['attribute_id_communication']], $value);
       	}
       	else
       	{
       		update_post_meta($the_business_post['ID'], $standard[$comm['attribute_id_communication']], '');
       	}
       }
       unset($theArray);
       unset($standard);
 
       /*
       *
       *
       	=============================================== EXTERNAL LINKS ======================================
       *
       *
       */
       $section = 'externals';		
       $theArray = $this->test_array_index($business[$section], 'external_system_code');
       unset($business[$section]);
 
       $indexOrder1 = 1;
       foreach($theArray as $index => $comm)
       {
       	$comm = (array)$comm;
       	if(isset($comm['external_system_code'])){
           update_post_meta($the_business_post['ID'], 'external_system_code-' . $indexOrder1, $comm['external_system_code']);
           update_post_meta($the_business_post['ID'], 'external_system_text-' . $indexOrder1, $comm['external_system_text']);
           $indexOrder1++;
         }
       }
       unset($theArray);
       unset($standard);
 
       /*
       *
       *
       	=============================================== VERTICAL CONNECTIONS ======================================
       *
       *
       */
       $section = 'vertical';
       $theArray = $this->test_array_index($business[$section], 'product_type_id');       
       unset($business[$section]);
       $collected_terms = array();
       $taxonomy = '';
       foreach($theArray as $index => $vert)
       {
       	$vert = (array)$vert;
        $cn_t_term = null;
       	
        //to get the <product_type_description_mv> (language translation), and update the WPML translationfor this term accordingly.
        if(!empty($vert['product_type_description_mv'])){
          // check whether the traditional chinese content have been created
          $cn_t_check_term = term_exists($vert['product_type_description_mv'], $the_business_post['taxonomy']);
          if($cn_t_check_term !== 0 && $cn_t_check_term !== null) {
            //if it is an existing category, use this
            $cn_t_term = (int)$cn_t_check_term['term_id'];
          }else{
            // get default language content
            $temp_product_type_description = str_replace(' and ', ' &amp; ', $vert['product_type_description']);
            $default_lang = $this->lang_en;
            // get the id for this taxonomy (of default language)
            $dl_check_term = term_exists($temp_product_type_description, $the_business_post['taxonomy']);
            $dl_term = (int)$dl_check_term['term_id'];
            // create new
            $cn_t_term = $this->api_wpml_translate_taxonomy( $dl_term, $vert['product_type_description_mv'], $the_business_post['taxonomy'], array ( 'slug' => sanitize_title($vert['product_type_description_mv']) ), $lang, $default_lang );
          }
        }
        
        $collected_terms[] = $cn_t_term;
        
        if(!empty($collected_terms))
        {
          wp_set_object_terms( $the_business_post['ID'], $collected_terms, $the_business_post['taxonomy'], false );
        }        
       }
       unset($collected_terms);
       unset($theArray);
       unset($tempY);
 
 
       /*
       *
       *
       	=============================================== ATTRIBUTES ======================================
       *
       *
       */
       $section = 'attributes';
       $theArray = $this->test_array_index($business[$section], 'attribute_id_description');
       unset($business[$section]);
 
       $indexOrder1 = 1;
       $indexOrder2 = 1;
       $indexOrder3 = 1;
       $indexOrder4 = 1;
       $indexOrder5 = 1;
       foreach($theArray as $index => $attr)
       {
       	$attr = (array)$attr;
       	if(isset($attr['attribute_type_id'])){
           switch($attr['attribute_type_id'])
         	{
         		case 'ENTITY FAC':
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_facility-' . $indexOrder1, $attr['attribute_id_description_mv']);
         			}
         			$indexOrder1++;
         			break;
         		
         		case 'DISASSIST':
         			update_post_meta($the_business_post['ID'], 'atdw_pr_disabled_access', $attr['attribute_id_description_mv']);
         			break;
         		
         		case 'ACTIVITY':
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_activity-' . $indexOrder3, $attr['attribute_id_description_mv']);
         			}
         			$indexOrder3++;
         			break;
         			
         		case 'TAG':
         			if(!empty($attr['attribute_id_description_mv'])){
         				wp_set_object_terms( $the_business_post['ID'], $attr['attribute_id_description_mv'], 'atdw_tags', true );
         			}
         			break;
         		
         		case 'CUISINE':
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_product_cuisine-' . $indexOrder5, $attr['attribute_id_description']);
         			}
         			$indexOrder5++;  
         			break;	
         		
         		case 'BYOLICENCE': // Liquor Licensing
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_byolicense_id', $attr['attribute_id']);
         				update_post_meta($the_business_post['ID'], 'atdw_pr_byolicense_description', $attr['attribute_id_description_mv']);
         			}
         			break;
               
         		case 'REST PRICE': // Price range
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_price_range', $attr['attribute_id_description_mv']);
         			}
         			break;  
         		
         		case 'EXPERIENCE':
         			
         			switch(strtolower($attr['attribute_id_description_mv']))
         			{
         				/* Sub category structure only can be set via hard-code. Temporary skipping this part. */
         			}
         			
         			/*if(!empty($tempY))
         			{			
         				//check to avoid replication
         				$result = $wpdb->get_results('SELECT * FROM wp_term_relationships WHERE object_id = "' .$the_business_post['ID'] . '" AND term_taxonomy_id = "' . $tempY . '" LIMIT 1', ARRAY_A);
         				if(count($result) < 1 || $result == false)
         				{
         					$wpdb->insert( "wp_term_relationships", array("object_id" => $the_business_post['ID'], "term_taxonomy_id" => $tempY, "term_order" => 0));
         				}
         			}*/
         			if(!empty($attr['attribute_id_description_mv'])){
         				update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_experience-' . $indexOrder2, $attr['attribute_id_description_mv']);
         				$indexOrder2++;
         			}
         			break;
         	}
         }
       	
       	if(isset($attr['attribute_type_id_description']) && (in_array($attr['attribute_type_id_description'], array("Rating AAA", "Star Ratings"))))
       	{
       		update_post_meta($the_business_post['ID'], 'atdw_pr_product_attr_rating-' . $indexOrder4, $attr['attribute_id_description']);
       		$indexOrder4++;
       	}
       }
       unset($theArray);
 
       /*
       *
       *
       	=============================================== IMAGES ======================================
       *
       *
       */
       $section = 'images';
       $theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_content');
       if(empty($theArray))
       {
       	$theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_contnt');
       }
       unset($business[$section]);
 
       $removed = $wpdb->query('
       		UPDATE 
       			`wp_postmeta`
       		SET
       			`meta_value` = ""
       		WHERE 
       			`post_id` = ' . $the_business_post['ID'] . '
       		AND 
       		(
       			`meta_key` LIKE "atdw_bmm_image-%" 
       		 		OR
       		 	`meta_key` LIKE "atdw_bmm_img_alt-%" 
       		)
       		');
 
       $output.= "\r\n\n\t\t" . "--IMAGE UPDATES = UNSET: " . $removed . " image(s)";
 
       $get_the_sequence = array();
 
       $indexOrder = 1;
       foreach($theArray as $index => $img)
       {
       	$img = (array)$img;
 
       	if(
       		(
       			(isset($img['attribute_id_multimedia_content']) && $img['attribute_id_multimedia_content'] == 'IMAGE')
       			||
       			(isset($img['attribute_id_multimedia_contnt']) && $img['attribute_id_multimedia_contnt'] == 'IMAGE')
       		)
       		&&
       		(
       			($img['attribute_id_size_orientation'] == '4X3') && ($img['width'] == '800')
       		)
       	)
       	{
       		$var1 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_image-' . $indexOrder . '" LIMIT 1;');
       		if($var1)
       		{
       			$test1 = $wpdb->update('wp_postmeta',
       												array(
       														'meta_value'=> $img['server_path']
       													),
       												array(
       														'post_id' 	=> $the_business_post['ID'],
       														'meta_key' 	=> 'atdw_bmm_image-' . $indexOrder
       													)
       											);
       		}
       		else
       		{
       			$test1 = $wpdb->insert('wp_postmeta',
       												array(
       														'post_id' 	=> $the_business_post['ID'],
       														'meta_key' 	=> 'atdw_bmm_image-' . $indexOrder,
       														'meta_value'=> $img['server_path']
       												)
       											);
       		}
 
 
       		$var2 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_img_alt-' . $indexOrder . '" LIMIT 1;');
       		if($var2)
       		{
       			$test2 = $wpdb->update('wp_postmeta',
       												array(
       														'meta_value'=> $img['alt_text']
       													),
       												array(
       														'post_id' 	=> $the_business_post['ID'],
       														'meta_key' 	=> 'atdw_bmm_img_alt-' . $indexOrder
       													)
       											);
       		}
       		else
       		{
       			$test2 = $wpdb->insert('wp_postmeta',
       												array(
       														'post_id' 	=> $the_business_post['ID'],
       														'meta_key' 	=> 'atdw_bmm_img_alt-' . $indexOrder,
       														'meta_value'=> $img['alt_text']
       												)
       											);
       		}
 
       		$get_the_sequence[$indexOrder] = (int)$img['sequence_number'];
       		
       		$output.= "\r\n----------" . $img['server_path'] . ' [' .  print_r($test1, true) . '|' . print_r($test2, true) . '] ';		
       		$indexOrder++;
       	}			
       }
 
       asort($get_the_sequence);
       update_post_meta($the_business_post['ID'], 'atdw_bmm_img_sequence', json_encode($get_the_sequence));
 
       $output.= "\r\n\t\t" . " --IMAGE UPDATES = SET: " . ($indexOrder-1) . " image(s) \r\n";
 
       unset($theArray);
       
       /*
      *
      *
      	VIDEOS
      *
      *
      */
      $section = 'videos';
      $theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_content');
      if(empty($theArray))
      {
      	$theArray = $this->test_array_index($business[$section], 'attribute_id_multimedia_contnt');
      }
      unset($business[$section]);

      $removed = $wpdb->query('
      		UPDATE 
      			`wp_postmeta`
      		SET
      			`meta_value` = ""
      		WHERE 
      			`post_id` = ' . $the_business_post['ID'] . '
      		AND 
      			`meta_key` LIKE "atdw_bmm_video-%"
      		');

      $output.= "\r\n\n\t\t" . "--VIDEO UPDATES = UNSET: " . $removed . " video(s)";

      $get_the_sequence = array();

      $indexOrder = 1;
      foreach($theArray as $index => $vid)
      {
      	$vid = (array)$vid;

      	if(isset($vid['attribute_id_multimedia_content']) && $vid['attribute_id_multimedia_content'] == 'VIDEO')
      	{
      		$var1 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_video-' . $indexOrder . '" LIMIT 1;');
      		if($var1)
      		{
      			$test1 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $vid['server_path']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_video-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test1 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_video-' . $indexOrder,
      														'meta_value'=> $vid['server_path']
      												)
      											);
      		}


      		$var2 = $wpdb->get_var('SELECT COUNT(*) FROM `wp_postmeta` WHERE `post_id` = ' . $the_business_post['ID'] . ' AND `meta_key` = "atdw_bmm_vid_alt-' . $indexOrder . '" LIMIT 1;');
      		if($var2)
      		{
      			$test2 = $wpdb->update('wp_postmeta',
      												array(
      														'meta_value'=> $vid['multimedia_description']
      													),
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_vid_alt-' . $indexOrder
      													)
      											);
      		}
      		else
      		{
      			$test2 = $wpdb->insert('wp_postmeta',
      												array(
      														'post_id' 	=> $the_business_post['ID'],
      														'meta_key' 	=> 'atdw_bmm_vid_alt-' . $indexOrder,
      														'meta_value'=> $vid['multimedia_description']
      												)
      											);
      		}

      		$get_the_sequence[$indexOrder] = (int)$vid['sequence_number'];
      		
      		$output.= "\r\n----------" . $vid['server_path'] . ' [' .  print_r($test1, true) . '|' . print_r($test2, true) . '] ';		
      		$indexOrder++;
      	}			
      }

      asort($get_the_sequence);
      update_post_meta($the_business_post['ID'], 'atdw_bmm_vid_sequence', json_encode($get_the_sequence));

      $output.= "\r\n\t\t" . " --VIDEO UPDATES = SET: " . ($indexOrder-1) . " video(s) \r\n";

      unset($theArray);
       
       /*
       *
       *
       	ENTRY COST
       *
       *
       */
       $section = 'entry_cost';	
       $theArray = $this->test_array_index($business[$section], 'entry_cost_from');
       unset($business[$section]);
 
       $indexOrderEc = 1;
       foreach($theArray as $index => $entryco)
       {
       	$entryco = (array)$entryco;
       	if(isset($entryco['attribute_id_entry_cost'])){
           $temp_entry_id = strtolower($entryco['attribute_id_entry_cost']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_id-'.$indexOrderEc, $entryco['attribute_id_entry_cost']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_description-'.$indexOrderEc, $entryco['attribute_id_entry_cost_description']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_from-'.$indexOrderEc, $entryco['entry_cost_from']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_to-'.$indexOrderEc, $entryco['entry_cost_to']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_valid_from_date-'.$indexOrderEc, $entryco['valid_from_date']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_valid_to_date-'.$indexOrderEc, $entryco['valid_to_date']);
           update_post_meta($the_business_post['ID'], 'atdw_entry_cost_comment_text-'.$indexOrderEc, $entryco['comment_text']);
           $indexOrderEc++;
         }
       }
       unset($theArray);	
       
       /*
       *
       *
       	Event Dates
       *
       *
       */
       $section = 'event_frequency';
       $theArray = $this->test_array_index($business[$section], 'frequency_start_date');
       unset($business[$section]);
       if($theArray){
         if( $attribute_id_frequency == 'ONCE ONLY' ){
           // need to find the event date that match with <next_occurrence>,
           // and assign the dates (start & end) as atdw_pr_event_starts and atdw_pr_event_ends
           foreach($theArray as $index => $ev_item)
           {
             $ev_item = (array)$ev_item;            
             if(isset($ev_item['frequency_start_date']) && $ev_item['frequency_start_date'] == $event_next_occurrence){
               update_post_meta($the_business_post['ID'], 'atdw_pr_event_starts', $ev_item['frequency_start_date']);
               update_post_meta($the_business_post['ID'], 'atdw_pr_event_ends', $ev_item['frequency_end_date']);
             }            
           }
         }else{
           // check whether $event_overall_start is a past date
           $event_overall_start_timestamp = strtotime($event_overall_start);
           if(time() > $event_overall_start_timestamp){
             // current time is greater than the given date, thus this is a past event
             // use the <next_occurrence> as new start date
             update_post_meta($the_business_post['ID'], 'atdw_pr_event_starts', $event_next_occurrence);
           }
         }
         
         // temporarily save all event dates, for debug
         update_post_meta($the_business_post['ID'], 'atdw_pr_event_items', $theArray);
       }
 
       /*
       *
       *
       	GENERAL POST
       *
       *
       */
       $wpdb->update('wp_posts', array(
       								'post_title' 	=> $the_business_post['post_title'],
       								'post_name' 	=> $the_business_post['post_name'],
       								'post_content' 	=> $the_business_post['post_content'],
       								//'guid' 			=> $the_business_post['guid'],
       								'post_modified'	=> date('Y-m-d H:i:s', (time() - 86400)),
       								'post_modified_gmt'	=> date('Y-m-d H:i:s', (time() - 86400)),
       								'post_status'	=> 'publish'
       							), array('ID' 		=> $the_business_post['ID']));
 
       $output.= "\r\n";
       
       // for logging
       $return['output'] = $output;
       
       return $return;
     } 
    
    /**
     * Helper functions
     */
    private function test_array_index($array, $test_string)
    {
    	$theArray = array();
    	if(!empty($array['row'][0][$test_string]))
    	{
    		$theArray = $array['row'];
    	}
    	else
    	{
    		$theArray = $array['row'][0] = $array;
    	}
    	return($theArray);
    }

    private function cron_validate_time($value)
    {
    	$pvalue = 0;
    	date_default_timezone_set("Australia/Sydney");
    	if($value <= 2400)
    	{
    		if(strlen($value) == 4)
    		{
    			$value = $value[0] . $value[1] . ":" . $value[2] . $value[3] . ":00";
    			$pvalue = date("g:i a", strtotime($value));
    		}
    		elseif(strlen($value) == 3)
    		{
    			$value = $value[0] . $value[1] . ":" . $value[2] . "0:00";
    			$pvalue = date("g:i a", strtotime($value));
    		}
    		elseif(strlen($value) == 2)
    		{
    			$value = $value[0] . $value[1] . ":00:00";
    			$pvalue = date("g:i a", strtotime($value));
    		}
    	}
    	return $pvalue;
    }

    private function cron_validate_yes($value)
    {
    	$pvalue = 'No';
    	if($value == "1")
    	{
    		$pvalue = 'Yes';
    	}
    	return $pvalue;
    }

    private function cron_validate_amount($value)
    {
    	$pvalue = ((!empty($value) && $value != ".00") ? $value : "");	
    	return $pvalue;
    }

    private function cron_validate_string_w_delimiter($value, $delimeter = ', ')
    {
    	return (!empty($value) ? $value . $delimeter : '');	
    }

    private function object2array($object) 
    {
    	return @json_decode(@json_encode($object),1); 
    }

    private function Zip($source, $destination)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', realpath($file));

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    private function rrmdir($dir) {
        foreach(glob($dir . '/*') as $file) {
            if(is_dir($file))
                rrmdir($file);
            else
                unlink($file);
        }
        rmdir($dir);
    }

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return floor((float)$usec + (float)$sec);
    }

    private function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
    
    // for inserting multi-language content
    // requires the WPML Multilingual CMS plugin
    // reference: https://wpml.org/forums/topic/programmatically-create-wpml-translations/
    private function api_wpml_translate_post( $ori_post_id, $post_type, $lang, $param ){
      // Include WPML API
      include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );

      // Insert translated post
      $post_translated_id = wp_insert_post( $param );

      // Get trid of original post
      $trid = wpml_get_content_trid( 'post_' . $post_type, $ori_post_id );

      // Get default language
      $default_lang = $this->lang_en;

      // Associate original post and translated post
      global $wpdb;
      $wpdb->update( $wpdb->prefix.'icl_translations', array( 'trid' => $trid, 'element_type' => 'post_' . $post_type, 'language_code' => $lang, 'source_language_code' => $default_lang ), array( 'element_id' => $post_translated_id ) );

      // Return translated post ID
      return $post_translated_id;
    }
    
    private function api_wpml_translate_taxonomy( $ori_term_id, $translation, $taxonomy, $param, $lang, $default_lang ){
      // Include WPML API
      include_once( WP_PLUGIN_DIR . '/sitepress-multilingual-cms/inc/wpml-api.php' );

      // Insert translated term
      $term_translated = wp_insert_term( $translation, $taxonomy, $param );
      $term_translated_id = (int)$term_translated['term_id'];

      // Get trid of original term
      $trid = wpml_get_content_trid( 'tax_' . $taxonomy, $ori_term_id );

      // Get default language
      //$default_lang = wpml_get_default_language();
      
      // Associate original post and translated post
      global $wpdb;
      $wpdb->update( $wpdb->prefix.'icl_translations', array( 'trid' => $trid, 'element_type' => 'tax_' . $taxonomy, 'language_code' => $lang, 'source_language_code' => $default_lang ), array( 'element_id' => $term_translated_id, 'element_type' => 'tax_' . $taxonomy ) );

      // Return translated term ID
      return $term_translated_id;
    }
    
    // to check whether the XML string is valid
    private function isXML($xml){
      libxml_use_internal_errors(true);

      $doc = new DOMDocument('1.0', 'utf-8');
      $doc->loadXML($xml);

      $errors = libxml_get_errors();

      if(empty($errors)){
          return true;
      }

      $error = $errors[0];
      if($error->level < 3){
          return true;
      }

      $explodedxml = explode("r", $xml);
      $badxml = $explodedxml[($error->line)-1];

      $message = $error->message . ' at line ' . $error->line . '. Bad XML: ' . htmlentities($badxml);
      return $message;
    }
    
    // substitute for file_get_contents
    private function get_web_page($url){
      $ch = curl_init();
      curl_setopt ($ch, CURLOPT_URL, $url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
      $contents = curl_exec($ch);
      
      if (curl_errno($ch)) {
        return curl_error($ch);
        return "\n<br />";
        $contents = false;
      } else {
        curl_close($ch);
      }
    
      if (!is_string($contents) || !strlen($contents)) {
        echo "Failed to get contents from ".$url.". ";
        $contents = false;
      }
    
      return $contents;
    }

    //get TripAdvisor data via API call
    private function get_tripadvisor_data_raw( $url ){
        /*$TA_API_KEY = 'c8d934065167458c96a19515920be63b';
        $ta_id = $this->get_string_between($url, '-d', '-');
        $api_call = 'http://api.tripadvisor.com/api/partner/2.0/location/'.$ta_id.'?key='.$TA_API_KEY;
        $call_results = @file_get_contents($api_call);
        if(!$call_results){
          $call_results = $this->get_web_page($api_call);
        }
        //$call_results = addslashes($call_results);
        $data = json_decode($call_results);
        if($data){
          return $call_results;
        }else{
          return false;
        }*/
        
        // Temporary replacement for grabbing TripAdvisor data.
        // Data obtained via crawling TripAdvisor page.
        
        // Retrieve all the client-side code of the page requested
        set_error_handler(
            create_function(
                '$severity, $message, $file, $line',
                'throw new ErrorException($message, $severity, $severity, $file, $line);'
            )
        );
        $pageContent = false;
        $parsed = parse_url($url);
        if (empty($parsed['scheme'])) {
          $url = 'https://' . ltrim($url, '/');
        }
        try{
          if( ini_get('allow_url_fopen') ){
            $pageContent = file_get_contents($url);
          }else{
            $pageContent = $this->get_web_page($url);
          }
        } catch (Exception $e) {
          echo 'Trying to read: '.$url.' ';
          echo $e->getMessage();
          return false;
        }
        restore_error_handler();
        if($pageContent === false){
          return false;
        }
        
        // include 3rd party helper
        include_once(ATDW_PLUGIN_DIR."/simple_html_dom.php");
        // get html dom
        $html_dom = str_get_html($pageContent);
        if($html_dom === false){
          return false;
        }
        
        // get rating value, review count
        $REGEX_ldjson = '/<script type=\"application\/ld\+json\">(.*?)<\/script>/is';	
        $itemExtracted_ldjson = preg_match_all($REGEX_ldjson, $pageContent, $itemMatches_ldjson);
        $item_ldjson_raw = $itemMatches_ldjson[1][0];
        $item_ldjson = json_decode($item_ldjson_raw);
        $ratingValue = null;
        if($item_ldjson->aggregateRating->ratingValue){
          $ratingValue = $item_ldjson->aggregateRating->ratingValue;
        }else{
          foreach($html_dom->find('div.heading_rating .ui_bubble_rating') as $e) {
            $temp_rateValue_raw = trim($e->outertext);
            $REGEX_rateValue = '/ui_bubble_rating bubble_(.*?)\">/is';	
            $itemExtracted_rateValue = preg_match_all($REGEX_rateValue, $temp_rateValue_raw, $itemMatches_rateValue);
            $ratingValue = $itemMatches_rateValue[1][0] / 10;
          }
        }
        if($ratingValue){
          $rating_image_url = 'http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/' . number_format($ratingValue, 1, '.', '') . '-37321-5.svg';
        }else{
          $rating_image_url = null;
        }
        $reviewCount = null;
        if($item_ldjson->aggregateRating->reviewCount){
          $reviewCount = $item_ldjson->aggregateRating->reviewCount;
        }else{
          foreach($html_dom->find('.heading_rating .rating .more') as $e) {
            $reviewCount = str_replace(' Reviews', '', trim($e->plaintext));
          }
        }
        
        // get review link
        if($item_ldjson->url){
          $web_url = "www.tripadvisor.com".$item_ldjson->url;
        }else{
          $web_url = str_replace( "https://", "", str_replace("tripadvisor.com.au", "tripadvisor.com", $url) );
        }
        $url_pattern = '/www\.tripadvisor\.com\/(\w+)\-g(.*?)/i';
        $url_replacement = 'www.tripadvisor.com/UserReview-g$2';
        $review_url = preg_replace($url_pattern, $url_replacement, $web_url);
        
        // get ranking string
        $item_rankingstring = null;
        foreach($html_dom->find('.header_popularity') as $header_popularity) {
          $item_rankingstring = trim($header_popularity->plaintext);
        }
        if(!$item_rankingstring){
          foreach($html_dom->find('.slim_ranking') as $header_popularity) {
            $item_rankingstring = trim($header_popularity->plaintext);
          }
        }
        // get reviews
        $reviews = [];
        $r_i = 0;        
        foreach($html_dom->find('div.reviewSelector') as $div) {
          $temp_review_id_raw = trim($div->outertext);
          $REGEX_review_id = '/<div id=\"review_(.*?)\"/is';
          $itemExtracted_review_id = preg_match_all($REGEX_review_id, $temp_review_id_raw, $itemMatches_review_id);
          $item_review_id = $itemMatches_review_id[1][0];
          if(!$item_review_id){
            continue;
          }
          $reviews[$r_i]['id'] = $item_review_id;
          
          $ind_url_pattern = '/www\.tripadvisor\.com\/(\w+)\-g(.*?)\-d(.*?)\-(Reviews|i(.*?))\-(.*?)/i';
          $ind_url_replacement = 'www.tripadvisor.com/ShowUserReviews-g$2-d$3'.'-r'.$item_review_id.'-$5';
          $ind_review_url = preg_replace($ind_url_pattern, $ind_url_replacement, $review_url);
          $reviews[$r_i]['url'] = 'http://' . $ind_review_url . '#review' . $item_review_id;
          
          foreach($div->find('span.ratingDate') as $e) {
             $temp_time = strtotime( str_replace('Reviewed ', '', trim($e->plaintext)) );
             $reviews[$r_i]['published_date'] = date('c', $temp_time);
          }
          foreach($div->find('div.quote span.noQuotes') as $e) {
            $reviews[$r_i]['title'] = trim($e->plaintext);
          }
          foreach($div->find('div.rating span.ui_bubble_rating') as $e) {
            $temp_rate_raw = trim($e->outertext);
            $REGEX_rate = '/ui_bubble_rating bubble_(.*?)\">/is';	
            $itemExtracted_rate = preg_match_all($REGEX_rate, $temp_rate_raw, $itemMatches_rate);
            $item_rate = $itemMatches_rate[1][0] / 10;
            $reviews[$r_i]['rating'] = $item_rate;
            $reviews[$r_i]['rating_image_url'] = 'http://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/s' . number_format($item_rate, 1, '.', '') . '-37321-5.svg';
          }
          foreach($div->find('div.inlineReviewUpdate p.partial_entry') as $e) {
            $reviews[$r_i]['text'] = str_replace('...More', '...', trim($e->plaintext));
          }
          foreach($div->find('div.member_info div.username') as $e) {
            $reviews[$r_i]['user']['username'] = trim($e->plaintext);
          }
          
          $r_i++;
        }
        
        $ta_scraper = [
          "rating" => $ratingValue,
          "rating_image_url" => $rating_image_url,
          "num_reviews" => $reviewCount,
          "web_url" => "https://".$web_url,
          "ranking_data" => [
            "ranking_string" => $item_rankingstring
          ],
          "write_review" => "https://".$review_url,
          "reviews" => $reviews
        ];
        
        return json_encode($ta_scraper);
    }
    
    //get the tripadvisor raw data stored in DB
    public function get_tripadvisor_data( $post_id ){
        $call_results = get_post_meta($post_id, 'tripadvisor_data_raw', true);
        $data = json_decode($call_results);
        return $data;
    }
    
    public function sync_tripadvisor(){
      echo 'Starting TripAdvisor content sync.<br>';
      echo 'Logs will be created at: '.$this->document_root.'<br>';

      if($this->multi_lang){
        // before start, set the language to default
        global $sitepress;
        $sitepress->switch_lang('en');
      }

      // setup
      $process_starts_at = time();
      $output = null;
      $process_per_page = 10;
      $process_sleep = 20; //seconds
      global $wpdb;
      
      //to store the city execution page
      if(!get_option('atdw_sync_tripadvisor_paged')){
        add_option( 'atdw_sync_tripadvisor_paged', 1, '', 'no');
        $atdw_sync_tripadvisor_paged = 1;
      }else{
        $atdw_sync_tripadvisor_paged = get_option('atdw_sync_tripadvisor_paged'); 
      }

      $output.= PHP_EOL.PHP_EOL.'################################## TripAdvisor Sync STARTS  (' . date('Y-m-d H:i:s') . ') ##################################' . PHP_EOL;
      
      // get total records with TripAdvisor fields      
      $ta_total_records = $wpdb->get_results('SELECT post_id FROM wp_postmeta WHERE meta_key LIKE "external_system_text%" AND meta_value LIKE "%tripadvisor%";');
      $ta_total_page = ceil( count($ta_total_records) / $process_per_page );
      $output.= "Found ".count($ta_total_records).' entries with TripAdvisor link added.'.PHP_EOL.PHP_EOL;
      $output.= "\nPage: " . $atdw_sync_tripadvisor_paged . " of " . $ta_total_page . " \n\n";
      // get current page records with TripAdvisor fields
      $ta_query_start = (int)(($atdw_sync_tripadvisor_paged - 1) * $process_per_page);
      $ta_records = $wpdb->get_results('SELECT post_id, meta_value FROM wp_postmeta WHERE meta_key LIKE "external_system_text%" AND meta_value LIKE "%tripadvisor%" ORDER BY post_id ASC LIMIT '.$ta_query_start.', '.$process_per_page.';');
      
      // process TripAdvisor data
      $i = $ta_query_start + 1;
      foreach($ta_records as $ta_record){
        $output.= "#".$i." Editing entry ID ".$ta_record->post_id.". ";
        // get TripAdvisor data via API call
        $ta_data = $this->get_tripadvisor_data_raw($ta_record->meta_value);
        $output.= ' ('.date('Y-m-d H:i:s').') ';
        if($ta_data){
          $output.= strlen($ta_data)." length of string received. ";
          //store into DB
          $result = update_post_meta($ta_record->post_id, 'tripadvisor_data_raw', $ta_data);
        }else{
          $output.= " Error reading source. ";
          $result = false;
        }
        
        //get the tripadvisor raw data stored in DB
        $ta_data_object = $this->get_tripadvisor_data($ta_record->post_id);
        if($ta_data_object){
          //store the tripadvisor ratings, to used for sorting on front end
          $result2 = update_post_meta($ta_record->post_id, 'tripadvisor_rating', (float) $ta_data_object->rating);
        }

        $output.= " Result: ";
        if($result == '1'){
          $output.= "Existing data updated.";
        }else if($result){
          $output.= "New record. Meta id ".$result;
        }else{
          $output.= "Data not updated (same value).";
        }
        $output.= PHP_EOL;
        $i++;
        
        // to avoid timeout
        sleep($process_sleep);
      }
      
      $output.= PHP_EOL.PHP_EOL. '============= COMPLETED (' . date('Y-m-d H:i:s') . ') =============' . PHP_EOL;

      $process_starts_at = ((time()) - $process_starts_at);
      $output.= $process_starts_at . " Second" . ($process_starts_at > 1 ? 's' : '') . PHP_EOL.PHP_EOL;
      $output.= PHP_EOL."System terminated. ".PHP_EOL.PHP_EOL." ATDW Wordpress Plugin ";
      
      // saving into log files
      // create folder if yet exist
      if (!file_exists($this->document_root . 'logs/') && !is_dir($this->document_root . 'logs/')) {
      	mkdir($this->document_root . 'logs/');
      }
      if (!file_exists($this->document_root . 'logs/tripadvisor/') && !is_dir($this->document_root . 'logs/tripadvisor/')) {
      	mkdir($this->document_root . 'logs/tripadvisor/');
      }
      $cronFile = $this->document_root . 'logs/tripadvisor/' . date("Y_m_d") . '.log';
      $fh = fopen($cronFile, 'a') or die("can't open file");
      fwrite($fh, $output);
      fclose($fh);
      
      //if is at last page
      if( $atdw_sync_tripadvisor_paged == $ta_total_page ){
        //reset pagination
        $atdw_sync_tripadvisor_paged = 1;
      }else{
        //increase pagination
        $atdw_sync_tripadvisor_paged ++;
      }
      //store the pagination
      update_option('atdw_sync_tripadvisor_paged', $atdw_sync_tripadvisor_paged);

      // output to page
      echo nl2br($output);
    }
    
    public function sync_preview(){
      echo 'Retriving ATDW Listings sync preview.<br>';
      $process_starts_at = time();
      $output = null;
      
      //to store the city execution page
      if(!get_option('atdw_sync_city_paged')){
        add_option( 'atdw_sync_city_paged', 1, '', 'no');
        $atdw_sync_city_paged = 1;
      }else{
        $atdw_sync_city_paged = get_option('atdw_sync_city_paged'); 
      }
      
      if($this->multi_lang){
        // before start, set the language to default
        global $sitepress;
        $sitepress->switch_lang('en');
      }
      
      // retriving towns list
      $args = array(
      			"post_type" => "towns",
      			"post_status" => "publish",
      			"posts_per_page" => $this->town_sync_perpage,
      			"paged" => $atdw_sync_city_paged,
      			"order" => "ASC",
      			"orderby" => "title"
      			);
      $the_query = new WP_Query( $args );
      
      $cities = array();
      $c_i = 0;
      foreach ($the_query->posts as $city_post)
      {
      	if(!empty($city_post->post_title))
      	{
      		$cities[$c_i]['name'] = trim($city_post->post_title);
      		$cities[$c_i]['state'] = get_post_meta($city_post->ID, 'town_state', true);
      		//$region_terms = wp_get_post_terms($city_post->ID, 'towns_cat');
      		//$region_names = wp_list_pluck($region_terms, 'name'); //need to add checking on whether region is empty for this city
      		//$cities[$c_i]['region'] = implode(',', $region_names);
      		$cities[$c_i]['show_on_frontend'] = get_post_meta($city_post->ID, 'town_showonfrontend', true);
      		$c_i++;
      	}
      }
      $output.= "\nTotal Cities count: " . $the_query->found_posts . "\n";
      $output.= "\nThe following are the amount of available entries for each town/city on ATDW Portal that are to be synced to this site. \n";
      $output.= "\nPage: " . $atdw_sync_city_paged . " of " . $the_query->max_num_pages . " \n\n";
      
      $i = 1;
      $overall_atdw_entries = 0;
      foreach($cities as $city){
      	$city_name = $city['name'];
      	$city_state = $city['state'];
        //$city_region = $city['region'];
        $city_show_on_frontend = ($city['show_on_frontend'] == 1) ? '(Front and Backend)' : '(Backend only)';
        
        $city_total_atdw_entries = 0;
        
        //getting info from ATDW
        $strURL	= 'http://' . $this->atdw_api_host . '/api/atlas/products?key=' . $this->atdw_api_key . '&st='. $city_state .'&ct=' . str_replace(' ', '%20', $city_name) . '&size=300&fl=product_id,product_category';
        $strXML	= @file_get_contents(str_replace(' ', '%20', $strURL));
        if(!$strXML){
          $strXML = $this->get_web_page(str_replace(' ', '%20', $strURL));
        }
        $checkXML = $this->isXML($strXML);
        if($checkXML === true){
          $businesses_list = new SimpleXMLElement($strXML);
          $businesses_list = $this->object2array($businesses_list);
          
          // ATDW categories that to be excluded from this sync
          $remove_cats = array('HIRE', 'TRANSPORT', 'JOURNEY', 'INFO', 'DESTINFO');
          if(((int)$businesses_list['number_of_results']) == 1){
            if(!in_array($businesses_list['products']['product_record']['product_category_id'], $remove_cats, false)){
              $city_total_atdw_entries++;
            }
          }elseif(((int)$businesses_list['number_of_results']) > 1){
            foreach($businesses_list['products']['product_record'] as $atdw_entries){
              if( !in_array($atdw_entries['product_category_id'], $remove_cats, false) ){
                $city_total_atdw_entries++;
              }
            }
          }
        }
        
        $output.= ((max(1, $atdw_sync_city_paged) - 1) * $this->town_sync_perpage + $i) . ". " . $city_name . ", " . $city_state ." " . $city_show_on_frontend . ":  ". $city_total_atdw_entries ." entries \n";
        
        $overall_atdw_entries += $city_total_atdw_entries;
        $i++;
      }
      $output.= "\n\nOverall available entries for this execution: ".$overall_atdw_entries;
      
      //output to screen
      echo nl2br($output);
      
      //if is at last page
      if($atdw_sync_city_paged == $the_query->max_num_pages){
        //reset pagination
        $atdw_sync_city_paged = 1;
      }else{
        //increase pagination
        $atdw_sync_city_paged ++;
      }
      //store the pagination
      update_option('atdw_sync_city_paged', $atdw_sync_city_paged);
      
      return;
    }
    
  } // END class ATDW_Sync
} // END if(!class_exists('ATDW_Sync'))  