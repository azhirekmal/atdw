<?php

	class ATDW_Tour extends ATDW_tool
	{
		const POST_TYPE	= "atdw_tour";
		const SLUG	= "tours";
		private $CPT_LABEL;
		
    	/**
    	 * The Constructor
    	 */
    	public function __construct()
    	{
				// setup labels, according to the fields in "Settings" page
				$this->CPT_LABEL = get_option('atdw_label_tour') ? ucfirst(get_option('atdw_label_tour')) : "Tour";
				
				// register actions
				add_action('init', array(&$this, 'init'));
				add_action('admin_init', array(&$this, 'initiate_atdw_meta_atdw_tour'));
				add_action('admin_init', array(&$this, 'admin_init'));
				add_action('post_edit_form_tag', array(&$this, 'update_edit_form'));

				// setup menus
				add_action('admin_menu', array(&$this, 'add_menu'), 10);
    	} // END public function __construct()

    	/**
    	 * hook into WP's init action hook
    	 */
    	public function init()
    	{
				// Permalinks
				add_filter( 'rewrite_rules_array', array(&$this, 'add_rewrite_rules') );
				add_filter( 'post_type_link', array(&$this, 'filter_post_type_link'), 10, 2 );
				
				// Initialize Post Type
				$this->create_post_type();
				$this->create_taxonomies();
    		
				add_action('save_post', array(&$this, 'save_post_atdw'));
        
    	} // END public function init()

    	/**
    	 * Create the post type
    	 */
    	public function create_post_type()
    	{
    		register_post_type(self::POST_TYPE,
    			array(
    				'labels' => array(
							'name' => _x($this->CPT_LABEL, 'post type general name'),
           		'singular_name' => _x($this->CPT_LABEL.'s', 'post type singular name'),
           		'add_new' => _x('Add New', $this->CPT_LABEL),
           		'add_new_item' => __('Add New '.$this->CPT_LABEL),
           		'edit_item' => __('Edit '.$this->CPT_LABEL),
           		'new_item' => __('New '.$this->CPT_LABEL),
           		'view_item' => __('View '.$this->CPT_LABEL),
           		'search_items' => __('Search '.$this->CPT_LABEL.'s'),
           		'not_found' =>  __('Nothing found'),
           		'not_found_in_trash' => __('Nothing found in Trash'),
           		'parent_item_colon' => ''
    				),
            'public' => true,
         		'publicly_queryable' => true,
         		'exclude_from_search' => false,
         		'show_ui' => true,
         		'show_in_menu' => 'atdw',
         		'query_var' => true,
         		//'menu_icon' => 'dashicons-palmtree',
				'rewrite' => array( 'slug' => self::SLUG.'/%atdw_cpt_category%', 'with_front' => false ),
         		'capability_type' => 'post',
         		'has_archive' => true,
         		'hierarchical' => false,
         		'menu_position' => null,
         		'supports' => array('title','thumbnail')
    			)
    		);
    	}
      
      /**
    	 * Create taxonomy, hierarchical
    	 */
      public function create_taxonomies() {
       	$labels = array(
       		'name'                          => $this->CPT_LABEL.' Categories',
       		'singular_name'                 => 'Category',
       		'search_items'                  => 'Search Categories',
       		'popular_items'                 => 'Popular Categories',
       		'all_items'                     => 'All Categories',
       		'parent_item'                   => 'Parent Category',
       		'edit_item'                     => 'Edit Category',
       		'update_item'                   => 'Update Category',
       		'add_new_item'                  => 'Add New Category',
       		'new_item_name'                 => 'New category',
       		'separate_items_with_commas'    => 'Separate categories with commas',
       		'add_or_remove_items'           => 'Add or remove categories',
       		'choose_from_most_used'         => 'Choose from most used categories'
       	);
       		
       	$args = array(
       		'label'                         => $this->CPT_LABEL.' Categories',
       		'labels'                        => $labels,
       		'public'                        => true,
       		'hierarchical'                  => true,
       		'show_ui'                       => true,
       		'show_in_nav_menus'             => true,
       		'args'                          => array( 'orderby' => 'term_order' ),
       		'rewrite'                       => array( 'slug' => self::SLUG, 'with_front' => false ),
       		'query_var'                     => true
       	);
       	
       	register_taxonomy( sprintf('%s_cat', self::POST_TYPE), self::POST_TYPE, $args);

       } // End of create_taxonomies() function.
			 
			 /**
      	 * add menus
      	 */		
      	public function add_menu()
      	{
	 	      add_submenu_page(
 	          'atdw',
 	          $this->CPT_LABEL.' Categories',
 	          'Category - '. $this->CPT_LABEL,
 	          'manage_categories',
 	          'edit-tags.php?taxonomy='.sprintf('%s_cat', self::POST_TYPE).'&post_type='.self::POST_TYPE
	 	      );
	 	      // Load the additional JS conditionally
	 	      // Temporary fix on the error of admin menu not higlighting this section when user is on this page. Need a proper fix.
	 	      $temp_get_taxonomy = (isset($_GET['taxonomy'])) ? $_GET['taxonomy'] : null;
	 	      if($temp_get_taxonomy == sprintf('%s_cat', self::POST_TYPE)){ 
	 	        wp_enqueue_script( 'atdw-tags-script', ATDW_PLUGIN_URL . '/assets/js/tags.js', array(), filemtime(ATDW_PLUGIN_DIR . '/assets/js/tags.js'));
						
	 	        // fixing the issue of submenu not highlighted
	 	        add_filter('parent_file', array(&$this, 'select_submenu'));
	 	      }
 	    	} // END public function add_menu()
				
			 /**
			  * fixing the issue of submenu not highlighted
			  */		
			 public function select_submenu($file)
			 {
	 	      global $parent_file;
	 	      global $submenu_file;
	 	      $parent_file = 'admin.php?page=atdw';
	 	      $submenu_file = 'edit-tags.php?taxonomy='.sprintf('%s_cat', self::POST_TYPE).'&post_type='.self::POST_TYPE;
			 } // END public function select_submenu()
			 
			 /**
     	 * Add rewrite rules
     	 */
			 public function add_rewrite_rules( $rules ) {
       	$new = array();
       	$temp_rule_1 = self::SLUG.'/([^/]+)/(.+)/?$';
       	$temp_rule_2 = self::SLUG.'/(.+)/?$';
       	$temp_rule_3 = self::SLUG.'/?$';
       	$new[$temp_rule_1] = 'index.php?'.self::POST_TYPE.'=$matches[2]';
       	$new[$temp_rule_2] = 'index.php?'.sprintf('%s_cat', self::POST_TYPE).'=$matches[1]';
       	$new[$temp_rule_3] = 'index.php?post_type=' . self::POST_TYPE;

       	return array_merge( $new, $rules ); // Ensure our rules come first
       } // End of add_rewrite_rules( $rules ) function.
			 
			 /**
     	 * Handle the '%atdw_cpt_category%' URL placeholder
     	 */
     	 function filter_post_type_link( $link, $post ) {
     	 	if ( $post->post_type == self::POST_TYPE ) {
     	 		if ( $cats = get_the_terms( $post->ID, sprintf('%s_cat', self::POST_TYPE) ) ) {
     	 			$link = str_replace( '%atdw_cpt_category%', current( $cats )->slug, $link );
     	 		}else{
     	 			//if this entry doesn't have any category assigned to it
     	 			$link = str_replace( '%atdw_cpt_category%', 'others', $link );
     	 		}
     	 	}
     	 	return $link;
     	 } // End of filter_post_type_link( $link, $post ) function.
       
       /**
     	 * hook into WP's admin_init action hook
     	 */
      	public function admin_init()
      	{			
      	 	// add custom columns on admin page
      	 	add_filter('manage_'.self::POST_TYPE.'_posts_columns', array(&$this, 'cpt_columns_head'));
      	 	add_action('manage_'.self::POST_TYPE.'_posts_custom_column', array(&$this, 'cpt_columns_content'), 10, 2);
      	} // END public function admin_init()
 			
      	/**
      	 * add custom column at admin page
      	 */
      	public function cpt_columns_head( $columns ) {
      	 	$new_columns['cb'] = '<input type="checkbox" />';
      	 	$new_columns['title'] = 'Title';
          $new_columns['city'] = 'City';
      	 	//$new_columns['region'] = 'Region';
      	 	$new_columns['state'] = 'State';
      	 	$new_columns['date'] = 'Date';
      	 	return $new_columns;
      	}
 			
      	public function cpt_columns_content( $column_name, $post_ID ) {
      	 	switch ($column_name) {
            case 'city':
       	 			echo get_post_meta($post_ID, 'atdw_pr_city', true);
       	 			break;
            case 'region':
      	 			echo get_post_meta($post_ID, 'atdw_pr_region', true);
      	 			break;
      	 		case 'state':
      	 			echo get_post_meta($post_ID, 'atdw_pr_state', true);
      	 			break;
      	 	}
      	}
      
      /**
    	 * init the meta box
    	 */
      public function initiate_atdw_meta_atdw_tour()
      { 
       add_meta_box("atdw_meta_atdw_tour", "ADTW Information Details - Tours (Tours & Trails)", array(&$this, "atdw_meta_atdw_tour"), self::POST_TYPE, "normal", "high");	
       
       //submit box
       add_meta_box( "submitdiv", "Publish", "post_submit_meta_box", self::POST_TYPE, "side", "high" );        
      }
      
      /**
    	 * the meta box
    	 */
      public function atdw_meta_atdw_tour()
			{
       	global $dataGlobal;
       	global $post;
       	
       	if( $post->ID || ($_GET['post_type'] == self::POST_TYPE))
       	{
       		if(is_numeric($post->ID))
       		{
       			$atdw_data = $this->get_post_custom_compress($post->ID);
       			$this->hook_atdw_js($atdw_data);
       			
       			echo "<h2><strong>Business Information</strong></h2>";
       			$this->atdw_data_process($atdw_data, "profile", self::POST_TYPE);		
       			
       			echo "<h2><strong>Communications</strong></h2>";
       			$this->atdw_data_process($atdw_data, "communications", self::POST_TYPE);
       			
       			echo "<h2><strong>Facilities &amp; Activities</strong></h2>";
       			$this->atdw_data_process($atdw_data, "facilities", self::POST_TYPE);
       			$this->atdw_data_process($atdw_data, "activities", self::POST_TYPE);
       			$this->atdw_data_process($atdw_data, "things to do", self::POST_TYPE);
       			
       			echo "<h2><strong>Media</strong></h2>";
       			$this->atdw_data_process($atdw_data, "media", self::POST_TYPE);	
       		}
       	}else{
       		echo $dataGlobal->notApplicableMsg;
       	}
      }
      
      /**
    	 * to enable file upload
    	 */
      function update_edit_form() {
      		echo ' enctype="multipart/form-data"';
      }
      
      /**
    	 * Save the metaboxes for this custom post type
    	 */
    	public function save_post_atdw()
			{
      	global $post, $wpdb;
      
      	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
      	{
      		return $post_id;
      	}
      	if( isset($post->post_type) && ($post->post_type == self::POST_TYPE) )
      	{		
          //Business Information > Identification Number > ATDW
      		if(!empty($_POST['input_atdw_profile_atdw'])){
      			//multi-language
      			$temp_post_excerpt = 'atdw_buss_id||' . $_POST['input_atdw_profile_atdw'];
      		}else{
      			$temp_post_excerpt = '';
      		}
      		$temp_post_id = $post->ID;
      		$wpdb->update('wp_posts', array(
      										'post_excerpt' 	=> $temp_post_excerpt,
      										'post_modified'	=> date('Y-m-d H:i:s', (time() - 86400)),
      										'post_modified_gmt'	=> date('Y-m-d H:i:s', (time() - 86400)),
      									), array('ID' 		=> $temp_post_id));
      		
      		//Business Information > Identification Number > ABN
      		update_post_meta($post->ID, "atdw_pr_abn", $_POST['input_atdw_profile_abn']);
      		
      		//Business Information > Sync
      		if(!empty($_POST['input_atdw_profile_sync']) && ($_POST['input_atdw_profile_sync'] == '1') )
      		{
      			update_post_meta($post->ID, "atdw_sync_toggle", "1");
      		}
      		else
      		{
      			update_post_meta($post->ID, "atdw_sync_toggle", "0");
      		}

          // //Business Information > Sync
          // if(!empty($_POST['input_atdw_profile_showonregionalhighlight']) && ($_POST['input_atdw_profile_showonregionalhighlight'] == '1') )
          // {
          //   update_post_meta($post->ID, "atdw_show_on_highlight_toggle", "1");
          // }
          // else
          // {
          //   update_post_meta($post->ID, "atdw_show_on_highlight_toggle", "0");
          // }
      		
      		//Business Information > Location > City
      		if(!empty($_POST['input_atdw_profile_city'])){
      			update_post_meta($post->ID, "atdw_pr_city", $_POST['input_atdw_profile_city']);
      		}
          
      		//Business Information > Location > Region
      		if(!empty($_POST['input_atdw_profile_region'])){
      			update_post_meta($post->ID, "atdw_pr_region", $_POST['input_atdw_profile_region']);
      		}
      		
      		//Business Information > Location > State
      		if(!empty($_POST['input_atdw_profile_state'])){
      			update_post_meta($post->ID, "atdw_pr_state", $_POST['input_atdw_profile_state']);
      		}
      		
      		//Business Information > Description
      		if(!empty($_POST['input_atdw_profile_description'])){
      			$temp_description = str_replace("\n","<br>",$_POST['input_atdw_profile_description']);			
      			update_post_meta($post->ID, "atdw_pr_product_description", $temp_description);
      		}
					
      		//Business Information > Short Description
      		$temp_sdescription = str_replace("\n","<br>",$_POST['input_atdw_profile_shortdescription']);			
      		update_post_meta($post->ID, "atdw_pr_product_short_description", $temp_sdescription);
      		
      		//Business Information > Product Rate > From
      		update_post_meta($post->ID, "atdw_pr_all_rate_from", $_POST['input_atdw_profile_from']);
      		
      		//Business Information > Product Rate > To
      		update_post_meta($post->ID, "atdw_pr_all_rate_to", $_POST['input_atdw_profile_to']);

					//Business Information > Star Rating
					update_post_meta($post->ID, "atdw_pr_product_attr_rating-1", $_POST['input_atdw_profile_starrating']);
      		
      		//Business Information > Time > Reception Hours
      		if(isset($_POST['input_atdw_profile_receptionhours'])){
      			update_post_meta($post->ID, "atdw_pr_reception_hours_txt", $_POST['input_atdw_profile_receptionhours']);
      		}
      		
      		//Business Information > Time > Check-In
      		if(isset($_POST['input_atdw_profile_checkin'])){
      			update_post_meta($post->ID, "atdw_pr_check-in_time", $_POST['input_atdw_profile_checkin']);
      		}
      		
      		//Business Information > Time > Check-Out
      		if(isset($_POST['input_atdw_profile_checkout'])){
      			update_post_meta($post->ID, "atdw_pr_check-out_time", $_POST['input_atdw_profile_checkout']);
      		}
      		
      		//Business Information > Facilities > Children Rate
      		update_post_meta($post->ID, "atdw_pr_children_catered", $_POST['input_atdw_profile_childrencatered']);
      		
      		//Business Information > Facilities > Disabled Access
      		//update_post_meta($post->ID, "atdw_pr_disabled_access", $_POST['input_atdw_profile_disabledaccess']);
      		
      		//Business Information > Facilities > Pet Allowed
      		update_post_meta($post->ID, "atdw_pr_pet_allowed", $_POST['input_atdw_profile_petallowed']);
      		
      		//Business Information > Facilities > Number of Room(s)
      		if(isset($_POST['input_atdw_profile_numberofrooms'])){
      			update_post_meta($post->ID, "atdw_pr_room_number", $_POST['input_atdw_profile_numberofrooms']);
      		}

					//Business Information > Accessibility
					update_post_meta($post->ID, "atdw_pr_disabled_access", $_POST['input_atdw_profile_accessibility']);
      		
      		//Business Information > Booking Button Landing Page
      		$temp = trim($_POST['input_atdw_booking_button']);
      		if($temp == "http://"){
      			$temp = '';
      		}
      		update_post_meta($post->ID, "atdw_booking_button", $temp);
      		if( (!empty($temp) && ($temp != "http://") ) )
      		{
      			update_post_meta($post->ID, "atdw_booking_button_int", "1");
      		}
      		else
      		{
      			update_post_meta($post->ID, "atdw_booking_button_int", "0");
      		}
      		
      		//Communications > Addresses > Location
      		if(!empty($_POST['input_atdw_communications_location'])){
      			update_post_meta($post->ID, "atdw_pr_pysical_addr", $_POST['input_atdw_communications_location']);
      		}else{
      			//echo some error message
      		}
      		
      		//Communications > Addresses > Postal
      		update_post_meta($post->ID, "atdw_pr_postal", $_POST['input_atdw_communications_postal']);
      		
      		//Communications > Addresses Latitude & Longitude > Latitude
      		update_post_meta($post->ID, "atdw_pr_pysical_lat", $_POST['input_atdw_communications_latitude']);
      		
      		//Communications > Addresses Latitude & Longitude > Longitude
      		update_post_meta($post->ID, "atdw_pr_pysical_long", $_POST['input_atdw_communications_longitude']);
      		
      		//Communications > Contact Number > Phone Number
      		if(!empty($_POST['input_atdw_communications_phonenumber'])){
      			update_post_meta($post->ID, "atdw_pr_comm_phone", $_POST['input_atdw_communications_phonenumber']);
      		}
      		
      		//Communications > Contact Number > Fax Number
      		update_post_meta($post->ID, "atdw_pr_comm_fax", $_POST['input_atdw_communications_faxnumber']);
      		
      		//Communications > Contact Number > Mobile Number
      		update_post_meta($post->ID, "atdw_pr_comm_mobile", $_POST['input_atdw_communications_mobilenumber']);
      		
      		//Communications > Contact Number > Tollfree Number
      		update_post_meta($post->ID, "atdw_pr_comm_tollfree", $_POST['input_atdw_communications_tollfreenumber']);
      		
      		//Communications > E-Communication > Website
      		update_post_meta($post->ID, "atdw_pr_comm_website", $_POST['input_atdw_communications_website']);
      		
      		//Communications > E-Communication > Email
      		update_post_meta($post->ID, "atdw_pr_comm_email", $_POST['input_atdw_communications_email']);

      		//Communications > E-Communication > Social Links
      		$temp_sl_query = $wpdb->get_results("SELECT meta_key, meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key like 'external_system%'");
      		$sl_ta_field = null;
      		$sl_fb_field = null;
      		$sl_tw_field = null;
      		$sl_ig_field = null;
      		$tmp_opentime_field = null; //Business Information > Operating Hours
      		$sl = 0;
      		foreach ($temp_sl_query as $sl_k => $sl_v) {
						if($sl_v->meta_value == 'TRIPADVISO'){
							$sl_ta_field = str_replace('external_system_code', 'external_system_text', $sl_v->meta_key);
						}
						if($sl_v->meta_value == 'FACEBOOK'){
							$sl_fb_field = str_replace('external_system_code', 'external_system_text', $sl_v->meta_key);
						}
						if($sl_v->meta_value == 'TWITTER'){
							$sl_tw_field = str_replace('external_system_code', 'external_system_text', $sl_v->meta_key);
						}
						if($sl_v->meta_value == 'INSTAGRAM'){
							$sl_ig_field = str_replace('external_system_code', 'external_system_text', $sl_v->meta_key);
						}
						if($sl_v->meta_value == 'OPENTIME'){
							$tmp_opentime_field = str_replace('external_system_code', 'external_system_text', $sl_v->meta_key);
						}
						$sl++;
      		}
      		$sl = absint($sl/2) + 1;
      		if($sl_ta_field){
						update_post_meta($post->ID, $sl_ta_field, $_POST['input_atdw_communications_tripadvisor']);
      		}else{
						update_post_meta($post->ID, 'external_system_code-'.$sl, 'TRIPADVISO');
						update_post_meta($post->ID, 'external_system_text-'.$sl, $_POST['input_atdw_communications_tripadvisor']);
						$sl++;
      		}
      		if($sl_fb_field){
						update_post_meta($post->ID, $sl_fb_field, $_POST['input_atdw_communications_facebook']);
      		}else{
						update_post_meta($post->ID, 'external_system_code-'.$sl, 'FACEBOOK');
						update_post_meta($post->ID, 'external_system_text-'.$sl, $_POST['input_atdw_communications_facebook']);
						$sl++;
      		}
      		if($sl_tw_field){
						update_post_meta($post->ID, $sl_tw_field, $_POST['input_atdw_communications_twitter']);
      		}else{
						update_post_meta($post->ID, 'external_system_code-'.$sl, 'TWITTER');
						update_post_meta($post->ID, 'external_system_text-'.$sl, $_POST['input_atdw_communications_twitter']);
						$sl++;
      		}
      		if($sl_ig_field){
						update_post_meta($post->ID, $sl_ig_field, $_POST['input_atdw_communications_instagram']);
      		}else{
						update_post_meta($post->ID, 'external_system_code-'.$sl, 'INSTAGRAM');
						update_post_meta($post->ID, 'external_system_text-'.$sl, $_POST['input_atdw_communications_instagram']);
						$sl++;
      		}
      		//Business Information > Operating Hours
      		if($tmp_opentime_field){
						update_post_meta($post->ID, $tmp_opentime_field, $_POST['input_atdw_profile_operatinghours']);
      		}else{
						update_post_meta($post->ID, 'external_system_code-'.$sl, 'OPENTIME');
						update_post_meta($post->ID, 'external_system_text-'.$sl, $_POST['input_atdw_profile_operatinghours']);
						$sl++;
      		}
      		
      		//Facilities & Activities > Facilities
      		for($i = 1; $i < 25; $i++){
      			$temp_wp_field = "atdw_pr_product_attr_facility-" . $i;
      			$temp_form_field = "input_atdw_facility-" . $i;
      			update_post_meta($post->ID, $temp_wp_field, $_POST[$temp_form_field]);
      		}
      		
      		//Facilities & Activities > Activities
      		for($i = 1; $i < 25; $i++){
      			$temp_wp_field = "atdw_pr_product_attr_activity-" . $i;
      			$temp_form_field = "input_atdw_activity-" . $i;
      			update_post_meta($post->ID, $temp_wp_field, $_POST[$temp_form_field]);
      		}
      		
      		//Facilities & Activities > Things To Do
      		for($i = 1; $i < 25; $i++){
      			$temp_wp_field = "atdw_pr_product_attr_experience-" . $i;
      			$temp_form_field = "input_atdw_experience-" . $i;
      			update_post_meta($post->ID, $temp_wp_field, $_POST[$temp_form_field]);
      		}
      		
      		//Media
      		//grab the existing images details and stored into a temporary arrays, for further usage (remove image, add new image)
      		//filenames
      		$temp_img_files = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_image-%' AND meta_value != '' ORDER BY meta_key", ARRAY_A);
      		//total images
      		$images_uploaded = sizeof($temp_img_files);
      		//alt text
      		$temp_img_alt = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_img_alt-%' ORDER BY meta_key", ARRAY_A);
      		//get image sequence
      		$temp_query = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key = 'atdw_bmm_img_sequence' LIMIT 0, 1");
      		$temp_img_sequence = json_decode($temp_query, true);
      		//store into a temporary array
      		$temp_img_arr = array();
      		for($i = 0; $i < $images_uploaded; $i++){
      			$temp_img_arr[$i]['img'] = $temp_img_files[$i]['meta_value'];
      			$temp_img_arr[$i]['alt'] = $temp_img_alt[$i]['meta_value'];
      		}
      		
      		//Media > Remove image
      		for($i = 1; $i < ($images_uploaded + 1); $i++){
      			if(!empty($_POST['input_atdw_removimage-'.$i]) && ($_POST['input_atdw_removimage-'.$i] == '1') ){
      				unset($temp_img_arr[($i-1)]);
      				unset($temp_img_sequence[$i]);
      			}
      		}
      		//store new images details
      		//first empty the existing
      		$temp_query = $wpdb->query("UPDATE wp_postmeta SET meta_value = '' WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_image-%'");
      		$temp_query = $wpdb->query("UPDATE wp_postmeta SET meta_value = '' WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_img_alt-%'");
      		//update the new image details
      		$i = 0;
      		foreach($temp_img_arr as $temp_img){
      			$temp_wp_field_img = "atdw_bmm_image-" . ($i + 1);
      			$temp_wp_field_txt = "atdw_bmm_img_alt-" . ($i + 1);
      			$wpdb->query("UPDATE wp_postmeta SET meta_value = '".$temp_img['img']."' WHERE post_id = '" . $post->ID . "' AND meta_key = '".$temp_wp_field_img."'");
      			$wpdb->query("UPDATE wp_postmeta SET meta_value = '".$temp_img['alt']."' WHERE post_id = '" . $post->ID . "' AND meta_key = '".$temp_wp_field_txt."'");
      			$i++;
      		}
      		//set new image sequence
      		$new_image_sequence = array();
      		$isq = 1;
      		foreach($temp_img_sequence as $img_seq){
      			$new_image_sequence[$isq] = $img_seq;
      			$isq ++;
      		}
      		$temp_img_sequence = $new_image_sequence;
      		update_post_meta($post->ID, "atdw_bmm_img_sequence", json_encode($temp_img_sequence));	
      		
      		//Media > Add New Image
      		$allowedExts = array("jpg", "jpeg", "gif", "png");
      		$ext = end(explode(".", strtolower($_FILES["input_atdw_newimage"]["name"])));
      		if ((($_FILES["input_atdw_newimage"]["type"] == "image/gif")	|| ($_FILES["input_atdw_newimage"]["type"] == "image/jpeg") || ($_FILES["input_atdw_newimage"]["type"] == "image/jpg") || ($_FILES["input_atdw_newimage"]["type"] == "image/png")	|| ($_FILES["input_atdw_newimage"]["type"] == "image/pjpeg")) && in_array($ext, $allowedExts)) {
      			if ($_FILES["input_atdw_newimage"]["error"] > 0) {
      				//echo error message
      			}else{
      				$uploadpath = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/'.date('Y').'/'.date('m').'/';
      				//remove extension
      				$filename_no_extension = pathinfo($_FILES["input_atdw_newimage"]["name"], PATHINFO_FILENAME);
      				//remove any special character with space
      				$file_name_clean = preg_replace("/[^a-zA-Z0-9]/", " ", $filename_no_extension);
      				//remove double space
      				$file_name = preg_replace('!\s+!', '_', $file_name_clean);
      				//remove space with underscore
      				$file_name = preg_replace('/\s\s+/', '_', $file_name);
      				//get file extension
      				$file_ext = strtolower(pathinfo($_FILES["input_atdw_newimage"]["name"], PATHINFO_EXTENSION));
      				//full file name with path
      				$file_name_upload = "$uploadpath" . $file_name . "." . $file_ext;
      				//full file name to store in DB
      				$file_name_db = get_site_url() . '/wp-content/uploads/'.date('Y').'/'.date('m').'/' . $file_name . '.' . $file_ext;
      				//upload file
      				if (move_uploaded_file($_FILES["input_atdw_newimage"]["tmp_name"], $file_name_upload)) {
      					//check how many images have been uploaded for this entry
      					$temp1 = $wpdb->get_var("SELECT count(*) FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_image-%' AND meta_value != ''");
      					$images_uploaded = (int)$temp1;
      					//set the fields
      					$temp_wp_field_img = "atdw_bmm_image-" . ($images_uploaded + 1);
      					$temp_wp_field_txt = "atdw_bmm_img_alt-" . ($images_uploaded + 1);
      					//set new image sequence
      					$new_img_seq = $images_uploaded + 1;
      					$temp_img_sequence[$new_img_seq] = ($new_img_seq * 100);
      					//update post meta
      					update_post_meta($post->ID, $temp_wp_field_img, $file_name_db);
      					update_post_meta($post->ID, $temp_wp_field_txt, $_POST['input_atdw_newimage_text']);
      					update_post_meta($post->ID, "atdw_bmm_img_sequence", json_encode($temp_img_sequence));
      				}
      			}
      		}
          
      		// Media > Video
      		//grab the existing videos details and stored into a temporary arrays, for further usage (remove video, add new video)
      		//filenames
      		$temp_vid_files = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_video-%' AND meta_value != '' ORDER BY meta_key", ARRAY_A);
      		//total videos
      		$videos_uploaded = sizeof($temp_vid_files);
      		//alt text
      		$temp_vid_alt = $wpdb->get_results("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_vid_alt-%' ORDER BY meta_key", ARRAY_A);
      		//get video sequence
      		$temp_query = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key = 'atdw_bmm_vid_sequence' LIMIT 0, 1");
      		$temp_vid_sequence = json_decode($temp_query, true);
      		//store into a temporary array
      		$temp_vid_arr = array();
      		for($i = 0; $i < $videos_uploaded; $i++){
      			$temp_vid_arr[$i]['vid'] = $temp_vid_files[$i]['meta_value'];
      			$temp_vid_arr[$i]['alt'] = $temp_vid_alt[$i]['meta_value'];
      		}
          
      		//Media > Remove video
      		for($i = 1; $i < ($videos_uploaded + 1); $i++){
      			if(!empty($_POST['input_atdw_removvideo-'.$i]) && ($_POST['input_atdw_removvideo-'.$i] == '1') ){
      				unset($temp_vid_arr[($i-1)]);
      				unset($temp_vid_sequence[$i]);
      			}
      		}
      		//store new videos details
      		//first empty the existing
      		$temp_query = $wpdb->query("UPDATE wp_postmeta SET meta_value = '' WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_video-%'");
      		$temp_query = $wpdb->query("UPDATE wp_postmeta SET meta_value = '' WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_vid_alt-%'");
      		//update the new video details  
      		$i = 0;
      		foreach($temp_vid_arr as $temp_vid){
      			$temp_wp_field_vid = "atdw_bmm_video-" . ($i + 1);
      			$temp_wp_field_txt = "atdw_bmm_vid_alt-" . ($i + 1);
      			$wpdb->query("UPDATE wp_postmeta SET meta_value = '".$temp_vid['vid']."' WHERE post_id = '" . $post->ID . "' AND meta_key = '".$temp_wp_field_vid."'");
      			$wpdb->query("UPDATE wp_postmeta SET meta_value = '".$temp_vid['alt']."' WHERE post_id = '" . $post->ID . "' AND meta_key = '".$temp_wp_field_txt."'");
      			$i++;
      		}
      		//set new video sequence
      		$new_video_sequence = array();
      		$isq = 1;
      		foreach($temp_vid_sequence as $vid_seq){
      			$new_video_sequence[$isq] = $vid_seq;
      			$isq ++;
      		}
      		$temp_vid_sequence = $new_video_sequence;
      		update_post_meta($post->ID, "atdw_bmm_vid_sequence", json_encode($temp_vid_sequence));
          
      		//Media > Add New Video
      		if ($_POST["input_atdw_newvideo"]){
      			//check how many videos have been uploaded for this entry
      			$temp1 = $wpdb->get_var("SELECT count(*) FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key LIKE 'atdw_bmm_video-%' AND meta_value != ''");
      			$videos_uploaded = (int)$temp1;
      			//set the fields
      			$temp_wp_field_vid = "atdw_bmm_video-" . ($videos_uploaded + 1);
      			$temp_wp_field_txt = "atdw_bmm_vid_alt-" . ($videos_uploaded + 1);
      			//set new video sequence
      			$new_vid_seq = $videos_uploaded + 1;
      			$temp_vid_sequence[$new_vid_seq] = ($new_vid_seq * 100);
      			//update post meta
      			update_post_meta($post->ID, $temp_wp_field_vid, $_POST["input_atdw_newvideo"]);
      			update_post_meta($post->ID, $temp_wp_field_txt, $_POST['input_atdw_newvideo_text']);
      			update_post_meta($post->ID, "atdw_bmm_vid_sequence", json_encode($temp_vid_sequence));
      		}
      	}
      }

	}