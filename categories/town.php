<?php

	/**
	 * Description for this post type, eg: A PostTypeTemplate class that provides 3 additional meta fields
	 */
	class ATDW_Town
	{
		const POST_TYPE	= "towns";
		const SLUG	= "towns";
		private $CPT_LABEL, $CPT_CAT_LABEL;
		private $_meta	= array(
			'town_lat',
			'town_lng',
			'town_state',
			//'town_showonmainpage',
			//'town_showonregionpage',
			'town_showonfrontend',
			'town_maintown'
		);
		
    	/**
    	 * The Constructor
    	 */
    	public function __construct()
    	{
				// setup labels, according to the fields in "Settings" page
				$this->CPT_LABEL = get_option('atdw_label_towns') ? ucfirst(get_option('atdw_label_towns')) : "Towns";
				//$this->CPT_CAT_LABEL = get_option('atdw_label_regions') ? ucfirst(get_option('atdw_label_regions')) : "Regions";
				
				// register actions
				add_action('init', array(&$this, 'init'));
				add_action('admin_init', array(&$this, 'admin_init'));
				
				// setup menus
				//add_action('admin_menu', array(&$this, 'add_menu'), 7);
    	} // END public function __construct()

    	/**
    	 * hook into WP's init action hook
    	 */
    	public function init()
    	{
				// Permalinks
				add_filter( 'rewrite_rules_array', array(&$this, 'add_rewrite_rules') );
				//add_filter( 'post_type_link', array(&$this, 'filter_post_type_link'), 10, 2 );
				
				// Initialize Post Type
				$this->create_post_type();
				//$this->create_taxonomies();
				
				add_action('save_post', array(&$this, 'save_post'));
        
        // Add new category data fields
        //add_action( 'towns_cat_add_form_fields', array(&$this, 'add_new_custom_fields'));

        // Edit category data fields
        //add_action( 'towns_cat_edit_form_fields', array(&$this, 'add_edit_custom_fields'));

        // Save the category data
        //add_action( 'edited_towns_cat', array(&$this, 'save_towns_cat_meta') );
        //add_action( 'create_towns_cat', array(&$this, 'save_towns_cat_meta') );
        
    	} // END public function init()

    	/**
    	 * Create the post type
    	 */
    	public function create_post_type()
    	{
    		register_post_type(self::POST_TYPE,
    			array(
    				'labels' => array(
							'name' => _x($this->CPT_LABEL, 'post type general name'),
							'singular_name' => _x($this->CPT_LABEL, 'post type singular name'),
							'add_new' => _x('Add New', $this->CPT_LABEL),
							'add_new_item' => __('Add New '.$this->CPT_LABEL),
							'edit_item' => __('Edit '.$this->CPT_LABEL),
							'new_item' => __('New '.$this->CPT_LABEL),
							'view_item' => __('View '.$this->CPT_LABEL),
							'search_items' => __('Search '.$this->CPT_LABEL.'s'),
							'not_found' =>  __('Nothing found'),
							'not_found_in_trash' => __('Nothing found in Trash'),
							'parent_item_colon' => ''
    				),
						'public' => true,
						'publicly_queryable' => true,
						'exclude_from_search' => false,
						'show_ui' => true,
						'show_in_menu' => 'atdw',
						'query_var' => true,
						//'menu_icon' => 'dashicons-location-alt',
						//'rewrite' => array( 'slug' => self::SLUG.'/%atdw_cpt_category%', 'with_front' => false ),
						'rewrite' => array( 'slug' => self::SLUG, 'with_front' => false ),
						'capability_type' => 'post',
						'has_archive' => true,
						'hierarchical' => false,
						'menu_position' => null,
						'supports' => array('title','editor','thumbnail')
    			)
    		);
    	}
			
      /**
       * Create taxonomy, hierarchical
       */
      public function create_taxonomies() {
       	$labels = array(
       		'name'                          => $this->CPT_CAT_LABEL,
       		'singular_name'                 => $this->CPT_CAT_LABEL,
       		'search_items'                  => 'Search '.$this->CPT_CAT_LABEL,
       		'popular_items'                 => 'Popular '.$this->CPT_CAT_LABEL,
       		'all_items'                     => 'All '.$this->CPT_CAT_LABEL,
       		'parent_item'                   => 'Parent '.$this->CPT_CAT_LABEL,
       		'edit_item'                     => 'Edit '.$this->CPT_CAT_LABEL,
       		'view_item'                     => 'View '.$this->CPT_CAT_LABEL,
       		'update_item'                   => 'Update '.$this->CPT_CAT_LABEL,
       		'add_new_item'                  => 'Add New '.$this->CPT_CAT_LABEL,
       		'new_item_name'                 => 'New '.$this->CPT_CAT_LABEL,
       		'separate_items_with_commas'    => 'Separate '.$this->CPT_CAT_LABEL.' with commas',
       		'add_or_remove_items'           => 'Add or remove '.$this->CPT_CAT_LABEL,
       		'choose_from_most_used'         => 'Choose from most used '.$this->CPT_CAT_LABEL
       	);
       		
       	$args = array(
       		'label'                         => $this->CPT_LABEL,
       		'labels'                        => $labels,
       		'public'                        => true,
       		'hierarchical'                  => true,
       		'show_ui'                       => true,
       		'show_in_nav_menus'             => true,
       		'args'                          => array( 'orderby' => 'term_order' ),
       		'rewrite'                       => array( 'slug' => self::SLUG, 'with_front' => false ),
       		'query_var'                     => true
       	);
       	
       	register_taxonomy( sprintf('%s_cat', self::POST_TYPE), self::POST_TYPE, $args);

			} // End of create_taxonomies() function.
			
			/**
				* add menus
				*/		
			public function add_menu()
			{
				 add_submenu_page(
					 'atdw',
					 $this->CPT_CAT_LABEL,
					 $this->CPT_CAT_LABEL,
					 'manage_categories',
					 'edit-tags.php?taxonomy='.sprintf('%s_cat', self::POST_TYPE).'&post_type='.self::POST_TYPE
				 );
				 // Load the additional JS conditionally
				 // Temporary fix on the error of admin menu not higlighting this section when user is on this page. Need a proper fix.
				 $temp_get_taxonomy = (isset($_GET['taxonomy'])) ? $_GET['taxonomy'] : null;
				 if($temp_get_taxonomy == sprintf('%s_cat', self::POST_TYPE)){ 
					 wp_enqueue_script( 'atdw-tags-script', ATDW_PLUGIN_URL . '/assets/js/tags.js', array(), filemtime(ATDW_PLUGIN_DIR . '/assets/js/tags.js'));
					 
					 // fixing the issue of submenu not highlighted
					 add_filter('parent_file', array(&$this, 'select_submenu'));
				 }
			} // END public function add_menu()
			
			/**
				* fixing the issue of submenu not highlighted
				*/		
			public function select_submenu($file)
			{
				 global $parent_file;
				 global $submenu_file;
				 $parent_file = 'admin.php?page=atdw';
				 $submenu_file = 'edit-tags.php?taxonomy='.sprintf('%s_cat', self::POST_TYPE).'&post_type='.self::POST_TYPE;
			} // END public function select_submenu()
			
			/**
			* Add rewrite rules
			*/
			public function add_rewrite_rules( $rules ) {
			 $new = array();

			 //$temp_rule_1 = self::SLUG.'/([^/]+)/(.+)/?$';
			 //$temp_rule_2 = self::SLUG.'/(.+)/?$';
			 $temp_rule_3 = self::SLUG.'/?$';

			 //$new[$temp_rule_1] = 'index.php?'.self::POST_TYPE.'=$matches[2]';
			 //$new[$temp_rule_2] = 'index.php?'.sprintf('%s_cat', self::POST_TYPE).'=$matches[1]';
			 $new[$temp_rule_3] = 'index.php?post_type=' . self::POST_TYPE;

			 return array_merge( $new, $rules ); // Ensure our rules come first
			} // End of add_rewrite_rules( $rules ) function.
			
			/**
			* Handle the '%atdw_cpt_category%' URL placeholder
			*/
			function filter_post_type_link( $link, $post ) {
			 if ( $post->post_type == self::POST_TYPE ) {
				 if ( $cats = get_the_terms( $post->ID, sprintf('%s_cat', self::POST_TYPE) ) ) {
					 $link = str_replace( '%atdw_cpt_category%', current( $cats )->slug, $link );
				 }else{
					 //if this entry doesn't have any category assigned to it
					 $link = str_replace( '%atdw_cpt_category%', 'others', $link );
				 }
			 }
			 return $link;
			} // End of filter_post_type_link( $link, $post ) function.
	
    	/**
    	 * Save the metaboxes for this custom post type
    	 */
    	public function save_post($post_id)
    	{
        // verify if this is an auto save routine. 
        // If it is our form has not been submitted, so we dont want to do anything
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        {
          return;
        }
            
    		if(isset($_POST['post_type']) && $_POST['post_type'] == self::POST_TYPE && current_user_can('edit_post', $post_id))
    		{
    			foreach($this->_meta as $field_name)
    			{
    				// Update the post's meta field
    				update_post_meta($post_id, $field_name, $_POST[$field_name]);
    			}
    		}
    		else
    		{
    			return;
    		} // if($_POST['post_type'] == self::POST_TYPE && current_user_can('edit_post', $post_id))
    	} // END public function save_post($post_id)

    	/**
    	 * hook into WP's admin_init action hook
    	 */
    	public function admin_init()
    	{			
    		// Add metaboxes
    		add_action('add_meta_boxes', array(&$this, 'add_meta_boxes'));
				
    		// add custom columns on admin page
    		add_filter('manage_towns_posts_columns', array(&$this, 'towns_columns_head'));
    		add_filter('manage_edit-towns_sortable_columns', array(&$this, 'sortable_towns_columns'));
    		add_action('manage_towns_posts_custom_column', array(&$this, 'towns_columns_content'), 10, 2);
    	} // END public function admin_init()
			
    	/**
    	 * hook into WP's add_meta_boxes action hook
    	 */
    	public function add_meta_boxes()
    	{
    		// Add the state selection metabox
    		add_meta_box( 
    			sprintf('osky_atdw_%s_state_section', self::POST_TYPE),
    			"State",
    			array(&$this, 'add_state_meta_boxes'),
    			self::POST_TYPE,
    			'side'
    		);
				
    		// Add the location/map metabox
    		add_meta_box( 
    			sprintf('osky_atdw_%s_section', self::POST_TYPE),
    			$this->CPT_LABEL." Location",
    			array(&$this, 'add_inner_meta_boxes'),
    			self::POST_TYPE,
    			'normal'
    		);

            // Add the show/hide metabox
            add_meta_box( 
                sprintf('osky_atdw_%s_showonmap_section', self::POST_TYPE),
                $this->CPT_LABEL." Extra Options",
                array(&$this, 'add_showonmap_meta_boxes'),
                self::POST_TYPE,
                'side'
            );            
    	} // END public function add_meta_boxes()

		/**
		 * called off of the add meta box
		 */
		public function add_inner_meta_boxes($post)
		{		
			// Render the job order metabox
			include(sprintf("%s/../templates/town_metabox.php", dirname(__FILE__)));			
		} // END public function add_inner_meta_boxes($post)
		
		public function add_state_meta_boxes($post)
		{		
			// Render the job order metabox
			include(sprintf("%s/../templates/town_state_metabox.php", dirname(__FILE__)));			
		} // END public function add_inner_meta_boxes($post)
		
        public function add_showonmap_meta_boxes($post)
        {       
            // Render the job order metabox            
            include(sprintf("%s/../templates/town_showonmap_metabox.php", dirname(__FILE__)));         
        } // END public function add_inner_meta_boxes($post)
		
		/**
		 * add custom column at admin page
		 */
		public function towns_columns_head( $columns ) {
			$new_columns['cb'] = '<input type="checkbox" />';
			$new_columns['title'] = 'Title';
			//$new_columns['region'] = $this->CPT_CAT_LABEL;
			$new_columns['state'] = 'State';
			//$new_columns['show_on_main_page'] = 'Show on Main';
			//$new_columns['show_on_region_page'] = 'Show on Region';
			$new_columns['show_on_front_end'] = 'Show on front end';
			$new_columns['main_town'] = 'Main Town';
			$new_columns['date'] = 'Date';
			return $new_columns;
		}
		
		public function sortable_towns_columns( $columns ) {
			//$columns['region'] = $this->CPT_CAT_LABEL;
			$columns['state'] = 'State';
			//$columns['show_on_main_page'] = 'Show on Main';
			//$columns['show_on_region_page'] = 'Show on Region';
			$columns['show_on_front_end'] = 'Show on front end';
			$columns['main_town'] = 'Main Town';
		 
			//To make a column 'un-sortable' remove it from the array
			//unset($columns['date']);
		 
			return $columns;
		}
		
		public function towns_columns_content( $column_name, $post_ID ) {
			switch ($column_name) {
    		case 'region':
    			$term_list = wp_get_post_terms($post_ID, 'towns_cat', array("fields" => "names"));
    			echo implode(', ', $term_list);
    			break;

    		case 'state':
    			$town_state = get_post_meta($post_ID, 'town_state', true);
    			switch($town_state){
						case 'ACT': echo 'Australian Capital Territory'; break;
						case 'NSW': echo 'New South Wales'; break;
						case 'NT': echo 'Northern Territory'; break;
						case 'TAS': echo 'Tasmania'; break;
						case 'SA': echo 'South Australia'; break;
						case 'QLD': echo 'Queensland'; break;
						case 'WA': echo 'Western Australia'; break;
						case 'VIC': echo 'Victoria'; break;
    			}
    			break;

            case 'show_on_main_page':
                $town_showonmainpage = get_post_meta($post_ID, 'town_showonmainpage', true);
                switch ($town_showonmainpage) {
                    case '1':
                        echo 'Yes';
                        break;
                    
                    default:
                        echo 'No';
                        break;
                }
                break;

            case 'show_on_region_page':
                $town_showonregionpage = get_post_meta($post_ID, 'town_showonregionpage', true);
                switch ($town_showonregionpage) {
                    case '1':
                        echo 'Yes';
                        break;
                    
                    default:
                        echo 'No';
                        break;
                }
                break;
            
            case 'show_on_front_end':
                $town_showonfrontend = get_post_meta($post_ID, 'town_showonfrontend', true);
                switch ($town_showonfrontend) {
                    case '1':
                        echo 'Yes';
                        break;
                        
                    default:
                        echo 'No';
                        break;
                }
                break;    

            case 'main_town':
                $town_maintown = get_post_meta($post_ID, 'town_maintown', true);
                switch ($town_maintown) {
                    case 'yes':
                        echo 'Yes';
                        break;
                    
                    default:
                        echo 'No';
                        break;
                }
                break;
			}
		}
    
    /**
     * Add new custom fields
     */
    public function add_new_custom_fields( )
    {
        echo '
        ';
    }
    
    /**
     * Add the edit custom fields
     */
    public function add_edit_custom_fields( $term )
    {
        $termMeta = get_option( 'towns_cat_meta_' . $term->term_id );
        echo '
        <tr>
          <td colspan="2"><hr></td>
        </tr>
        
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[main_town]">Main Town</label></th>
            <td>
                <input type="text" name="term_meta[main_town]" id="term_meta[main_town]" value="'.( (!empty($termMeta['main_town'])) ?  esc_attr($termMeta['main_town']) : null ).'">
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[second_lvl_detail]">Second-level Detail</label></th>
            <td>
                <input type="text" name="term_meta[second_lvl_detail]" id="term_meta[second_lvl_detail]" value="'.( (!empty($termMeta['second_lvl_detail'])) ?  esc_attr($termMeta['second_lvl_detail']) : null ).'">
                <p class="description">Use commas to separate items.</p>
            </td>
        </tr>
        
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[description_short]">Description (Homepage map)</label></th>
            <td>
                <input type="text" name="term_meta[description_short]" id="term_meta[description_short]" value="'.( (!empty($termMeta['description_short'])) ?  esc_attr($termMeta['description_short']) : null ).'">
                <p class="description">This description will be used in the map slide out boxes at home page.</p>
            </td>
        </tr>
        
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[learn_more]">"Learn More" link</label></th>
            <td>
                <input type="text" name="term_meta[learn_more]" id="term_meta[learn_more]" value="'.( (!empty($termMeta['learn_more'])) ?  esc_attr($termMeta['learn_more']) : null ).'">
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[visitor_guide]">Visitor Guide</label></th>
            <td>
                <input type="text" name="term_meta[visitor_guide]" id="term_meta[visitor_guide]" value="'.( (!empty($termMeta['visitor_guide'])) ?  esc_attr($termMeta['visitor_guide']) : null ).'">
            </td>
        </tr>
        
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[facebook]">Facebook</label></th>
            <td>
                <input type="text" name="term_meta[facebook]" id="term_meta[facebook]" value="'.( (!empty($termMeta['facebook'])) ?  esc_attr($termMeta['facebook']) : null ).'">
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[twitter]">Twitter</label></th>
            <td>
                <input type="text" name="term_meta[twitter]" id="term_meta[twitter]" value="'.( (!empty($termMeta['twitter'])) ?  esc_attr($termMeta['twitter']) : null ).'">
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[instagram]">Instagram</label></th>
            <td>
                <input type="text" name="term_meta[instagram]" id="term_meta[instagram]" value="'.( (!empty($termMeta['instagram'])) ?  esc_attr($termMeta['instagram']) : null ).'">
            </td>
        </tr>
        ';
    }
    
    /**
     * Save the taxonomy custom meta
     */
    public function save_towns_cat_meta($termId)
    {
        if ( !empty( $_POST['term_meta'] ) )
        {
            $term_meta = get_option( 'towns_cat_meta_' . $termId );

            foreach ( $_POST['term_meta'] as $key => $val )
            {
                $term_meta[$key] = sanitize_text_field($val);
            }

            update_option( 'towns_cat_meta_' . $termId, $term_meta );
        }
    }

	}
