<?php

class ATDW_Functions
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
        	// register actions
        	add_action('admin_init', array(&$this, 'admin_init'));					
        	// setup menus
					add_action('admin_menu', array(&$this, 'add_menu'), 6);
					// cron related functions
					require_once(ATDW_PLUGIN_DIR."/class/sync.php");					
        } 
        
        /**
         * hook into WP's admin_init action hook
         */
        public function admin_init()
        {
        	// register plugin's settings
        	register_setting('atdw-group-api', 'atdw_setting_host');
        	register_setting('atdw-group-api', 'atdw_setting_key');
        	register_setting('atdw-group-api', 'atdw_tripadvisor_key');
        	register_setting('atdw-group-api', 'atdw_gmap_key');
        	register_setting('atdw-group-sync', 'atdw_sync_method');
        	register_setting('atdw-group-sync', 'atdw_cron_method');
        	register_setting('atdw-group-sync', 'atdw_cron_time_start');
        	register_setting('atdw-group-sync', 'atdw_cron_time_end');
        	register_setting('atdw-group-sync', 'atdw_cron_frequency');
        	register_setting('atdw-group-customsync', 'atdw_customsync_town');
        	register_setting('atdw-group-customsync', 'atdw_customsync_town_select');
        	register_setting('atdw-group-customsync', 'atdw_customsync_town_select_tmp');
        	register_setting('atdw-group-labels', 'atdw_label_towns');
        	register_setting('atdw-group-labels', 'atdw_label_regions');
        	register_setting('atdw-group-labels', 'atdw_label_accomm');
        	register_setting('atdw-group-labels', 'atdw_label_attraction');
        	register_setting('atdw-group-labels', 'atdw_label_event');
        	register_setting('atdw-group-labels', 'atdw_label_restaurant');
        	register_setting('atdw-group-labels', 'atdw_label_tour');
					
        	// Load the additional JS conditionally
        	$current_page = (isset($_GET['page'])) ? $_GET['page'] : null;
        	if($current_page == 'atdw'){ 
        		wp_enqueue_script( 'atdw-settings-script', ATDW_PLUGIN_URL . '/assets/js/settings.js', array(), filemtime(ATDW_PLUGIN_DIR . '/assets/js/settings.js'));
        	}    
        } 

        public function add_menu()
        {
        	// Adding ATDW related pages at the main menu
        	add_menu_page( 
        		'ATDW', 
        		'ATDW', 
        		'manage_options', 
        		'atdw', 
        		array(&$this, 'plugin_settings_page'),
        		'dashicons-tickets-alt',
        		'26'
        	);
        	add_submenu_page(
        		'atdw',
        		'Settings',
        		'Settings',
        		'manage_options',
        		'atdw'
        	);
        }

        /**
         * Menu Callback
         */		
        public function plugin_settings_page()
        {
        	if(!current_user_can('manage_options'))
        	{
        		wp_die(__('You do not have sufficient permissions to access this page.'));
        	}
		
        	// Render the settings template
        	include(ATDW_PLUGIN_DIR."/templates/admin-settings.php");
					
        	// Manual Sync
        	if(get_option('atdw_sync_method') == 'manual'){
	        	$action = (isset($_GET['action'])) ? $_GET['action'] : null;
	        	$nonce = (isset($_GET['_wpnonce'])) ? $_GET['_wpnonce'] : null;
	        	switch($action){
	        		case 'manual_sync_atdw':
		        		$verify_nonce = wp_verify_nonce( $nonce, 'atdw_manual_sync_atdw' );
		        		if($verify_nonce == 1){
		        			echo 'Starting...<br><br>';
		        			$this->sync( 'atdw' );
		        		}else{
		        			echo 'Invalid request.';
		        		}
		        		break;
	        		case 'manual_sync_ta':
		        		$verify_nonce = wp_verify_nonce( $nonce, 'atdw_manual_sync_ta' );
		        		if($verify_nonce == 1){
			        		echo 'Starting...<br><br>';
			        		$this->sync( 'ta' );
		        		}else{
			        		echo 'Invalid request.';
		        		}
		        		break;
              case 'get_sync_preview':
                $verify_nonce = wp_verify_nonce( $nonce, 'atdw_get_sync_preview' );
                if($verify_nonce == 1){
                  echo 'Starting...<br><br>';
                  $this->sync_preview( 'atdw' );
                }else{
                  echo 'Invalid request.';
                }
                break;  
	        	}
        	}
					
        	// Auto Sync Functions
					if(get_option('atdw_sync_method') == 'auto') 
						{
							if(get_option('atdw_cron_method') == 'wp')
								{
									// wil be updated soon...
								}
						}	
				}
						
					/**
					 * Sync
					 */		
					public function sync( $action ){
						$ATDW_Sync = new ATDW_Sync();
						switch($action){
							case 'atdw': $ATDW_Sync->sync(); break;
							case 'ta': $ATDW_Sync->sync_tripadvisor(); break;
						}
					}
					
					/**
					 * Sync Preview
					 */		
					public function sync_preview( $action ){
						$ATDW_Sync = new ATDW_Sync();
						switch($action){
							case 'atdw': $ATDW_Sync->sync_preview(); break;
						}
					} 				
        }     
  
