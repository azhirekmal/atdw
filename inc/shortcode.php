<?php

// shortcode function
function atdw_shortcode( $atts ) {
    global $wpdb;
    // extract the variables, if there is no ID attr, the default is 1
    extract( shortcode_atts( array(
        'id' => 1
    ), $atts ) );
    // set page id to either the id attr which defaults to one.
    $page_id = $id;
    $title = get_the_title( $page_id );
    $url_title = get_permalink( $page_id );
    
    $fb_row = $wpdb->get_row( "SELECT * FROM wp_postmeta WHERE post_id = '" . $page_id . "' AND meta_key like 'external_system_text-1'" );
    $tw_row = $wpdb->get_row( "SELECT * FROM wp_postmeta WHERE post_id = '" . $page_id . "' AND meta_key like 'external_system_text-2'" );

    $html.= '
    <div>
        <h1 class="entry-title">
            <a href="'.$url_title.'">'.$title.'</a>
        </h1>
        <p>
            City: '.get_post_field( 'atdw_pr_city', $page_id ).' | State: '.get_post_field( 'atdw_pr_state', $page_id ).'
        </p>
        <p>
            Description: '.get_post_field( 'atdw_pr_product_short_description', $page_id ).'... [<a href="'.$url_title.'">Read more</a>]
        </p>
        <p>
            Phone Number: '.get_post_field( 'atdw_pr_comm_phone', $page_id ).' | Mobile Number: '.get_post_field( 'atdw_pr_comm_mobile', $page_id ).'
        </p> 
        <p>
            Website: <a href="'.get_post_field( 'atdw_pr_comm_website', $page_id ).'" target="blank">'.get_post_field( 'atdw_pr_comm_website', $page_id ).'</a> <br>
            Email: <a href="mailto:'.get_post_field( 'atdw_pr_comm_email', $page_id ).'">'.get_post_field( 'atdw_pr_comm_email', $page_id ).'</a> 
        </p>
        <p>
            Facebook: <a href="'.$fb_row->meta_value.'" target="blank">'.$fb_row->meta_value.'</a> <br>
            Twitter: <a href="'.$tw_row->meta_value.'" target="blank">'.$tw_row->meta_value.'</a> 
        </p> 
  
        <iframe width="100%" height="350" id="gmap_canvas" src="https://maps.google.com/maps?q='.get_post_field( 'atdw_pr_pysical_lat', $page_id ).','.get_post_field( 'atdw_pr_pysical_long', $page_id ).'&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>    
    </div>
    ';

    foreach( $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = '" . $page_id . "' AND meta_key like '%atdw_bmm_image%'" ) as $key => $row ) {
        $img_row = $row->meta_value;
        $html.= '
        <div style="float: left; width: 33%;">
        <p style="line-height: 14px; margin: 10px 0px 0px 0px; font-style: italic;">    
            <a class="thickbox_h" style="outline:0px;" href="'.$img_row.'">
                <img class="imageView" style="padding: 3px; border: 1px solid #BBB; background-color: white;" src="'.$img_row.'">
            </a>  
        </p> 
        </div>
        ';
    }

    return $html;
}

function ink_wp_shortcode($atts, $content=null)
{
    $post_url = get_permalink($post->ID);
    $post_title = get_the_title($post->ID);
    $tweet = '<a style="color:blue; font-size: 20px;" href="http://twitter.com/home/?status=Read more: ' . $post_title . ' at ' . $post_url . '"><b> Share on Twitter </b></a>';
    return $tweet;
}

add_shortcode( 'atdw-shortcode', 'atdw_shortcode' );
add_shortcode( 'atdw-ink', 'form_creation' );
