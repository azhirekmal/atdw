<?php

	/**
	 * For ATDW global tags
	 */
	class ATDW_Tags
	{
			/**
    	 * The Constructor
    	 */
    	public function __construct()
    	{
    		// register actions
    		add_action('init', array(&$this, 'init'));
				
    		// setup menus
    		add_action('admin_menu', array(&$this, 'add_menu'), 11);
    	} // END public function __construct()

    	/**
    	 * hook into WP's init action hook
    	 */
    	public function init()
    	{
    		// Initialize Taxonomy
    		$this->create_taxonomies();
    		
    	} // END public function init()

    	/**
    	 * Create taxonomy, non-hierarchical
    	 */
      public function create_taxonomies() {
       	$labels = array(
       		'name'                          => 'ATDW Tags',
       		'singular_name'                 => 'Tag',
       		'search_items'                  => 'Search Tags',
       		'popular_items'                 => 'Popular Tags',
       		'all_items'                     => 'All Tags',
       		'parent_item'                   => 'Parent Tag',
       		'edit_item'                     => 'Edit Tag',
       		'update_item'                   => 'Update Tag',
       		'add_new_item'                  => 'Add New Tag',
       		'new_item_name'                 => 'New tag',
       		'separate_items_with_commas'    => 'Separate tags with commas',
       		'add_or_remove_items'           => 'Add or remove tags',
       		'choose_from_most_used'         => 'Choose from most used tags'
       	);
       		
       	$args = array(
       		'label'                         => 'ADTW Tags',
					'description'                   => 'Global Tags applied in ATDW across all product types in the website',
       		'labels'                        => $labels,
       		'public'                        => true,
       		'hierarchical'                  => false,
       		'show_ui'                       => true,
       		'show_in_menu'                  => 'admin.php?page=atdw',
       		'args'                          => array( 'orderby' => 'term_order' ),
       		'rewrite'                       => array( 'slug' => 'global-tags', 'with_front' => false ),
       		'query_var'                     => true
       	);
       	
       	register_taxonomy( 'atdw_tags', array('atdw_accomm', 'atdw_attraction', 'atdw_event', 'atdw_tour', 'atdw_restaurant', 'atdw_genservice'), $args);

       } // End of create_taxonomies() function.
       
       /**
     	 * hook into WP's admin_init action hook
     	 */
     	public function admin_init()
     	{		
				
     	} // END public function admin_init()
			
     	/**
     	 * add menus
     	 */		
     	public function add_menu()
     	{
	      $atdw_tags_page = add_submenu_page(
	          'atdw',
	          'ATDW Tags',
	          'ATDW Tags',
	          'manage_options',
	          'edit-tags.php?taxonomy=atdw_tags'
	      );
	      // Load the additional JS conditionally
	      // Temporary fix on the error of admin menu not higlighting this section when user is on this page. Need a proper fix.
	      $temp_get_taxonomy = (isset($_GET['taxonomy'])) ? $_GET['taxonomy'] : null;
	      if($temp_get_taxonomy == 'atdw_tags'){ 
	        wp_enqueue_script( 'atdw-tags-script', ATDW_PLUGIN_URL . '/assets/js/tags.js', array(), filemtime(ATDW_PLUGIN_DIR . '/assets/js/tags.js'));
					
	        // fixing the issue of submenu not highlighted
	        add_filter('parent_file', array(&$this, 'select_submenu'));
	      }
	    } // END public function add_menu()
			
	    /**
	     * fixing the issue of submenu not highlighted
	     */		
	    public function select_submenu($file)
	    {
	      global $parent_file;
	      global $submenu_file;
	      $parent_file = 'admin.php?page=atdw';
	      $submenu_file = 'edit-tags.php?taxonomy=atdw_tags';
			} // END public function select_submenu()
						
		}