<?php

/**
 * @package ATDW
 */
/*
Plugin Name: ATDW Sync
Plugin URI: https://www.oskyinteractive.com.au
Description: ATDW documentation at <a href="http://developer.atdw.com.au/ATLAS/API/ATDWO-getproduct.html">http://developer.atdw.com.au/ATLAS/API/ATDWO-getproduct.html</a>
Version: 0.0.1
Author: OSKY Interactive
Author URI: https://www.oskyinteractive.com.au
License: GPL2 or later
Text Domain: atdw
*/

class ATDW
	{
			public function __construct()
			{
				// Global paths
				define('ATDW_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());
				define('ATDW_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));
				define('ATDW_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . ATDW_PLUGIN_NAME);
				define('ATDW_PLUGIN_URL', WP_PLUGIN_URL . '/' . ATDW_PLUGIN_NAME);	
				
				// Initialize Settings
				require_once(ATDW_PLUGIN_DIR."/inc/atdw-functions.php");
				$ATDW_Functions = new ATDW_Functions();
				$plugin = plugin_basename(__FILE__);
				add_filter("plugin_action_links_{$plugin}", array( $this, 'plugin_settings_link' ));	
				
				// Tags
				require_once(ATDW_PLUGIN_DIR."/inc/tags.php");
				$ATDW_Tags = new ATDW_Tags();				

				// shared functions
				require_once(ATDW_PLUGIN_DIR."/class/tool.php");
				
				// Register custom post types
				require_once(ATDW_PLUGIN_DIR."/categories/town.php");
				$ATDW_Town = new ATDW_Town();
				require_once(ATDW_PLUGIN_DIR."/categories/accomm.php");
				$ATDW_Accomm = new ATDW_Accomm();
				require_once(ATDW_PLUGIN_DIR."/categories/attraction.php");
				$ATDW_Attraction = new ATDW_Attraction();
				require_once(ATDW_PLUGIN_DIR."/categories/event.php");
				$ATDW_Event = new ATDW_Event();
				require_once(ATDW_PLUGIN_DIR."/categories/tour.php");
				$ATDW_Tour = new ATDW_Tour();
				require_once(ATDW_PLUGIN_DIR."/categories/genservice.php");
				$ATDW_Genservice = new ATDW_Genservice();
				require_once(ATDW_PLUGIN_DIR."/categories/restaurant.php");
				$ATDW_Restaurant = new ATDW_Restaurant();				
			}

			public static function activate()
			{
				// Do nothing
			}

			public static function deactivate()
			{
				// Do nothing
			}

			// Add the settings link to the plugins page
			function plugin_settings_link($links)
			{
				$settings_link = '<a href="admin.php?page=atdw">Settings</a>';
				array_unshift($links, $settings_link);
				return $links;
			}					
	}

// Installation and uninstallation hooks
register_activation_hook(__FILE__, array('ATDW', 'activate'));
register_deactivation_hook(__FILE__, array('ATDW', 'deactivate'));

// instantiate the plugin class
$osky_atdw = new ATDW();

require_once(ATDW_PLUGIN_DIR . '/templates/page-template.php');
require_once(ATDW_PLUGIN_DIR . '/inc/shortcode.php');