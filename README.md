# ATDW WP Plugin
A WordPress plugin that queries ATDW API and sync into the WordPress powered website.

## Notes
* Current version 0.0.1, plugin still need an improvement on back-end side.

## Reference for ATDW API
* Link to apply for Distribution key: https://oauth.atdw-online.com.au/listing-subscription?
* Documentation: http://developer.atdw.com.au/ATLAS/API/ATDWO-api.html 
* Search (user-front): http://atdw.com.au/our-listings/

## Reference for TripAdvisor API
* Link to apply for API access: https://www.tripadvisor.com/APIAccessSupport
* Guidelines: https://developer-tripadvisor.com/content-api/display-requirements/, https://developer-tripadvisor.com/content-api/terms-and-conditions/ 