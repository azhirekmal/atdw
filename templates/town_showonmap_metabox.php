<?php if ( !defined( 'ABSPATH' ) ) die( 'No direct access allowed' ); ?>
<?php 
$town_showonmainpage = get_post_meta($post->ID, 'town_showonmainpage', true);
$town_showonregionpage = get_post_meta($post->ID, 'town_showonregionpage', true);
$town_maintown = get_post_meta($post->ID, 'town_maintown', true);
$town_showonfrontend = get_post_meta($post->ID, 'town_showonfrontend', true);
?>
<!-- <input type="checkbox" id="town_showonmainpage" name="town_showonmainpage" value="1" <?php echo (($town_showonmainpage == '1') ? 'checked' : ''); ?>>
<label for="town_showonmainpage">Show Town on <b>Main</b> Destination Page</label>
<br>
<input type="checkbox" id="town_showonregionpage" name="town_showonregionpage" value="1" <?php echo (($town_showonregionpage == '1') ? 'checked' : ''); ?>>
<label for="town_showonregionpage">Show Town on <b>Region</b> Destination Page</label>
<br> -->
<input type="checkbox" id="town_showonfrontend" name="town_showonfrontend" value="1" <?php echo (($town_showonfrontend == '1') ? 'checked' : ''); ?>>
<label for="town_showonfrontend">Show on front end</label>
<br><br>
<input type="checkbox" id="town_maintown" name="town_maintown" value="yes" <?php echo (($town_maintown == 'yes') ? 'checked' : ''); ?>>
<label for="town_maintown">Main Town</label>
