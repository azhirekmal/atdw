<?php
/**
 * Add "Custom" template to page attirbute template section.
 */

add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'atdw_event' || get_post_type() == 'atdw_accomm' || get_post_type() == 'atdw_attraction' || get_post_type() == 'atdw_tour' || get_post_type() == 'atdw_genservice' || get_post_type() == 'atdw_restaurant' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'page-atdw.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = ATDW_PLUGIN_DIR . '/templates/page-atdw.php';
            }
        }
    }

    if ( get_post_type() == 'towns' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'page-towns.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = ATDW_PLUGIN_DIR . '/templates/page-towns.php';
            }
        }
    }

    return $template_path;
}
