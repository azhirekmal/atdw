<?php if ( !defined( 'ABSPATH' ) ) die( 'No direct access allowed' ); ?>

<div class="wrap">
    <h2>The Australian Tourism Data Warehouse (ATDW) - Settings</h2>
    <?php
    $current_tab = isset($_GET['tab']) ? $_GET['tab'] : null;
    ?>
    <h2 class="nav-tab-wrapper">
        <a href="admin.php?page=atdw&tab=summary" class="nav-tab <?php if($current_tab == 'summary') echo 'nav-tab-active'; ?>">Summary</a> 
        <a href="admin.php?page=atdw&tab=atdw" class="nav-tab <?php if($current_tab == 'atdw') echo 'nav-tab-active'; ?>">API Details</a>
        <a href="admin.php?page=atdw&tab=sync" class="nav-tab <?php if($current_tab == 'sync') echo 'nav-tab-active'; ?>">Sync</a>
        <a href="admin.php?page=atdw&tab=customsync" class="nav-tab <?php if($current_tab == 'customsync') echo 'nav-tab-active'; ?>">Custom Sync</a>
        <a href="admin.php?page=atdw&tab=labels" class="nav-tab <?php if($current_tab == 'labels') echo 'nav-tab-active'; ?>">Labels</a>
    </h2>
    <form method="post" action="options.php"> 
      <?php 
      switch($current_tab){
        default:
          settings_fields('atdw-group-sync');
          do_settings_fields('atdw-group-sync', null); 
        ?>
        <div class="welcome-box-left">
          <h1>Welcome to ATDW Wordpress Plugin!</h1>
          <p>The most versatile The Australian Tourism Data Warehouse wordpress plugin </p>

          <h3>Shortcode</h3>
          <p>Place <b>[atdw-shortcode id=post_id]</b> on the page or post you wish to use. Note: ID = ATDW post ID. 
        </div>
        <?php  
        break;

        case 'sync':
          settings_fields('atdw-group-sync');
          do_settings_fields('atdw-group-sync', null); 
        ?>
        <table class="form-table">
          <tr valign="top">
            <th scope="row">
             <label for="atdw_sync_method">Sync Method</label>
            </th>
            <td>
              <div class="form-group">
                <select class="form-control" name="atdw_sync_method" id="atdw_sync_method">
                  <option value="auto" <?php echo ((get_option('atdw_sync_method') == 'auto') ? 'selected = "selected"' : ''); ?>>Auto</option>
                  <option value="manual" <?php echo ((get_option('atdw_sync_method') == 'manual') ? '' : ''); ?>>Manual</option>
                </select>
              </div>
            </td>
          </tr>
          <tr valign="top" id="row-atdw_cron_method" <?php echo ((get_option('atdw_sync_method') == 'manual') ? '' : ''); ?>>
            <th scope="row"><label for="atdw_cron_method">Cron Method</label></th>
            <td>
              <select name="atdw_cron_method" id="atdw_cron_method">
                <option value="server" <?php echo ((get_option('atdw_cron_method') == 'server') ? 'selected = "selected"' : ''); ?>>Server Cron</option>
                <option value="wp" <?php echo ((get_option('atdw_cron_method') == 'wp') ? 'selected = "selected"' : ''); ?>>WP Cron</option>
              </select>
            </td>
          </tr>
          <tr valign="top" class="row-atdw_cron_schedule" <?php echo ((get_option('atdw_sync_method') == 'manual') ? '' : ''); ?>>
            <th scope="row"><label for="atdw_cron_time_start">Cron Start Time</label></th>
            <td>
              <select name="atdw_cron_time_start_hours" id="atdw_cron_time_start_hours">
                <?php 
                for( $i = 0; $i < 24; $i++ ){ 
                  $tmp_hours = str_pad($i, 2, '0', STR_PAD_LEFT);
                ?>
                <option value="<?php echo $i; ?>" <?php echo ((get_option('atdw_cron_time_start_hours') == $i) ? 'selected = "selected"' : ''); ?>><?php echo $tmp_hours; ?></option>
                <?php } ?>
              </select>
              :
              <select name="atdw_cron_time_start_minutes" id="atdw_cron_time_start_minutes">
                <?php 
                for( $i = 0; $i < 60; $i+=5 ){ 
                  $tmp_minutes = str_pad($i, 2, '0', STR_PAD_LEFT);
                ?>
                <option value="<?php echo $i; ?>" <?php echo ((get_option('atdw_cron_time_start_minutes') == $i) ? 'selected = "selected"' : ''); ?>><?php echo $tmp_minutes; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr valign="top" class="row-atdw_cron_schedule" <?php echo ((get_option('atdw_sync_method') == 'manual') ? '' : ''); ?>>
            <th scope="row"><label for="atdw_cron_frequency">Cron Frequency</label></th>
            <td>
              <select name="atdw_cron_frequency" id="atdw_cron_frequency">
                <option value="daily" <?php echo ((get_option('atdw_cron_frequency') == 'daily') ? 'selected = "selected"' : ''); ?>>daily</option>
                <option value="weekly" <?php echo ((get_option('atdw_cron_frequency') == 'weekly') ? 'selected = "selected"' : ''); ?>>weekly</option>
                <option value="monthly" <?php echo ((get_option('atdw_cron_frequency') == 'monthly') ? 'selected = "selected"' : ''); ?>>monthly</option>
              </select>
            </td>
          </tr>
          <tr valign="top" class="row-atdw_cron_settings" <?php 
          if(get_option('atdw_sync_method') == 'auto'){
            echo ( (get_option('atdw_cron_method') == 'server') ? '': '' ); 
          }
          ?>>
            <th scope="row"><label for="atdw_cron_settings">Cron Settings</label></th>
            <td>
              Set up a cron job using the following:<br>
              <p><code>0 0,1,2,3,4,5 * * * GET <?php echo ATDW_PLUGIN_URL; ?>/cron.php?auth=osky > /dev/null</code></p>
            </td>
          </tr>
        </table>
        <em> Note: You need to create "<a href="post-new.php?post_type=towns">Towns</a>" first in order to sync based on towns attribute </em>
        <?php  
          break;
        case 'customsync':
          settings_fields('atdw-group-customsync');
          do_settings_fields('atdw-group-customsync', null); 
          
          // to get the selected town
          $atdw_customsync_town = get_option('atdw_customsync_town');
          
          // to get the list of custom sync entries
          if(!get_option('atdw_customsync_list')){
            add_option( 'atdw_customsync_list', null, '');
            $atdw_customsync_list = array();
          }else{
            $atdw_customsync_list = get_option('atdw_customsync_list'); 
          }
          
          // to get the exclude list of custom sync entries
          if(!get_option('atdw_customsync_list_exclude')){
            add_option( 'atdw_customsync_list_exclude', null, '');
            $atdw_customsync_list_exclude = array();
          }else{
            $atdw_customsync_list_exclude = get_option('atdw_customsync_list_exclude'); 
          }
          
          // to get the selected entries
          $atdw_customsync_town_select = get_option('atdw_customsync_town_select') ? get_option('atdw_customsync_town_select') : array();
          // to get the previous selected entries
          $atdw_customsync_town_select_prev = get_option('atdw_customsync_town_select_tmp') ? get_option('atdw_customsync_town_select_tmp') : array();
          
          if(isset($atdw_customsync_town_select)){
            foreach ($atdw_customsync_town_select as $key => $value) {
              // to add this selected item into custom sync list
              if( !in_array($value, $atdw_customsync_list, false) ){
                array_push($atdw_customsync_list, $value);
              }
              
              // remove this selected item from "exclude" sync list
              if (($atdw_customsync_list_exclude_key = array_search($value, $atdw_customsync_list_exclude)) !== false) {
                unset($atdw_customsync_list_exclude[$atdw_customsync_list_exclude_key]);
              }
            }
            
            // find if there are any item removed
            $check_changes = array_diff($atdw_customsync_town_select_prev, $atdw_customsync_town_select);
            foreach ($check_changes as $key => $value) {
              // remove this removed item from custom sync list
              if (($atdw_customsync_list_key = array_search($value, $atdw_customsync_list)) !== false) {
                unset($atdw_customsync_list[$atdw_customsync_list_key]);
              }
              
              // add this into a "exclude" sync list
              if( !in_array($value, $atdw_customsync_list_exclude, false) ){
                array_push($atdw_customsync_list_exclude, $value);
              }
            }
            
            update_option('atdw_customsync_list', $atdw_customsync_list);
            update_option('atdw_customsync_list_exclude', $atdw_customsync_list_exclude);
            
            // reset
            update_option( 'atdw_customsync_town_select_tmp', null );
          }
        ?>
        <?php if(!$atdw_customsync_town): ?>
        <table class="form-table">
          <tr valign="top">
            <th scope="row" style="width:120px;">
              <label for="">Select a town:</label>
            </th>
            <td><select name="atdw_customsync_town" id="atdw_customsync_town">
              <option value="">Please select</option>
              <?php 
              // retriving towns list
              $args = array(
                      "post_type" => "towns",
                      "post_status" => "publish",
                      "posts_per_page" => -1,
                      "order" => "ASC",
                      "orderby" => "title"
                      );
              $the_query = new WP_Query( $args );
              foreach ($the_query->posts as $city_post):
                  $city_show_on_frontend = (get_post_meta($city_post->ID, 'town_showonfrontend', true) == 1) ? '(Front and Backend)' : '(Backend only)';
              ?>
              <option value="<?php echo $city_post->ID; ?>"><?php echo $city_post->post_title; ?> <?php echo $city_show_on_frontend; ?></option>
              <?php endforeach; ?>
            </select>         
            </td>
          </tr>
        </table>
        <em> Note: You need to create "<a href="post-new.php?post_type=towns">Towns</a>" first in order to sync based on towns attribute </em>
        <?php 
          submit_button('Custom Sync Entries', 'secondary');
        else: 
          $city_show_on_frontend = (get_post_meta($atdw_customsync_town, 'town_showonfrontend', true) == 1) ? '(Front and Backend)' : '(Backend only)';
        ?>
        <h3>Town selected: <em><?php echo get_the_title($atdw_customsync_town); ?></em> <span style="font-size:smaller;"><?php echo $city_show_on_frontend; ?></span></h3>
        <input type="hidden" name="atdw_customsync_town" value="<?php echo $atdw_customsync_town; ?>">
        <?php endif; ?>
        <?php
          function isXML($xml){
            libxml_use_internal_errors(true);

            $doc = new DOMDocument('1.0', 'utf-8');
            $doc->loadXML($xml);

            $errors = libxml_get_errors();

            if(empty($errors)){
                return true;
            }

            $error = $errors[0];
            if($error->level < 3){
                return true;
            }

            $explodedxml = explode("r", $xml);
            $badxml = $explodedxml[($error->line)-1];

            $message = $error->message . ' at line ' . $error->line . '. Bad XML: ' . htmlentities($badxml);
            return $message;
          }
          
          function object2array($object) 
          {
          	return @json_decode(@json_encode($object),1); 
          }
          
          // substitute for file_get_contents
          function get_web_page($url){
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            $contents = curl_exec($ch);
            
            if (curl_errno($ch)) {
              return curl_error($ch);
              return "\n<br />";
              $contents = '';
            } else {
              curl_close($ch);
            }
          
            if (!is_string($contents) || !strlen($contents)) {
              echo "Failed to get contents.";
              $contents = '';
            }
          
            return $contents;
          }
        
          // display query result if any
          if($atdw_customsync_town):
            $atdw_customsync_town_state = get_post_meta($atdw_customsync_town, 'town_state', true);
            $atdw_customsync_town_name = get_the_title($atdw_customsync_town);
            
            // query ATDW for entries under selected town
            $strURL	= 'http://' . get_option('atdw_setting_host') . '/api/atlas/products?key=' . get_option('atdw_setting_key') . '&st='. $atdw_customsync_town_state .'&ct=' . str_replace(' ', '%20', $atdw_customsync_town_name) . '&size=300&fl=product_id,product_name,product_category';
            $strXML	= @file_get_contents(str_replace(' ', '%20', $strURL));
            if(!$strXML){
              $strXML = get_web_page(str_replace(' ', '%20', $strURL));
            }
            echo "Querying ATDW entries of " . $atdw_customsync_town_name . " via " . $strURL ." .<br><br>";
            $checkXML = isXML($strXML);
            if($checkXML === true){
              $businesses_list = new SimpleXMLElement($strXML);
              $businesses_list = object2array($businesses_list);
              
              // ATDW categories that to be excluded from this sync
              $remove_cats = array('HIRE', 'TRANSPORT', 'JOURNEY', 'INFO', 'DESTINFO');
              
              $city_total_atdw_entries = 0;
              global $wpdb;
              $atdw_customsync_list = (get_option('atdw_customsync_list')) ? get_option('atdw_customsync_list') : array();
              $atdw_customsync_list_exclude = (get_option('atdw_customsync_list_exclude')) ? get_option('atdw_customsync_list_exclude') : array();
              
              if(((int)$businesses_list['number_of_results']) == 1){
                echo '<table class="form-table">';
                if(!in_array($businesses_list['products']['product_record']['product_category_id'], $remove_cats, false)){
                  echo '<tr valign="top">';
                  
                  $this_checked = '';
                  $this_checked_msg = '';
                  
                  // check whether this entry is already added
                  $check_buss = $wpdb->get_results("SELECT ID, post_excerpt FROM wp_posts WHERE post_excerpt LIKE '%atdw_buss_id||" . $businesses_list['products']['product_record']['product_id'] . "%' LIMIT 1;");
                  if($check_buss[0]->post_excerpt == 'atdw_buss_id||'.$businesses_list['products']['product_record']['product_id']){
                    $check_buss_edit_link = get_edit_post_link($check_buss[0]->ID);
                    $this_checked_msg = '<br>Has been imported to: <a href="' . $check_buss_edit_link . '" target="_blank">' . $check_buss_edit_link . '</a><br>Post status: '.get_post_status($check_buss[0]->ID);
                  }
                  if( !in_array($businesses_list['products']['product_record']['product_id'], $atdw_customsync_list_exclude, false) ){
                    $this_checked = 'checked="checked"';
                  }
                  
                  echo '<td style="width:5px; padding:5px 10px; vertical-align:top;">';
                  echo '<input type="checkbox" name="atdw_customsync_town_select[]" id="cst-select-' .  $businesses_list['products']['product_record']['product_id'] . '" value="' .  $businesses_list['products']['product_record']['product_id'] . '" ' . $this_checked . '>';
                  if($this_checked == 'checked="checked"'){
                    echo '<input type="hidden" name="atdw_customsync_town_select_tmp[]" value="' .  $businesses_list['products']['product_record']['product_id'] . '">';
                  }
                  echo '</td>';
                  echo '<td style="padding:5px 10px;"><label for="cst-select-' . $businesses_list['products']['product_record']['product_id'] . '"><b>' . $businesses_list['products']['product_record']['product_name'] . '</b><br>(ATDW product id: ' . $businesses_list['products']['product_record']['product_id'] . ')</label>' . $this_checked_msg . '</td></tr>';
                  
                  $city_total_atdw_entries++;
                }
                echo '</table>';
              }elseif(((int)$businesses_list['number_of_results']) > 1){
                echo '<table class="form-table">';
                foreach($businesses_list['products']['product_record'] as $atdw_entries){
                  if( !in_array($atdw_entries['product_category_id'], $remove_cats, false) ){
                    echo '<tr valign="top">';
                    
                    $this_checked = '';
                    $this_checked_msg = '';
                    
                    // check whether this entry is already added
                    $check_buss = $wpdb->get_results("SELECT ID, post_excerpt FROM wp_posts WHERE post_excerpt LIKE '%atdw_buss_id||" . $atdw_entries['product_id'] . "%' LIMIT 1;");
                    if($check_buss[0]->post_excerpt == 'atdw_buss_id||'.$atdw_entries['product_id']){
                      $check_buss_edit_link = get_edit_post_link($check_buss[0]->ID);
                      $this_checked_msg = '<br>Has been imported to: <a href="' . $check_buss_edit_link . '" target="_blank">' . $check_buss_edit_link . '</a><br>Post status: '.get_post_status($check_buss[0]->ID);
                    }
                    if( !in_array($atdw_entries['product_id'], $atdw_customsync_list_exclude, false) ){
                      $this_checked = 'checked="checked"';
                    }
                    
                    echo '<td style="width:5px; padding:5px 10px; vertical-align:top;">';
                    echo '<input type="checkbox" name="atdw_customsync_town_select[]" id="cst-select-' . $atdw_entries['product_id'] . '" value="' . $atdw_entries['product_id'] . '" ' . $this_checked . '>';
                    if($this_checked == 'checked="checked"'){
                      echo '<input type="hidden" name="atdw_customsync_town_select_tmp[]" value="' . $atdw_entries['product_id'] . '">';
                    }
                    echo '</td>';
                    echo '<td style="padding:5px 10px;"><label for="cst-select-'. $atdw_entries['product_id'] . '"><b>' . $atdw_entries['product_name'] . '</b><br>(ATDW product id: ' . $atdw_entries['product_id'] . ') </label>' . $this_checked_msg . '</td></tr>';

                    $city_total_atdw_entries++;
                  }
                }
                echo '</table>';
              }
              
              echo "<br><br>" . $city_total_atdw_entries . " entries found.";
              
              if($city_total_atdw_entries > 0){
                echo "<br><br>Please tick the entries to included them in the ATDW sync list.";
                echo "<br>Changes on the list will be applied in the <u>next</u> ATDW sync (manual sync and scheduled sync).";
              }
            }
          endif;
          
          if($atdw_customsync_town){
            submit_button('Update sync list', 'secondary');
            echo '<a href="' . admin_url('admin.php?page=atdw&tab=customsync') . '" class="button button-secondary button-syncnow">Select another town</a>';
          }
          
          // reset
          update_option( 'atdw_customsync_town', null );
          update_option( 'atdw_customsync_town_select', null );
          break;
        case 'labels':
          settings_fields('atdw-group-labels');
          do_settings_fields('atdw-group-labels', null); 
        ?>
        <br />
        <p>To change the <b>Label</b> based on user preferences:</p>
        <table class="form-table">  
          <!-- <tr valign="top">
            <th scope="row"><label for="atdw_label_regions">Regions</label></th>
            <td><input type="text" name="atdw_label_regions" id="atdw_label_regions" value="<?php echo (get_option('atdw_label_regions') ? get_option('atdw_label_regions') : 'Regions'); ?>" /></td>
          </tr> -->
          <tr valign="top">
            <th scope="row"><label for="atdw_label_towns">Towns</label></th>
            <td>
            <input type="text" class="regular-text" name="atdw_label_towns" id="atdw_label_towns" value="<?php echo (get_option('atdw_label_towns') ? get_option('atdw_label_towns') : 'Towns'); ?>" />
            </td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_accomm">Accommodation</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_accomm" id="atdw_label_accomm" value="<?php echo (get_option('atdw_label_accomm') ? get_option('atdw_label_accomm') : 'Accommodation'); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_attraction">Attraction</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_attraction" id="atdw_label_attraction" value="<?php echo (get_option('atdw_label_attraction') ? get_option('atdw_label_attraction') : 'Attraction'); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_event">Event</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_event" id="atdw_label_event" value="<?php echo (get_option('atdw_label_event') ? get_option('atdw_label_event') : 'Event'); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_tour">Tour</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_tour" id="atdw_label_tour" value="<?php echo (get_option('atdw_label_tour') ? get_option('atdw_label_tour') : 'Tour'); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_genservice">General Service</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_genservice" id="atdw_label_genservice" value="<?php echo (get_option('atdw_label_genservice') ? get_option('atdw_label_genservice') : 'General Service'); ?>" /></td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_label_restaurant">Restaurant</label></th>
            <td><input type="text" class="regular-text" name="atdw_label_restaurant" id="atdw_label_restaurant" value="<?php echo (get_option('atdw_label_restaurant') ? get_option('atdw_label_restaurant') : 'Restaurant'); ?>" /></td>
          </tr>
        </table>
        <?php
          break;

          case 'atdw':
          settings_fields('atdw-group-api');
          do_settings_fields('atdw-group-api', null); 
        ?>
        
        <p>The ATDW ATLAS Api allows you to extract tourism information from the ATDW database. The database contains over 33,000 tourism related products across a variety of categories.</p>
        <p>The Api allows for geospatial searching of the data and allows filtering using the ATDW content structure. </p>
        
        <table class="form-table">  
          <tr valign="top">
            <th scope="row"><label for="atdw_setting_host">ATDW API Host</label></th>
            <td>
              <input type="text" class="regular-text" name="atdw_setting_host" id="atdw_setting_host" value="<?php echo get_option('atdw_setting_host'); ?>" />
              <p><em> Please insert ATDW API Host e.g. "atlas.atdw-online.com.au"</em></p>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_setting_key">ATDW API Key</label></th>
            <td>
              <input type="text" class="regular-text" name="atdw_setting_key" id="atdw_setting_key" value="<?php echo get_option('atdw_setting_key'); ?>" />
              <p><em> You may get your ATDW API Key</em> <a href="https://oauth.atdw-online.com.au/listing-subscription?" target="blank">here</a></p>
            </td>
          </tr>

          <tr valign="top">
            <th scope="row"><hr></hr></th>
            <td>
              <hr></hr>
            </td>
          </tr>
          
          <tr valign="top">
            <th scope="row"><label for="atdw_tripadvisor_key">TripAdvisor API Key</label></th>
            <td>
              <input type="text" class="regular-text" name="atdw_tripadvisor_key" id="atdw_tripadvisor_key" value="<?php echo get_option('atdw_tripadvisor_key'); ?>" />
              <p><em> You may get your Trip Advisor API <a href="https://developer-tripadvisor.com/content-api/request-api-access/" target="blank">here</a></em></p>
            </td>
          </tr>
          <tr valign="top">
            <th scope="row"><label for="atdw_gmap_key">Google Maps API Key</label></th>
            <td>
              <input type="text" class="regular-text" name="atdw_gmap_key" id="atdw_gmap_key" value="<?php echo get_option('atdw_gmap_key'); ?>" />
              <p><em> You may get your Google Maps API <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="blank">here</a></em></p>
            </td>
          </tr>
        </table>
        <?php    
          break;
      }
      ?>
      
      <?php 
      if($current_tab != 'customsync' && $current_tab != 'summary'):
        submit_button(); 
      endif;
      ?>
      <p class="submit">
          <a href="<?php echo wp_nonce_url(admin_url('admin.php?page=atdw&tab=sync&action=get_sync_preview'), 'atdw_get_sync_preview'); ?>" class="button button-secondary button-syncnow" <?php echo (((get_option('atdw_sync_method') == 'manual') && ($current_tab == 'sync')) ? '' : 'style="display:none;"'); ?>>
          Sync Preview ATDW
          </a>
          <a href="<?php echo wp_nonce_url(admin_url('admin.php?page=atdw&tab=sync&action=manual_sync_atdw'), 'atdw_manual_sync_atdw'); ?>" class="button button-secondary button-syncnow" <?php echo (((get_option('atdw_sync_method') == 'manual') && ($current_tab == 'sync')) ? '' : 'style="display:none;"'); ?>>
          Sync ATDW Now
          </a> 
          <a href="<?php echo wp_nonce_url(admin_url('admin.php?page=atdw&tab=sync&action=manual_sync_ta'), 'atdw_manual_sync_ta'); ?>" class="button button-secondary button-syncnow" <?php echo (((get_option('atdw_sync_method') == 'manual') && ($current_tab == 'sync')) ? '' : 'style="display:none;"'); ?>>
          Sync TripAdvisor Now
          </a>
      </p>
    </form>
</div>