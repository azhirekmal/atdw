<?php if ( !defined( 'ABSPATH' ) ) die( 'No direct access allowed' ); ?>
<?php 
$town_state = get_post_meta($post->ID, 'town_state', true);
?>
<select name="town_state" id="town_state">
  <option value="">Please Select</option>
  <option value="ACT" <?php echo (($town_state == 'ACT') ? 'selected = "selected"' : ''); ?>>Australian Capital Territory</option>
  <option value="NSW" <?php echo (($town_state == 'NSW') ? 'selected = "selected"' : ''); ?>>New South Wales</option>
  <option value="NT" <?php echo (($town_state == 'NT') ? 'selected = "selected"' : ''); ?>>Northern Territory</option>
  <option value="TAS" <?php echo (($town_state == 'TAS') ? 'selected = "selected"' : ''); ?>>Tasmania</option>
  <option value="SA" <?php echo (($town_state == 'SA') ? 'selected = "selected"' : ''); ?>>South Australia</option>
  <option value="QLD" <?php echo (($town_state == 'QLD') ? 'selected = "selected"' : ''); ?>>Queensland</option>
  <option value="WA" <?php echo (($town_state == 'WA') ? 'selected = "selected"' : ''); ?>>Western Australia</option>
  <option value="VIC" <?php echo (($town_state == 'VIC') ? 'selected = "selected"' : ''); ?>>Victoria</option>
</select>