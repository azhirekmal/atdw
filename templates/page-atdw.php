<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @since 1.0.0
 */

get_header();
?>

<section id="primary" class="content-area">
    <main id="main" class="site-main">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
            <h1 class="entry-title"><?php echo $post->post_title; ?></h1>     
            <p>City: <?php echo $post->atdw_pr_city; ?> | State: <?php echo $post->atdw_pr_state; ?></p> 
            <p>Description: <?php echo wpautop($post->atdw_pr_product_description); ?></p>  

            <?php if (!empty($post->atdw_pr_event_starts) || !empty($post->atdw_pr_event_ends) || !empty($post->atdw_pr_event_frequency) || !empty($post->atdw_pr_event_status)) { ?>
            <hr></hr>
            <p>Event Details</p>
            <p>Start Date: <?php echo $post->atdw_pr_event_starts; ?> | End Date: <?php echo $post->atdw_pr_event_ends; ?></p>
            <p>Frequency: <?php echo $post->atdw_pr_event_frequency; ?> | Status: <?php echo $post->atdw_pr_event_status; ?></p>
            <?php } ?>

            <hr></hr>
            <h4>Facilities and Accessibility</h4>
            <p>Children Catered: <?php echo $post->atdw_pr_children_catered; ?> | Pet Allowed: <?php echo $post->atdw_pr_pet_allowed; ?></p>

            <p><?php echo $post->atdw_pr_disabled_access; ?></p> 

            <hr></hr>
            <h4>Addresses</h4>
            <p><?php echo wpautop($post->atdw_pr_pysical_addr); ?></p>   
 
            <p><iframe width="100%" height="350" src="https://maps.google.com/maps?q=<?php echo $post->atdw_pr_pysical_lat; ?>,<?php echo $post->atdw_pr_pysical_long; ?> &t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></p>

            <p>
            <h4>Social Media</h4>
            <?php $fb_row = $wpdb->get_row( "SELECT * FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key like 'external_system_text-1'" ); ?>
            Facebook: <a href="<?php echo $sm_row->meta_value; ?>" target="blank"><?php echo $fb_row->meta_value; ?></a> <br>
            <?php $tw_row = $wpdb->get_row( "SELECT * FROM wp_postmeta WHERE post_id = '" . $post->ID . "' AND meta_key like 'external_system_text-2'" ); ?>
            Twitter: <a href="<?php echo $tw_row->meta_value; ?>" target="blank"><?php echo $tw_row->meta_value; ?></a> 
        </p>                 
        </div>
    </article>
</main>
</section><!-- .entry-content -->

<footer class="post-footer clearfix"></footer>

<?php get_footer() ?>