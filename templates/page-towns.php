<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @since 1.0.0
 */

get_header();
?>

<section id="primary" class="content-area">
    <main id="main" class="site-main">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
            <h1 class="entry-title"><?php echo $post->post_title; ?></h1>     
            <p>Town: <?php echo $post->town_state; ?></p> 

            <hr></hr>
            <p>Google Map</p>  
            <p>Lat: <?php echo $post->town_lat; ?> | Lang: <?php echo $post->town_lng; ?></p>              
        </div>
    </article>
</main>
</section><!-- .entry-content -->

<footer class="post-footer clearfix"></footer>

<?php get_footer() ?>