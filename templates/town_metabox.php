<?php if ( !defined( 'ABSPATH' ) ) die( 'No direct access allowed' ); ?>
<?php 
$town_lat = get_post_meta($post->ID, 'town_lat', true) ? get_post_meta($post->ID, 'town_lat', true) : '-25.363882';
$town_lng = get_post_meta($post->ID, 'town_lng', true) ? get_post_meta($post->ID, 'town_lng', true) : '131.044922';
?>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 400px;
        width: 100%;
      }
      #pac-input.controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }
      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 70%;
      }
      #pac-input:focus {
        border-color: #4d90fe;
      }
      .pac-container {
        font-family: Roboto;
      }
      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }
      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
    </style>
    
<table style="width: 100%;"> 
  <tr>
    <td>
      <input id="pac-input" class="controls" type="text" placeholder="Search Box">
      <div id="map"></div>
      <input type="hidden" id="town-lat" name="town_lat" value="<?php echo $town_lat; ?>" />
      <input type="hidden" id="town-lng" name="town_lng" value="<?php echo $town_lng; ?>" />
    </td>
  </tr>         
</table>

<script>
  function initAutocomplete() {
    var myLatlng = new google.maps.LatLng(<?php echo $town_lat; ?>,<?php echo $town_lng; ?>);
    var mapOptions = {
      zoom: 12,
      center: myLatlng
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    // Place a draggable marker on the map
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      draggable: true,
      title: "Drag me!"
    });
    
    marker.addListener('dragend', function() {
      document.getElementById("town-lat").value = this.getPosition().lat();
      document.getElementById("town-lng").value = this.getPosition().lng();
    });
    
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
    
    // To avoid page submission when user clicked "Enter" key to select a location
    jQuery(document).find('#pac-input').keypress(function(e){
      if ( e.which == 13 ) // Enter key = keycode 13
      {
        return false;
      }
    });
    
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        // Update marker position
        marker.setPosition(place.geometry.location);
        
        // Update hidden fields value for save post
        document.getElementById("town-lat").value = marker.getPosition().lat();
        document.getElementById("town-lng").value = marker.getPosition().lng();        

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo @get_option('atdw_gmap_key'); ?>&libraries=places&callback=initAutocomplete"
     async defer></script>